<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Webmozart\Assert\Assert;

trait AttributeTrait {
    private $index;
    public function has($name) {
        return isset($this->index[$name]); 
    }
    public function get($name, $default = '')
    {
        if ($this->has($name)) return (string) $this->index[$name];
        return $default;
    }
    public function set($name, $value)
    {
        $this->index[$name] = $value;
        return $this;
    }
}