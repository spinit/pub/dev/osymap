<?php
namespace Spinit\Dev\Opensymap\Command;

use Spinit\Dev\AppRouter\Command\ResponseCommand;

class ResponseSetContent extends ResponseCommand {
    public function exec()
    {
        $args = func_get_args();
        array_unshift($args, 'cmd', 'desktop.setContent(args[0], args[1])');
        $this->getResponse()->add('data,exec', $args);
    }
}