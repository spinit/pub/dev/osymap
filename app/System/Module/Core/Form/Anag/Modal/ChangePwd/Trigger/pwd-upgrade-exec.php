<?php

use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form = $this->getForm();
$usr = $form->getModel();
$usr->load($form->getItem('hdn_id')->getValue());

if (strtoupper(xid($this->getItem('pwd_new1')->getValue())) != strtoupper(xid($this->getItem('pwd_new2')->getValue()))) {
    throw new MessageError("Le password non coincidono", "Attenzione");
}

$usr->set('aut_pwd', xid($this->getItem('pwd_new1')->getValue()));
$usr->save();
$form->getResponse()->addCommand('$(this).trigger("close")');

