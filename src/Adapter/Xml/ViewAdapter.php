<?php
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Adapter\Xml\ItemAdapter;
use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\FileTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasBuilderTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ViewAdapterInterface;

use function Spinit\Dev\AppRouter\fspath;

abstract class ViewAdapter implements ViewAdapterInterface {

    use FileTrait;
    use AttributeTrait;
    use HasBuilderTrait;
    use FileTrait;

    public function getITemList() : iterable {
        foreach($this->index->view->item?:[] as $item) {
            yield new ItemAdapter($this, $item);
        }
    }

    public function getTrigger($event, $when): string
    {
        $fname = fspath($this->getRoot(), 'Trigger', "{$event}-{$when}.php");
        $content = '';
        if (is_file($fname)) {
            $content =  file_get_contents($fname);
        }
        return $content;
    }

    public function getAssetList() : iterable {
        return $this->listResourceIn('Asset', 'is_file', function($item) {
            return $item;
        });
    }

}