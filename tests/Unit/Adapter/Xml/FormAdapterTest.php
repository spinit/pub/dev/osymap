<?php
namespace Spinit\Test\Unit\Adapter\Xml;

use PHPUnit\Framework\TestCase;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;

use function Spinit\Dev\AppRouter\debug;

class FormAdapterTest extends TestCase {
    private $obj;
    protected function setUp() : void
    {
        parent::setUp();
        $app = new TestApplicationAdapter();
        $this->obj = $app->getForm('Core:Test');
    }
    
    public function testBuilder() {
        $this->assertEquals('Builder:Inner', $this->obj->getBuilder('inner'));
        $this->assertEquals('', $this->obj->getBuilder(''));
    }

    public function testGetItemList() {
        $items = $this->obj->getItemList();
        foreach(['', 'itm2'] as $name) {
            $item = $items->current();
            $this->assertEquals($name, $item->getName());
            $items->next();
        }
    }
    public function testModal() {
        $modal = $this->obj->getModal('GetEmail');
        $items = $modal->getItemList();
        foreach($items as $item) {
            $this->assertEquals('Email', $item->get('label'));
            $this->assertEquals($modal, $item->getView());
            break;
        }
    }
}