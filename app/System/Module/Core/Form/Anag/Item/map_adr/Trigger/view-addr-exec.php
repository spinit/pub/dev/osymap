<?php

use Spinit\Dev\Opensymap\Core\Helper\FieldFormDetail;

// viene caricata la nuova form con la chiave eventualmente indicata
$item = $event->getParam(0);
$key = ['id' => $item['id']];
$newForm = new FieldFormDetail($this);
$newForm->exec($key, '@build');
