<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\Button;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util;
/**
 * Description of TextBox:Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Button extends Builder
{
    public function build(ItemInterface $field, $whithLabel = true)
    {
        $this->addResource('script.js');
        $this->addResource('Button.css');
        $class = $field->get('class');
        $field->set('class', '');

        $div = $this->makeComponent($field, 'osy-command');
        $lbl = $div->child(0);
        if ($field->get('noLabel')) {
            $lbl->att('style', 'display:none');
        } else {
            $lbl->att('style', 'visibility:hidden; white-space: nowrap;');
        }
        $btn = $div->add(Util\Tag::create('button'))
            ->att('class', 'btn '.Util\nvl($class, 'btn-success'))
            ->att('style', $field->get('style'))
            ->att('type', 'button');
        
        $event = $field->get('event');
        $eventForm = "";
        if ($field->get('eventForm')) {
            $eventForm = "1";
            $event =  $event ?: $field->get('eventForm');
        }
        $btn->att('osy-event-form', $eventForm) // l'evento viene gestito dalla form?
            ->att('osy-event', $event?:'click')
            ->att('osy-param',  $field->get('param'))
            ->att('osy-message',  $field->get('message'));
        
        $btn->add($field->get('label', '[-- Label non impostata --]'));
        return $div;
    }

}
