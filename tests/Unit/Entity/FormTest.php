<?php
namespace Spinit\Test\Entity\FormTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Test\Helper\TestChannel;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class FormTest extends TestCase {

    private $osy;
    /**
     * @var ApplicationInstance
     */
    private $app;

    /**
     * @var FormInterface
     */
    private $form;

    protected function setUp() : void
    {
        parent::setUp();
        $this->osy = new OpensymapDummy();
        $this->app = $this->osy->getFactory()->getApplication('app@test');
        $this->form = $this->app->getForm('Test');
    }
    
    public function testGetInstance() {
        $this->assertEquals($this->app, $this->form->getApplication());
    }

    public function testItem() {
        $items = (array) $this->form->getItemList();
        $itx = array_shift($items);
        $it2 = array_shift($items);
        $this->assertEquals(0, count($items));
        $this->assertEmpty($itx->getName());
        $this->assertEquals('itm2', $it2->getName());
        $it1 = $this->form->getItem('itm1');
        $this->assertEquals('itm1', $it1->getName());
    }

    public function testRun() {
        $expected = '[(direct class)[itm1(in form)]][itm2(in module)[(in app)][(type empty)]]';
        $response = $this->form->run(new Request('POST', ''));
        $this->assertStringContainsString($expected, $response->get('content'));
    }

    public function testRunWithBuilderInFactory() {
        $it1 = $this->form->getItem('itm1');
        $it1->set('type', 'in-factory');
        try {
            $response = $this->form->run(new Request('POST', ''));
            $this->fail('Errore non rilevato : Builder non presente tra quelli impostati');
        } catch (\Exception $e) {
            $this->assertTrue(true);
        }
        $this->osy->getFactory()->setBuilder('in-factory', 'Spinit:Test:Builder:TestBuilder');
        $response = $this->form->run(new Request('POST', ''));
        $expected = '[(direct class)[itm1(in form)]][itm2(in module)[(in app)][(type empty)]]';
        $this->assertStringContainsString($expected, $response->get('content'));
    }
}