<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use function Spinit\Dev\AppRouter\debug;

class ModalLoad extends EventHandlerModal {

    /**
     * preimpostazione dei singoli 
     */
    public function doAfter($event) {
        $view  =$this->getModal();
        $request = $view->getForm()->getRequest();
        $loader = function($list) use (&$loader, $request) {
            foreach($list as $item) {
                $item->trigger('@load');
                $loader($item->getItemList());
            }
        };
        $loader($view->getItemList());
    }

    /**
     * Caricamento dati nel modello e aggiornamento dei valori in struttura
     */
    public function doExec($event) {
        $modal = $this->getModal();
        $form = $modal->getForm();
        $request = $form->getRequest();

        foreach($event->getParam(0)?:[] as $name => $value) {
            try {
                $modal->getItem($name)->setValue($value);
            } catch (\Exception $e) {
            }
        }
    }
}