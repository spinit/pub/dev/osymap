<?php
namespace Spinit\Test\Unit\Runner;
use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Runner\ComponentRunner;
use Spinit\Test\Helper\TestChannel;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getColonsPath;

class ComponentRunnerTest extends TestCase {

    private $channel;
    private $runner;

    public function testComponent()
    {
        $instance = new OpensymapDummy([], []);
        $resource = getColonsPath(self::class).'/ComponentFile.txt';
        $instance->setRequest(new Request('GET', 'localhost/cmp/'.$resource));
        $this->runner = new ComponentRunner($instance);
        $this->channel = $channel = new TestChannel();
        $response = $this->runner->run(new Request('GET', $resource));
        $response->send($channel);
        
        $this->assertEquals('Ok test', $channel->getContent());
    }

    public function testComponentCallWhitoutAuth() {
        $instance = new OpensymapDummy([], []);
        $resource = getColonsPath(self::class).'/ComponentFile.txt';
        $response = $instance->run(new Request('GET', 'localhost/cmp/'.$resource));
    
        $this->assertEquals('login', $response->get('section'));
    }

    public function testComponentCallWhitAuth() {
        $resource = getColonsPath(self::class).'/ComponentFile.txt';

        $instance = new OpensymapDummy([], []);
        $request = new Request('GET', 'cmp/'.$resource);
        $instance->getMainInteractor()->setUser($request, ['id'=>'ok'], '');

        $channel = new TestChannel();
        $response = $instance->run($request);
        $response->send($channel);
        
        $this->assertEquals('Ok test', $channel->getContent());
    }
}
