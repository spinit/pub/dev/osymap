
module.exports = {
    setupFiles : ["./conf/jest/mocks/dom.js"],
    coverageDirectory : './.cache/jest',
    collectCoverage : true
};