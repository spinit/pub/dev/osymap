<?php
namespace Spinit\Dev\Opensymap;

use Spinit\Dev\AppRouter\Core\HasInstanceTrait;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Exception\UserNotLoggedExecption;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;

abstract class Runner {

    use HasInstanceTrait {
        getInstance as private ga;
    }

    public function __construct(OpensymapInterface $instance)
    {
        $this->setInstance($instance);
        $this->init();
    }
    public function getInstance() : OpensymapInterface {
        return $this->ga();
    }
    protected function init() {
    }
    
    abstract public function run(Request $request) : Response;
}