<?php
namespace Spinit\Dev\Opensymap\Runner;

use ReflectionClass;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Core\Asset;
use Spinit\Dev\Opensymap\Runner;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getClassPath;

class ComponentRunner extends Runner {
    public function run(Request $request) : Response {

        $reflect = new ReflectionClass(getClassPath($request->getPath(0)));
        $rootDir = dirname($reflect->getFileName());
        $fs = new Asset($this->getInstance(), $rootDir);
        return $fs->run($request->shiftPath());
    }
}