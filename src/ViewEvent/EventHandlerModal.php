<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Type\ModalInterface;

class EventHandlerModal {
    private $modal;

    public function __construct(ModalInterface $modal )
    {
        $this->modal = $modal;
    }

    public function getModal() : ModalInterface {
        return $this->modal;
    }
}