<?php
namespace Spinit\Dev\Opensymap\Helper\Formatter;

use Spinit\Dev\AppRouter\FormatterIdentity;

use function Spinit\Dev\AppRouter\debug;

class DateTimeFormatter extends DateFormatter {
    function format($value): string
    {
        list($date, $time) = $this->makeDate($value);
        if (!$time) return $date;
        $tt = explode(':', $time);
        if (count($tt)> 2) {
            array_pop($tt);
        }
        return $date.' '.implode(':', $tt);
    }
}