<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;

class FormLoad extends EventHandlerForm {

    private function iterItems($items, $event) {
        foreach($items as $item) {
            $item->trigger($event);
            // viene chiesto al builder quali sono i figli "effettivi" dell'elemento
            $this->iterItems($item->getBuilder()->getItemList($item), $event);
        }
    }

    /**
     * Precaricamento degli input sui valori passati in POST
     */
    public function doBefore($event) {
        $form= $this->getForm();
        $form->setPkey(null);
        if ($event->getParam(0)) {
            if (is_array($event->getParam(0))) {
                $pkey = $event->getParam(0);
            } else {
                $pkey = json_decode(base64_decode($event->getParam(0)) ,1);
            }
            $form->setPkey($pkey);
            if (!$pkey) debug($event->getParam(0));
        }
        $this->iterItems($form->getItemList(), '@loading');
        foreach($form->getRequest()->getPost()?:[] as $name => $value) {
            try {
                $form->getItem($name)->setValue($value, 1);
            } catch (NotFoundException $e) {
                // se il componente non viene trovato ... allora non viene impostato
            }
        }    
    }

    /**
     * Caricamento dati nel modello e aggiornamento dei valori in struttura
     */
    public function doExec($event) {
        $form = $this->getForm();
        
        $request = $form->getRequest();
        $model = null;
        // il model viene letto se i dati non sono ancora inizializzati OR è richiesto esplicitamente
        if (!$request->getApp('osy,init') or $request->getApp('osy,load-model')) {
            // e ovviamente è presente la chiave
            $model = $form->getModel();
            if ($pkey = $form->getPkey()) {
                $model AND $model->load($pkey);
            }
            if ($pkey or $request->getApp('osy,load-model')) {
                $model AND $form->trigger('@load-model', [$model]);
            }
        }
    }

    public function doExecLoadModel($event) {
        $this->doReloadItem($event->getParam(0));
    }

    public function doAfter($event) {
        $this->iterItems($this->getForm()->getItemList(), '@load');
    }
    
    public function doReloadItem($extModel = null) {

        $form = $this->getForm();
        $response = $form->getResponse();
        $model = $extModel?:$form->getModel();
        // i campi chiave sono preimpostati
        $form->getPkey() AND $model AND $model->set($form->getPkey());
        $loader = function($list) use (&$loader, $response, $model) {
            foreach($list as $item) {
                //debug($item->getValue());
                // successivamente viene aggiornato con il valore presente nel model
                $model AND $item->getBuilder()->loadFromModel($item, $model);
                // infine il valore attuale viene registrato nella riposta per poter aggiornare l'interfaccia
                //debug($item->getName(), $item->getValue());
                $item->getName() AND $response->set('init,'.$item->getName(), $item->getValue());
                // il tutto viene eseguito su tutti gli elementi dell'albero
                $loader($item->getBuilder()->getItemList($item));
            }
        };
        $loader($form->getItemList());
    }
}