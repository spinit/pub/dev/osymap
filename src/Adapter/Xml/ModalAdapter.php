<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\HasBuilderTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasFormTrait;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ModalAdapterInterface;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;

class ModalAdapter extends ViewAdapter implements ModalAdapterInterface {

    use HasFormTrait;

    private $name;

    public function __construct(FormAdapterInterface $form,  string $name)
    {
        $this->setForm($form);
        $this->name = $name;
        $this->init(fspath($form->getRoot(), 'Modal', $name, 'index.xml'));
    }
    public function getName($full = false) : string{
        return (string) $this->name;
    }
    public function getCommandList(): iterable
    {
        foreach($this->getIndex()->command->item?:[] as $item) {
            yield new ItemAdapter($this, $item);
        }
    }
    public function getInitCode() : string
    {
        return (string) $this->getIndex()->init;
    }
}