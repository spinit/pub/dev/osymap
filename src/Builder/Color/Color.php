<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\Color;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util;
use Spinit\Util\Table;
/**
 * Description of TextBox:Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Color extends Builder
{
    public function checkValue(ItemInterface $item, $value) {
        if (is_array($value)) return $value;
        return json_decode($value, 1) ? : "";
    }
    public function build(ItemInterface $field, $whithLabel = true)
    {
        $this->addResource('coloris.js');
        $this->addResource('coloris.css');
        $this->addResource('color.css');
        $this->addResource('color.js');

        $div = $this->makeComponent($field, 'osy-color');
        
        $div->add(Util\Tag::create("input"))
            ->att("type", "hidden")
            ->att("name", $field->getName())
            ->att("value", $field->getValue())
            ->att("class", "info");
        $tbl = $div->add(new Table());
        $tbl->Cell("Testo")
            ->att('class', "color")
            ->att("name", "txt")
            ->att('style', "background-color:".$field->getValue("txt"))
            ->prp("data-coloris");
        $tbl->Cell("Sfondo")
            ->att('class', "color")
            ->att("name", "bkg")
            ->att('style', "background-color:".$field->getValue("bkg"))
            ->prp("data-coloris");
        $tbl->Cell("Bordo")
            ->att('class', "color")
            ->att("name", "brd")
            ->att('style', "background-color:".$field->getValue("brd"))
            ->prp("data-coloris");
        $res = Util\Tag::create("div")
            ->att("class", "res")
            ->att('style', "color:".$field->getValue("txt").";".
                           "background-color:".$field->getValue("bkg").";".
                           "border-color:".$field->getValue("brd").";");
        $tbl->Cell($res);
        
        $res->add("Risultato");
        return $div;
    }

}
