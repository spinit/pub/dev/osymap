<?php

namespace Spinit\Dev\Opensymap\App\Monitor\Module\Core\Source;

use Spinit\Dev\Opensymap\Builder\PropertyFilter\ItemController;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

class InstanceUsrAnagraficaController extends ItemController {
    public function render($field, $item)
    {
        return 'ok';
    }

    public function accediCon($value, $row) {
        $btn = Tag::create('button')->att('class', 'btn btn-outline-success btn-sm');
        $btn->att('osy-event', 'accedi-con')
            ->att('osy-param', $row);
        $btn->add('Accedi',);
        return $btn;
    }
}