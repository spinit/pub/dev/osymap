<?php

namespace Spinit\Dev\Opensymap\Type;

interface ExtendInterface {
    function getApplicationName() : string;
    function getApplication() : ApplicationInterface;
}