<?php
namespace Spinit\Test\Helper;

use Spinit\Dev\AppRouter\Helper\ChannelTest;

class TestChannel extends ChannelTest {
    private $content = '';
    public function header($code, $headers)
    {
        
    }
    public function write($content)
    {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }
    public function close() {
        $this->content .= ob_get_clean();
    }
}