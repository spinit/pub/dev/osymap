<?php

namespace Spinit\Dev\Opensymap\Entity;

use PharIo\Version\AndVersionConstraintGroup;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Helper\FormUtil;
use Spinit\Dev\Opensymap\Type\Adapter\ItemAdapterInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ViewInterface;
use Spinit\Util\ExecCodeTrait;
use Spinit\Util\ParamTrait;
use Spinit\Util\TriggerTrait;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\getInstance;

class Item implements ItemInterface {
    
    use HasAdapterTrait {
        getAdapter as private _getAdapter;
    }

    use ExecCodeTrait;
    use TriggerTrait;
    use ParamTrait;
    
    /**
     * @var FormInterface
     */
    private $form;
    private $view;

    private $childs = [];

    private $parent = null;

    private $value;
    private $props = [];

    public function __construct(ViewInterface $view, ItemAdapterInterface $adapter) {
        $this->view = $view;
        $this->setAdapter($adapter);
/*
        // produzione contenuto
        $this->bindExec('@build', function($event) {
            $noLabel = $event->getParam(1);
            return $this->build(!$noLabel);
        });
        // invio
        $this->bindAfter('@build', function($event) {
            $builder->setForm($this->getForm());
            if ($response) {
                $response->addContent($this->getName(), $event['result']);
            }
        });
        */
    }
    public function __clone() {
        foreach($this->childs as $k=>$c){
            $this->childs[$k] = clone $c;
        }
        foreach($this->props as $k=>$c){
            $this->props[$k] = is_object($c) ? clone $c : $c;
        }
    }
    // TODO
    public function isVisible() {
        return $this->get('visible')!='no';
    }
    public function getAdapter() : ItemAdapterInterface {
        return $this->_getAdapter();
    }

    public function setParent(ItemInterface $parent) {
        $this->parent = $parent;
    }
    public function getParent() : ItemInterface {
        return $this->parent;
    }
    public function getView() : ViewInterface {
        return $this->view;
    }
    public function getForm() : FormInterface {
        return $this->getView()->getForm();
    }
    /**
     * Effettua la ricostruzione del componente. Se viene passata un oggetto Response, si serializza in esso
     */
    public function build() {
        $args = func_get_args();
        $builder = $this->getBuilder();
        $builder->setForm($this->getForm());
        $cnt = $builder->build($this);
        $this->trigger('@build', [$cnt]);
        if (count($args) and ($args[0] instanceof Response)) {
            $args[0]->addContent($this->getName(), $cnt, true);
        }
        return $cnt;
    }

    public function getBuilder() : BuilderInterface {
        $builder = $this->get('type');
        return $this->getView()->getBuilder($builder);
    }

    public function getName() : string{
        return $this->get('name');
    }
    public function has($name) {
        if (array_key_exists($name, $this->props)) return true;
        return $this->getAdapter()->has($name);
    }
    public function get($name, $default = '') {
        if (array_key_exists($name, $this->props)) return $this->props[$name];
        return $this->getAdapter() ->get($name, $default);
    }

    public function set($name, $value) {
        $this->props[$name] = $value;
        return $this;
    }

    public function addItem(ItemInterface $item) : ItemInterface{
        $item->setParent($this);
        $this->childs[] = $item;
        return $this;
    }

    public function getItemList() : iterable {
        return $this->childs;
    }

    public function getItemAll() : iterable {
        $getAllItem = function ($item) use (&$getAllItem) {
            yield $item;
            foreach($item->getItemList() as $i) yield from $getAllItem($i);
        };
        foreach($this->getItemList() as $item) yield from $getAllItem($item);          
    }

    public function setValue($value, $rewriteValue = false) : ItemInterface
    {
        $this->value = $this->getBuilder()->checkValue($this, $value);
        $rewriteValue AND  $this->getView()->trigger('@rewriteValue', [$this->get('name'), $this->value]);
        return $this;
    }

    public function getValue() {
        //if (!is_array($this->value)) return $this->value;
        $args = func_get_args();
        if (!count($args)) return $this->value;
        if (is_array($args[0]) and !count($args[0])) return $this->value;

        array_unshift($args, $this->value);
        return call_user_func_array('Spinit\Util\arrayGet', $args);
    }
    public function onTrigger($event, $target) {
        // esecuzione trigger definito sul builder
        call_user_func([$this->getBuilder(), 'execTrigger'], $event, $target);
    }
    public function afterTrigger($event, $target) {
        // esecuzione codice personalizzato
        $response = $this->getForm()->getResponse();
        $request = $this->getForm()->getRequest();

        $this->execCode(
            $this->getAdapter()->getTrigger($event['name'], $event['when']),
            array('event' => $event, 'target'=>$target, 'request'=>$request, 'response'=>$response),
            "{$this->get('name')}-{$event['name']}-{$event['when']}_".$this->getView()->getUri()
        );
    }

    public function setValueByRequest(Request $req) {
        $field = $this;
        if (!$field->getName()) return;

        if ($req->getApp('osy,back-name') == $field->getName()) {
            $req->setApp('osy,back-name', '');
            $req->getApp('osy,back-pkey') !== '' AND 
            $field->trigger('@backValue', [$req->getApp('osy,back-pkey')]);
        } else {
            $req->hasPost($field->getName()) AND 
            $field->setValue($req->getPost($field->getName()), 1);
        }
    }
}