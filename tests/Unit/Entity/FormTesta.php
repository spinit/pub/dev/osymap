<?php

namespace Spinit\Test\Unit\Entity\Appli;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Data;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Entity\Form;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ItemAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ModuleAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\ExtendInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;
use Spinit\Dev\Opensymap\Type\OverlayInterface;
use Spinit\Test\Entity\FormDummy;

use function Spinit\Util\arrayGet;

class FormTesta extends TestCase {
    private $app;
    private $form;
    private $adapter;
    protected function setUp() :void
    {
        parent::setUp();
        $this->app = new TestApplication();
        $this->adapter = new TestFormAdapter($this->getStruct());
        $this->form = new Form($this->app, $this->adapter);
    }
    private function getStruct() {
        return [
            [
                'attr' => ['type'=>'panel', 'name'=>'itm0', "test"=>"ok"],
                'child'=>[
                    [
                        'attr' => ['type'=>'panel1', 'name'=>'itm2']
                    ],
                    [
                        'attr' => ['type'=>'panel', 'name'=>'itm3']
                    ],
                ]
            ],
            [
                'attr' => ['type'=>'panel1', 'name'=>'itm1', "test"=>"ok"]
            ],
        ];

    }
    public function testGetItemList() {
        $this->assertEquals(2, count($this->form->getItemList()));
        foreach($this->form->getItemList() as $idx => $item) {
            $this->assertEquals('ok', $item->get('test'));
            $this->assertEquals('itm'.$idx, $item->getName());
        }
    }
    public function testGetItem() {
        $item = $this->form->getItem('itm0');
        $this->assertEquals('ok', $item->get('test'));
        foreach($item->getItemList() as $idx => $child) {
            $this->assertEquals('itm'.($idx+2), $child->getName());
        }
        $item = $this->form->getItem('itm2');
        $this->assertEquals('panel1', $item->get('type'));
        $this->assertEquals('itm2', $item->getName());
    }

    public function testBuild() {
        $item = $this->form->getItem('itm2');
        $this->assertEquals('=itm2=', $item->build());
        $item = $this->form->getItem('itm3');
        $this->assertEquals('+itm3+', $item->build());
    }

    public function testRun() {
        $actual = $this->form->run(new Request('POST', 'localhost'));
        $expected = '+itm0+=itm1=';
        $this->assertEquals($expected, $actual->get('content'));
    }
}

class TestApplication implements ApplicationInterface {
    private $response;

    public function run(Request $request) : Response {
        throw new \Exception();
    }
    public function getResponse(): Response
    {
        if (!$this->response) $this->response = new Response(); 
        return $this->response;
    }
    public function getForm($name) : FormInterface {
        return new FormDummy();
    }
    public function getInstance() : OpensymapInterface {
        throw new \Exception();
    }
    public function getModuleList() : iterable {
        return [];
    }
    public function getModule($name) : ModuleInterface {
        throw new \Exception();
    }
    public function getExtendList() : iterable {
        return [];
    }
    public function addExtend(ExtendInterface $extend) {

    }
    public function getBuilder($name) : ?BuilderInterface {
        return new TestBuilder2();
    }
    public function getOverlay($name) : OverlayInterface {
        throw new \Exception();
    }
    public function getOverlayList() : iterable {
        return [];
    }
}

class TestFormAdapter implements FormAdapterInterface {
    private $list = [];
    public function __construct($struct )
    {
        $this->list = [];
        foreach($struct as $item) {
            $this->list[] = new TestItemAdapter($this, $item);
        }
    }
    public function getItemList() : iterable {
        return $this->list;
    }
    public function getBuilder($name) : string {
        if ($name == 'panel1') {
            return TestBuilder1::class;
        }
        return '';
    }
    public function get($name, $default = '') {
        
    }
    public function set($name, $value) {
        
    }
}

class TestItemAdapter implements ItemAdapterInterface {
    private $data;
    private $form;
    private $list;
    public function __construct(FormAdapterInterface $form, $struct = [])
    {
        $this->data = new Data(arrayGet($struct, 'attr', []));
        $this->form = $form;
        $this->list = [];
        foreach(arrayGet($struct, 'child', []) as $child) {
            $this->list[] = new self($form, $child);
        }
    }
    function getForm() : FormAdapterInterface {
        return $this->form;
    }
    function getItemList() : iterable{
        return $this->list;
    }
    function get($name, $default = '') {
        return $this->data->get($name, $default);
    }
    function set($name, $value) {
        $this->data->set($name, $value);
    }
}

class TestBuilder1 extends Builder {
    public function build(ItemInterface $item)
    {
        return '='.$item->getName().'=';
    }
}
class TestBuilder2 extends Builder {
    public function build(ItemInterface $item)
    {
        return '+'.$item->getName().'+';
    }
}