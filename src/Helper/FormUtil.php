<?php

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Dev\Opensymap\Type\FormInterface;

use function Spinit\Dev\AppRouter\debug;

class FormUtil {
    /**
     * @type : FormInterface
     */
    private $form;

    public function __construct(FormInterface $form) {
        $this->form  = $form;
    }

    public function getApplication() {
        return $this->form->getApplication();
    }
    public function getRequest() {
        return $this->form->getRequest();
    }
    public function getAdapter() {
        return $this->form->getAdapter();
    }

    public function getUrl() {
        $form = $this->form;
        $app = $form->getApplication();
        $pkey = '';
        if($key = $form->getPkey()) {
            if (is_array($key)) ksort($key);
            $pkey = base64_encode(json_encode($key)).'/';
        }
        $query = http_build_query($form->getQuery());
        $path = $app->getName().'/'.$form->getName().'/'.$pkey.($query?'?'.$query:'');
        $url = $app->getInstance()->makeSectionPath('app', $path);
        return $url;
    }
}