var $ = null;
var desktop = null;

var moveGant = false;
var movedGant = false;

function initDesktop(d) {
    desktop = d;
    $ = d.$;
}

function initGant(gant) {
    gant.data('@land', gant.find('.resBox'));
    gant.data('@dayWidth', gant.find('.datInp input.dayWidth'));
    gant.data('@firstRes', gant.find('.datInp input.firstRes'));

    gant.data('@datDays', gant.find('.datInp input.datDays'));
    gant.data('@datStart', gant.find('.datInp input.datStart'));
    gant.data('@datToday', gant.find('.datInp .datToday'));
    gant.data('@datInput', gant.data('@datToday').find('input.today'));
    gant.data('@datView', gant.data('@datToday').find('div.today'));

    gant.data('@datList', gant.find('.datList'));
    gant.data('@datCanvas', gant.find('.datList canvas'));
    gant.data('@resList', gant.find('.resList'));
    gant.data('@resArea', gant.find('.resList .resArea'));
    gant.data('@resName', gant.find('.resName'));

    var resList = {};
    // per posizionare correttamente il background
    gant.find('.resBackground').css('marginLeft', gant.data('@resName').width());
    // indicizzazione risorse
    gant.find('.resBox .boxLine').each(function(line) {
        resList[line.getAttribute('rid')] = line;
    });
    gant.data('@datDays').val(Math.ceil(gant.width() / (gant.data('@dayWidth').val()||100)));
    // lista delle risorse su cui far vedere gli impegni
    gant.data('$resList', resList);
    
    // lista delle risorse visibili nel gant
    gant.data('$resView', {});        
    
    $(document).on('resized.osy-gant', function() {
        resizeGant(gant);
    });
    
    gant.addClass('inited');
    gant.trigger('inited.gant');

};


function resizeGant(gant) {
    gant.data('@datList').width($('.resList .resArea', gant).width());
    gant.data('@resList').css('marginTop', gant.data('@datList').height());
    $('.datInp', gant).width($('.resName', gant).width());
    if (gant.is('.maximize')) {
        let pp = gant.data('@resArea');
        let padT = parseInt($('#main .content').styles().paddingBottom);
        let hh = Math.min(window.innerHeight-pp.offset().top - 50 - padT - 20, pp.height());
        gant.data('@resArea').height(hh+'px');
    }
    paintDatCanvas(gant);
};

function getDate(dat, step, strFormat)
{
    var nDate = new Date(); 
    try {
        var dateParts = dat.split("-");
        // month is 0-based, that's why we need dataParts[1] - 1
        nDate = new Date(+dateParts[0], dateParts[1] - 1, +dateParts[2]); 
    } catch (e) {
        
    }
    if (step) {
        nDate.setDate(nDate.getDate() + step);
    }
    if (strFormat) {
        var mm = (nDate.getMonth() + 1) + '';
        var dd = nDate.getDate() + '';
        var yy = nDate.getFullYear() + '';
        return  yy + '-' + mm.padStart(2, '0') + '-' + dd.padStart(2, '0');
    }
    return nDate;
}

var nmeMonth = {
    0 : 'Gennaio',
    1 : 'Febbraio', 
    2 : 'Marzo',
    3 : 'Aprile',
    4 : 'Maggio',
    5 : 'Giugno',
    6 : 'Luglio',
    7 : 'Agosto',
    8 : 'Settembre',
    9 : 'Ottobre',
    10 : 'Novembre',
    11 : 'Dicembre'
};

/**
 * Scritura date presente sulle colonne
 * @param {type} gant
 * @param {type} refreshData
 * @returns {undefined}
 */
function paintDatCanvas(gant)
{
    let dw =  parseInt(gant.data('@dayWidth').val());
    var ww = gant.width();
    var hh = gant.data('@datList').height();

    gant.data('@datCanvas').attr('width', ww+'px');
    gant.data('@datCanvas').attr('height', hh+'px');

    var ctx = gant.data('@datCanvas').cc(0).getContext('2d');

    gant.data('@resList').css('marginTop', hh);
    ctx.font = '12px verdana';
    //ctx.fillStyle = 'rgba(0, 0, 0)';

    var ll = gant.find('.datInp').width();
    var td = getDate(gant.data('@datInput').val());
    let map = gant.data('$datMap') || {};
    // aggiustamento sfondo overlay
    var delta = -1 * parseInt(gant.data('@dayWidth').val()) * td.getDay();
    var resWidth = gant.data('@resName').width();
    gant.data('@resList').find('.resBackground').css('backgroundPosition', (delta + resWidth) +'px');
    var days = 0;
    var curMonth = -1;
    var sepMonth = -1;
    var prtYear = 0;
    ctx.lineWidth = 0.5;
    for(var px = ll; px < ww; px += dw) {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.strokeStyle = '#0f0f0f';
        ctx.moveTo(px, hh);
        ctx.lineTo(px, hh/2);
        ctx.stroke();
        days += 1;
        if (curMonth != td.getMonth()) {
            if (sepMonth < 0 || sepMonth >= 100) {
                curMonth = td.getMonth();
                var month = nmeMonth[curMonth];
                if (prtYear != td.getFullYear()) {
                    month += ' '+td.getFullYear();
                    prtYear = td.getFullYear();
                }
                ctx.fillText(month, px+7, ctx.canvas.height - 30);
                sepMonth = 0;
            }
        }
        //ctx.textBaseline = 'hanging';
        ctx.fillText((td.getDate()+'').padStart(2, ' '), px+2, ctx.canvas.height - 9);

        let did = [td.getFullYear(), ((td.getMonth()+1)+'').padStart(2, '0'), (td.getDate()+'').padStart(2, '0')].join('-');
        let lw = 7;
        if (map[did] && map[did]['color']) {

            ctx.beginPath();
            ctx.lineWidth = lw;
            ctx.strokeStyle = map[did]['color'];
            ctx.moveTo(px+1, hh-lw+4);
            ctx.lineTo(px+dw-1, hh-lw+4);
            ctx.stroke();
        }
        td.setDate(td.getDate() + 1);
        // i mesi devono essere separati da almeno 100px per non farli sovrapporre
        sepMonth += dw;
    }
    // se i dati non vanno aggiornati (allora la lunghezza viene aggiornata subito)
    var oldVal = parseInt(gant.data('@datDays').val());
    gant.data('@datDays').val(days);
    
}

function setCurrentDate(gant, dat, personalView)
{
    let vv = dat.split('-').reverse();
    vv[0] = vv[0].padStart(2, '0');
    vv[1] = vv[1].padStart(2, '0');
    let viewDate = vv.join('/');
    vv.reverse();
    let storeDate = vv.join('-'); 
    gant.data('@datInput').val(storeDate);

    gant.data('@datView').attr('osy-param', [dat]).html(personalView || viewDate);

    var dw = parseInt(gant.data('@dayWidth').val());
    var d1 = getDate(gant.data('@datInput').val());
    var d2 = getDate(gant.data('@datStart').val());
    var step = -parseInt((d1 - d2)/(24*3600*1000));
    
    gant.data('@land').css('marginLeft', (dw * step + gant.data('@resName').width())+'px');
    setTimeout(paintDatCanvas, 100, gant);
    gant.trigger('data-reload');
    //dataLoad(gant);
}
var docInited = false;

function initDocument() {
    if (docInited) return;

    docInited = true;

    $(document).on('mousedown.osy-gant', '.osy-gant .datCal', function(event) {
        if (event.button === 0) {
            $(this).trigger('data-move', [event.pageX, event.pageY]);
        }
    });
    $(document).on('click.osy-gant', '.osy-gant .datCal', function(event) {
        let canvas = $(this);
        let gant = canvas.closest('.osy-gant');
        let ll = gant.find('.datInp').width();
        let dw = parseInt(gant.data('@dayWidth').val());
        let gap = event.pageX - canvas.offset().left - ll;
        let step = Math.floor(gap / dw);
        let dateClicked = getDate(gant.data('@datInput').val(), step, 1);
        if (movedGant) return;
        gant.trigger('click-date', [dateClicked]);
    });

    let datViewTm = null;
    let datView = null;
    $(document).on('mousemove.osy-gant', '.osy-gant .datCal', function(event) {
        let element = this;
        clearTimeout(datViewTm);
        datViewTm = setTimeout(function() {
            if(moveGant) {
                if (datView) datView['snippet'].remove();
                datView = null;
                return;
            }
            let canvas = $(element);
            if((event.pageY - canvas.offset().top) < canvas.height() / 2) return;
            let gant = canvas.closest('.osy-gant');
            let ll = gant.find('.datInp').width();
            let dw = parseInt(gant.data('@dayWidth').val());
            let gap = event.pageX - canvas.offset().left - ll;
            let step = Math.floor(gap / dw);
            let dateClicked = getDate(gant.data('@datInput').val(), step, 1);
            if (datView && datView['dat'] == dateClicked) return;
            let map = gant.data('$datMap') || {};
            let el = map[dateClicked];
            if (datView) datView['snippet'].remove();
            datView = {
                'dat': dateClicked, 
                'snippet' : $('<div class="dat-view-snippet">'+((el && el['html']) || dateClicked)+'</div>')};
            if (gap > (canvas.width()/2)) {
                let rpos =  (canvas.width() + canvas.offset().left) - event.pageX;
                datView['snippet'].css('right',(Math.floor( rpos / dw ) *dw)+'px');
            } else {
                let lpos =  (step*dw) + ll;
                datView['snippet'].css('left',lpos+'px');
            }
            datView['snippet'].css('top', canvas.height()+'px');
            datView['snippet'].after(element);
        }, 200);
    });
    $(document).on('mouseout.osy-gant', '.osy-gant .datCal', function(event) {
        if (datView) datView['snippet'].remove();
        datView = null;
        clearTimeout(datViewTm);
    });

    // quando viene cliccato il giorno si effettua la lettura dati
    $(document).on('setDate.osy-gant', '.osy-gant .datInp', function(event, dat) {
        let arDat = typeof(dat) == typeof('') ? JSON.parse(dat) : dat ;
        setCurrentDate($(this).closest('.osy-gant'), arDat[0], arDat[1]);
        $('.osy-box', this).trigger('close');
    });
    $(document).on('mousemove.osy-gant', function(event) {
        if(!moveGant) {
            return;
        }

        var dw = parseInt(moveGant.data('@dayWidth').val());
        var step = parseInt((event.pageX - moveGant.data('startX')) / dw);
        moveGant.data('@resArea').scrollTop(moveGant.data('deltaY') - event.pageY);
        setCurrentDate(moveGant, getDate(moveGant.data('startD'), -step, 1));
    });

    $(document).on('mouseup.osy-gant', function() {
        moveGant && moveGant.trigger('data-move-load');
        moveGant = false;
    });
    // quando i dati sono stati riscritti ... si fa un refresh sulle date ... se qualcosa è cambiato
    $(document).on('data-loaded.osy-gant', function() {
        paintDatCanvas($(this));
    });

    $(document).on('mouseleave.osy-gant', function(event) {
        if (event.target == this) {
            moveGant = false;
        }
    });

    $(document).on('data-move.osy-gant', '.osy-gant', function(event, [startX, startY])
    {
        movedGant = false;
        moveGant = $(this);
        moveGant.data('startD', moveGant.data('@datInput').val());
        moveGant.data('startX', startX);
        moveGant.data('startY', startY);
        moveGant.data('deltaX', parseInt(moveGant.data('@land').css('marginLeft')));
        moveGant.data('deltaY', parseInt(moveGant.data('@resArea').scrollTop()) + startY);
    });

    $(document).on('mousedown.osy-gant', '.osy-gant .resBackground', function(event) {
        if (event.button === 0) {
            $(this).trigger('data-move', [event.pageX, event.pageY]);
        }
    });
    $(document).on('mousedown.osy-gant', '.osy-gant .resBox', function(event) {
        if (event.button === 0) {
            $(this).trigger('data-move', [event.pageX, event.pageY]);
        }
    });
    $(document).on('touchstart.osy-gant', '.osy-gant .resBackground', function(event) {
        let pageX = event.touches[0].pageX;
        let pageY = event.touches[0].pageY;
        $(this).trigger('data-move', [pageX, pageY]);
    });
    $(document).on('touchend.osy-gant', function() {
        moveGant && moveGant.trigger('data-move-load');
        moveGant = false;
    });
    $(document).on('touchmove.osy-gant', function(event) {
        if(!moveGant) {
            return;
        }

        var dw = parseInt(moveGant.data('@dayWidth').val());
        var step = parseInt((event.touches[0].pageX - moveGant.data('startX')) / dw);
        moveGant.data('@resArea').scrollTop(moveGant.data('deltaY') - event.touches[0].pageY);
        setCurrentDate(moveGant, getDate(moveGant.data('startD'), -step, 1));
    });

    // per muovere il calendario di mese in mese
    $(document).on('setMonth', '.osy-gant .datInp', function(event, month) {
        let inp = $('input.today', this);
        desktop.sendEvent(inp.closest('.osy-gant'), ['open-cal', [inp.val(), month]]);
    });

    // quando si preme su un cella allora parte la richiesta 
    $(document).on('click.osy-gant', '.osy-gant .resBackground .resLine', function(event)
    {
        if (isMovedGant()) return;
        
        var el = $(this);
        var eloff = $(this).offset();
        var gant = el.closest('.osy-gant');
        var posClick = {
            'x' : event.pageX - eloff.left - gant.data('@resName').width(),
            'y' : event.pageY - eloff.top
        }
        var step = parseInt(posClick.x / parseInt(gant.data('@dayWidth').val()));
        desktop.setPositionClick(event);
        desktop.sendEvent(el, ['res-click', {'rid': el.attr('rid'), 'step':step, 'date': getDate(gant.data('@datInput').val(), step, 1)}]);
        event.stopPropagation();
        $('.osy-box').trigger('close');
    });
}

export function init (gant, desktop) {
    initDesktop(desktop);
    initGant(gant);
    // richiesta dati
    setCurrentDate(gant, gant.data('@datInput').val());
    resizeGant(gant);
    initDocument();
}

export function isMovedGant() {
    if (arguments.length>0) {
        movedGant = arguments[0];
    }
    return movedGant;
}
