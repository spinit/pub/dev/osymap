<?php
namespace Spinit\Dev\Opensymap\Builder\Gant;

use DateTime;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Helper\Calendar;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Helper\OpenFormDetail;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of Gant
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Gant extends Builder
{
    public function registerEvent() {
        $this->bindExec('open-cal', [$this, 'onOpenCalendar']);
        $this->bindExec('open-res', [$this, 'onOpenResource']);
        $this->bindExec('data-load', function($event, $target) {
            //$target->getForm()->setQuery($target->getName(),['dat' => $target->getValue('dat')]);
        });
    }
    public function build(ItemInterface $field, $whithLabel = true)
    {
        $cnt = $this->makeComponent($field, 'osy-gant', false);
        $this->addResource('Gant.js');
        $this->addResource('Gant.css');
        $datList = $cnt->add(Util\Tag::create('div'))->att('class', 'datList');
        $datList->add($this->initCalendarArea($field));
        $resList = $cnt->add(Util\Tag::create('div'))->att('class', 'resList');
        $resList->add($this->initWorkArea($field));
        return $cnt;
    }
    
    private function dateFormat($val, $from='/', $to='-') {
        $part = array_reverse(explode($from, $val));
        if (count($part)<3) {
            return $val;
        }
        return implode($to, $part);
    }
    private function isValidDate ($date) {
        $format = 'Y-m-d';
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }
    private function initCalendarArea($field)
    {
        $cnt = Util\Tag::create('');
        $datCal = $cnt->add(Util\Tag::create('canvas'))->att('class', 'datCal');
        $datInp = $cnt->add(Util\Tag::create('div'))->att('class', 'datInp');
        $datStart = $this->dateFormat($field->getValue('start', date('Y-m-d')));
        if (!$this->isValidDate($datStart)) $datStart = date('Y-m-d');

        $datDat = $field->getValue('dat', $datStart);
        if (!$this->isValidDate($datDat)) $datDat = $datStart;
        
        $inp = $datInp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[dayw]')
            ->att('class', 'dayWidth')
            ->att('value', $field->get('dayw', 100))
            ->att('type', 'hidden');
        $inp = $datInp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[days]')
            ->att('class', 'datDays')
            ->att('type', 'hidden');
        $inp = $datInp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[start]')
            ->att('class', 'datStart')
            ->att('value', $datStart)
            ->att('type', 'hidden');
        $inp = $datInp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[firstRes]')
            ->att('class', 'firstRes')
            ->att('value', $field->getValue('firstRes'))
            ->att('type', 'hidden');
        $datDiv = $datInp->add(Util\Tag::create('div'))->att(['class'=>'datToday']);
        $datSpan = $datDiv->add(Util\Tag::create('div'))->att(['class'=>'today cursor-pointer', 'osy-event'=>'open-cal']);
        $datSpan->add($this->dateFormat($datDat, '-', '/'));
        $inp = $datDiv->add(Util\Tag::create('input'))
            ->att('class', 'today')
            ->att('name', $field->getName().'[dat]')
            ->att('type', 'hidden');
        $inp->att('value', $datDat);
        $datDiv->add(Util\Tag::create('div'))->att(['class'=>'spinner']);
        $inp = $datInp->add(Util\Tag::create('div'))->att('class', 'dat-content');
        return $cnt;
    }
    public function getResourceList($field, $debugArea) {
        return (new MakerData())->build($field, $field->getValue('search'), $debugArea);
    }
    
    private function initWorkArea($field)
    {
        $cnt = Util\Tag::create('div')->att('class', 'resArea');
        $list = $this->getResourceList($field, $cnt);
        $cnt->att('onscroll', "clearTimeout(this.tts); this.tts = setTimeout((resList) => resList.trigger('gant-scroll'), 500, desktop.$(this));");
        $boxA = $cnt->add(Util\Tag::create('div'))->att('class', 'resBox');
        $layA = $cnt->add(Util\Tag::create('div'))->att('class', 'resBackground');
        $resA = $cnt->add(Util\Tag::create('div'))->att('class', 'resName');
        
        $boxG = $boxA->add(Util\Tag::create('div'))->att('class', 'group');
        $layG = $layA->add(Util\Tag::create('div'))->att('class', 'group');
        $resG = $resA->add(Util\Tag::create('div'))->att('class', 'group');
        $groupName = '';
        $groupCount = 0;
        foreach($list as $res) {
            if ($groupName != Util\arrayGet($res, '__group')) {
                $groupCount = 0;
                if ($groupName) {
                    $boxG = $boxA->add(Util\Tag::create('div'))->att('class', 'group');
                    $layG = $layA->add(Util\Tag::create('div'))->att('class', 'group');
                    $resG = $resA->add(Util\Tag::create('div'))->att('class', 'group');
                }
                $group = [
                    $boxG->add(Util\Tag::create('div'))->att('class', 'groupLine'),
                    $layG->add(Util\Tag::create('div'))->att('class', 'groupLine'),
                    $resG->add(Util\Tag::create('div'))->att('class', 'groupLine')
                ];
                $field->trigger('@gant-group', [$group[2], $res, $group[0], $group[1]]);
                $groupName = (string) Util\arrayGet($res, '__group');
                // se non è stato impostato nulla ... si mette il nome del gruppo
                if(!$group[2]->child(0)) {
                    $group[2]->add(str_repeat($groupName, 1));
                }
            }
            $row = $boxG->add(Util\Tag::create('div'))->att(['id'=>'res-'.$res['id'], 'rid'=>$res['id'], 'class'=>'resLine boxLine']);
            $layG->add(Util\Tag::create('div'))->att(['id'=>'lay-'.$res['id'], 'class'=>'resLine', 'rid'=>$res['id'], 'class'=>'resLine']);
            $resTag = $resG->add(Util\Tag::create('div'));
            $resTag->att([
                'id'=>'nme-'.$res['id'], 
                'rid'=>$res['id'], 
                'class'=>'resLine '.Util\arrayGet($res,'__class'),
                'osy-descr'=>$res['nme']])
                   ->add($res['nme']);
            if ($groupCount++ == 0) {
                $resTag->att('data-scrollTo', 'parent');
            }
            if ($field->get('formDetail')) {
                $resTag->att('osy-event', 'open-res');
                $resTag->att('class', 'cursor-pointer', ' ');
                $resTag->att('osy-param', $res['id']);
            }
        }
        $bgw = Util\arrayGet($field->getValue(), 'dayw', $field->get('dayw', 100));
        $bg =  $this->getResourcePath('bgcolor');
        $cnt->add('<style>#'.$field->getName().'.osy-gant .resList .resBackground {background: url('.$bg.'&w='.$bgw.');}</style>');
        return $cnt;
    }
    
    protected function onOpenCalendar($Event, $field)
    {
        $param = $Event->getParam(0);
        $cal = new Calendar(array_shift($param), array_shift($param));
        $cal->setNotEmpty();
        
        $box = Util\Tag::create('div')->att('class', 'osy-box');
        $box->add($cal->getTag($field->getName()));
        
        $field->getForm()->getResponse()->addContent($field->get('name'), ['.datInp .dat-content'=> $box]);
    }
    
    protected function onOpenResource($event, $field) {
        // viene caricata la nuova form con la chiave eventualmente indicata
        $param = $event->getParam(0);
        $newForm = new OpenFormDetail($field);
        $newForm->exec(['id'=>$param]);
        
    }
}
