<?php

namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\Request;
use Spinit\Util\ParamInterface;
use Spinit\Util\TriggerInterface;

interface ItemInterface extends ParamInterface, TriggerInterface {
    function build();
    function getView() : ViewInterface;
    function getForm() : FormInterface;
    function getName() : string;
    function get($name, $default='');
    function set($name, $value);
    function getItemList() : iterable;
    function addItem(ItemInterface $child) : ItemInterface;
    function setParent(ItemInterface $parent);
    function getParent() : ItemInterface;
    function setValue($value) : ItemInterface;
    function setValueByRequest(Request $req);
    function getValue();
    function getParam($name, $default = '', $notEmpty = false);
    function getBuilder() : BuilderInterface;
}