<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

interface FormAdapterInterface extends ViewAdapterInterface {
    function getModule() : ModuleAdapterInterface;
    function getModal($name) : ModalAdapterInterface;
}