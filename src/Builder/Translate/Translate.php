<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\Translate;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of SearchRunner
 *
 * @author ermanno
 */
class Translate extends Builder {

    public function build(ItemInterface $field, $withLabel = true) {
        $this->addResource('Translate.css');
        
        $div = $this->makeComponent($field, 'osy-translate osy-field mode-'.$field->get('mode'), $withLabel);
        if (!$field->get('name')) {
            $div->add('<div class="sys-error">Impostare il nome al componente</div>');
            return $div;
        }
        $box = $div->add(Tag::create('div'))->att('class', 'box');
        $list = $this->getInstance()->getDatasource()->query("
            select lower(ti.cod) as cod, ti.nme
            from osy_itm ti 
            inner join osy_itm tt on (ti.id_typ = tt.id and tt.urn = 'urn:opensymap.org@type#lang')");
        foreach($list as $rec) {
            $cnt = $box->add(Tag::create('div'));
            $cnt->add(Tag::create('label'))->add($rec['nme']);
            $cnt->add(Tag::create('input'))
                ->att('value', $field->getValue($rec['cod']))
                ->att('name', $field->getName().'['.$rec['cod'].']');
        }
        return $div;
    }

    public function checkValue(ItemInterface $item, $value)
    {
        if (is_string($value)) $value = json_decode($value, 1);
        return $value;
    }

}