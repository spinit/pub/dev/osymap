<?php

namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasApplicationTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ModuleAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Lib\Model\Model;
use Spinit\Lib\Model\ModelInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getInstance;

class Module implements ModuleInterface {

    use HasAdapterTrait {
        getAdapter as private _getAdapter;
    }
    /**
     * @var ApplicationInterface
     */
    private $app;

    public function __construct(ApplicationInterface $app, ModuleAdapterInterface $adapter)
    {
        $this->app = $app;
        $this->setAdapter($adapter);    
    }
    public function getName() : string {
        return $this->getAdapter()->getName();
    }
    public function getApplication () : ApplicationInterface {
        return $this->app;
    }
    public function getAdapter() : ModuleAdapterInterface {
        return $this->_getAdapter();
    }
    function getExtendList() :iterable {
        foreach($this->getAdapter()->getExtendList() as $extend) {
            yield new Extend($this, $extend);
        }
    }
    function getFormList() : iterable {
        return $this->getAdapter()->getFormList();
    }
    function getForm($name) : FormInterface {
        $form = new Form($this, $this->getAdapter()->getForm($name));
        $form->trigger('@init');
        return $form;
    }
    function getModelList() : iterable {
        return $this->getAdapter()->getModelList();
    }
    function getModel($name) : ModelInterface {
        $adapter = $this->getAdapter()->getModel($name);
        $datasource = $this->getApplication()->getInstance()->getDataSource($adapter->get('source'));
        $model = new Model($adapter, $datasource);
        $model->trigger('init');
        return $model;
    }
    function getBuilder($name): string
    {
        if ($builder = $this->getAdapter()->getBuilder($name)) {
            return $builder;
        }
        return $this->getApplication()->getBuilder($name);
    }
    public function getMenuBars($names) {
        return $this->getAdapter()->getMenuBars($names);
    }

    public function init() {
        $exe = function ($ctor) {
            getInstance($ctor, $this)->exec();
        };
        // prima di iniziare i moduli
        foreach($this->getAdapter()->getInitRunner('before') as $runner) $exe($runner);
        foreach($this->getModelList() as $modelName) {
            $model = $this->getModel($modelName);
            $model->init();
        }
        // terminate le inizializzazioni dei moduli
        foreach($this->getAdapter()->getInitRunner('after') as $runner) $exe($runner);
    }
}