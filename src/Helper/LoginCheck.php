<?php

namespace Spinit\Dev\Opensymap\Helper;
use Spinit\Dev\AppRouter\Core\HasInstanceTrait;
use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Util;
use function Spinit\Dev\AppRouter\debug;

class LoginCheck {
    use HasInstanceTrait;

    public function __construct(InstanceInterface $instance) {
        $this->setInstance($instance);
    }

    public function check($response, $login, $password) {

        $request = $this->getInstance()->getRequest();

        if (Util\getenv('PHPUNIT')) {
            $response->set('test', 'login-access');
        }

        $user = $this->getInstance()->getMainInteractor()->searchUser(
            $this->getInstance()->getInfo("id"),
            $login, 
            $password,
            function () {
                return $this->getInstance()->getDataSource();
            }
        );

        if (!$user) {
            throw new MessageError("Utente non trovato", "Attenzione");
        } 
        $this->getInstance()->setUser($user);
        
        $part = explode('/', $request->getPath());
        if (in_array(array_shift($part), ['login'])) {
            $response->addCommand('location.replace(args[0])', $request->getScheme().'://'.$this->getInstance()->makePath(''));
        } else {
            $response->addCommand("location.reload()");
        }
        
        $response->stop();
    }
}