<?php

namespace Spinit\Dev\Opensymap\Builder\InputText;

use Spinit\Dev\Opensymap\Builder\Util;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

class InputText extends Builder {
    public function build(ItemInterface $item, $withLabel = true)
    {
        $this->addResource('InputText.css');
        $this->addResource('InputText.js');

        $inp = $this->makeTextarea($item, $withLabel);
        if (!$inp) {
            $inp = new Tag('input');
            $inp->att('name', $item->getName());
            $inp->att('value', $item->getValue());
            $inp->att('type', $item->get('type'));
        }
        switch($item->get('type')) {
            case 'hidden':
                $inp->att('osy-item', $item->getName());
                $inp->att('id', $item->getName());
                $inp->att('class', 'osy-input-text');
                return $inp;
                
            case 'checkbox':
                $value = $item->get('value') || '1';
                if ($item->getValue() == $value) {
                    $inp->prp('checked');
                }
                $inp->att('value', $value);
                break;
            case 'money':
                $inp->att('type', 'text');
                $inp->att('pattern', '^[0-9]+(\.[0-9]*)?$');
                if ($item->getValue()) {
                    $item->setValue(number_format($item->getValue(), 2, '.', ''), 1);
                    $inp->att('value', $item->getValue());
                }
        }
        $inp->att('class', 'form-control', ' ');
        $div = $this->makeComponent($item, 'osy-input-text input-type-'.$item->get('type'));
        $div->add($inp);
        return $div;
    }

    private function makeTextarea($item, $withLabel) {
        if ($item->get('type') != 'text') return;
        if (!$item->get('rows')) return;
        $value = $item->getValue();
        if (is_array($value)) $value = json_encode($value);
        $inp = new Tag('textarea');
        $inp->att('rows', $item->get('rows'));
        $inp->att('name', $item->getName());
        $inp->add($value);
        $inp->att('dat', $value);
        return $inp;
    }
}