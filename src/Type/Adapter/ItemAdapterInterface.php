<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

interface ItemAdapterInterface extends AdapterInterface {
    function getParent() : ?ItemAdapterInterface;
    function getView() : ViewAdapterInterface;
    function getItemList() : iterable;
    function getName(): string;
    function getTrigger($event, $when) : string;
}