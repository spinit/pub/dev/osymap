<?php
namespace Spinit\Dev\Opensymap\Helper\Formatter;

use Spinit\Dev\AppRouter\FormatterIdentity;

use function Spinit\Dev\AppRouter\debug;

class DateFormatter extends FormatterIdentity {
    public function format($value): string
    {
        list($date, ) = $this->makeDate($value);
        return $date;
    }

    protected function makeDate($value) {
        if (!$value ) return ['', ''];
        list($value, $time) = explode(' ', $value.' ');
        $args = func_get_args();
        array_shift($args);
        $sep = array_shift($args)?:'/';
        $asep = $sep == '/' ? '-' : '/';
        $part = explode($sep, $value);
        if (count($part)<2) {
            // occorre convertire
            $apart = explode($asep, $value);
            $part = array_reverse($apart);
        }
        $part[1] = str_pad($part[1], 2, '0', STR_PAD_LEFT);
        if ($sep == '/') {
            $part[0] = str_pad($part[0], 2, '0', STR_PAD_LEFT);
        } else {
            $part[2] = str_pad($part[2], 2, '0', STR_PAD_LEFT);
        }
        return [implode($sep, $part), $time];
    }
}