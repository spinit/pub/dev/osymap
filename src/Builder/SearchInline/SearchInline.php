<?php
namespace Spinit\Dev\Opensymap\Builder\SearchInline;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of SearchInline
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class SearchInline extends Builder
{
    public function registerEvent() {
        $runner = SearchManager::getInstance();
        $this->bindExec('add', [$runner, 'onOpenFormDetail']);
        $this->bindExec('open', [$runner, 'onOpenSelfDetail']);
        $this->bindExec('load', [$runner, 'onLoadData']);
        $this->bindExec('select', [$runner, 'onSelectData']);
        $this->bindExec('@load', [$runner, 'onCheckValue']);
        $this->bindExec('@backValue', function($event, $field) {
            $field->setValue($event->getParam(0));
        });
        $this->bindExec('@check', function($event, $field) {
            // se non può essere vuoto si controlla il valore in post ... 
            // e in ultima istanza si prende il primo valore del datasource
            if ($field->get('empty') == 'no' and !$field->getValue('id')) {
                $value = $field->getValue();
                if (!is_array($value)) {
                    $value = ['id'=>$value];
                }
                if (implode('', $value)) {
                    $field->setValue($value, 1);
                    return;
                }
                $cmnt = new Util\Tag('');
                $field->setParam('cmnt', $cmnt);
                try {
                    $data = (new MakerData())->build($field, '', $cmnt);
                    if ($field->get('debug')) {
                        debug($data);
                    }
                } catch (\Exception $e) {
                    debug(['master' =>$field->getForm()->getUri(), 'field'=>$field->getName()], 
                          $e->getMessage(), 
                          $field->getForm()->getDataSource()->getCommandLast());
                }
                $rec = $data->current();
                $field->setValue($rec, 1);
                $field->setParam('cmnt', $cmnt);
            }
        });
    }
    public function isNull($field)
    {
        try {
            $field->trigger('@isNull'); // permette di impostare il valore del field prima di verificare se sia null
        } catch (\Exception $e) {
            return true;
        }
        return trim(implode('',$field->getValue())) == '';
    }
    public function checkValue(ItemInterface $field, $value) {
        $rec = $value;
        if (!is_array($value)) {
            $value = ['id' => $value, 'name' => ''];
        } else {
            $value = ['id' => array_shift($value), 'name' => array_shift($value)];
        }
        if (!$value['id']and !$value['name'] and $field->get('default')) {
            $value['id'] = $field->get('default');
        }
        if (Util\arrayGet($value, 'id') and !Util\arrayGet($value, 'name')) {
            $cmnt = new Util\Tag('');
            $field->setParam('cmnt', $cmnt);
            $rec = SearchManager::getInstance()->getRecord($field, $value['id'], $cmnt);
            $value = ['id'=>array_shift($rec), 'name'=>array_shift($rec)];
            //debug($value, $rec);
        }
        if ($field->getValue('id') and !$value['id']) {
            //debug($value);
        }
        $field->setParam('data', $rec);
        //if ($field->get('debug') and !$value['name']) debug([$field->getName(), $aa, $value]);
        return $value;
    }
    public function build(ItemInterface $field, $withTitle = true)
    {
        $this->addResource('SearchInline.js');
        $this->addResource('SearchInline.css');
        // contenitore principale
        $div = $this->makeComponent($field, 'osy-search-inline mode-'.$field->get('mode'), $withTitle);
        if (!$field->get('name')) {
            $div->add('<div class="sys-error">Impostare il nome al componente</div>');
            return $div;
        }
        $div->add($field->getParam('cmnt'));
        $header = $div->child(0);
        $cmp = $div->add(Util\Tag::create('div'))->att('class','osy-search-cmp');
        // è presente la form di dettaglio da aprire sugli elementi della lista?
        if ($field->get('formDetail')) {
            // non è esclusa l'aggiunta di elementi
            if ($field->get('add')!='no') {
                $header->add(Util\Tag::create('button'))
                    ->att('type', 'button')
                    ->att('class', 'btn btn-outline-info btn-sm osy-btn')
                    ->att('osy-event', 'add')
                    ->add('+');
            }
            $cmp->att('class', 'hasDetail', ' ');
            $viewItem = $cmp->add(Util\Tag::create('div'))
                ->att('class', 'osy-search-view')->att('osy-event', 'open');
            $viewItem->add('<i class="fa fa-fw fa-pencil"></i>');
            
        } else if ($field->get('add')=='1') {
            // formDetail non è impostata ma viene forzata la visualizzazione del pulsante di aggiunta
            $header->add(Util\Tag::create('button'))
                ->att('type', 'button')
                ->att('class', 'btn btn-outline-info btn-sm osy-btn')
                ->att('osy-event', 'add')
                ->add('+');
        }

        if ($header) {
            $header->add('<div style="clear:both"></div>');
        }
        if (!$field->get('readOnly')) {
            $cmp->add(Util\Tag::create('div'))->att('class','osy-search-btn')->add('<i class="fa fa-chevron-down fa-w-14"></i>');
        }
        $value = $this->checkValue($field, $field->getValue());
        // se il campo è stato impostato a mano con un valore singolo .. allora si cerca l'elemento da visualizzare
        $cmp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[id]')
            ->att('value', Util\arrayGet($value, 'id'))
            ->att('type', 'hidden')
            ->att('class','osy-search-id');
        $inp = $cmp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[name]')
            ->att('value', Util\arrayGet($value, 'name'))
            ->att('osy-id',$field->getName())
            ->att('autocomplete', 'off')
            ->att('class','osy-search-name py-2 form-control');
        if ($field->get('readOnly')) {
            $inp->prp('readonly');
            $inp->att('class','osy-readOnly', ' ');
        }
        if (!Util\arrayGet($value, 'id')) {
            $div->att('class', 'no-key', ' ');
        }
        // occore visualizzare l'elemento con extra dati?
        $div->add(Util\Tag::create('div'))->att('class','osy-search-place box-content')->add('');
        
        //$div->add($this->getMaskData($field));
        return $div;
    }
    
    public function loadFromModel(ItemInterface $field, ModelInterface $model) {
        if (!$model) {
            return;
        }
        $names = Util\asArray($field->get('field'), ',');
        $prop = $field->get('property');
        switch(count($names)) {
            case 0 :
                break;
            case 1 :
                if ($prop) {
                    $field->setValue($model->getPropertyValue($prop, array_shift($names)));
                } else {
                    $field->setValue($model->get(array_shift($names)));
                }
                break;
            default :
                if ($prop) {
                    $field->setValue([
                        'id'=>$model->getPropertyValue($prop, array_shift($names)), 
                        'name'=>$model->getPropertyValue($prop, array_shift($names))
                    ]);
                } else {
                    $field->setValue([
                        'id'=>$model->get(array_shift($names)), 
                        'name'=>$model->get(array_shift($names))
                    ]);
                }
                break;
        }
    }
    
    public function storeInModel(ItemInterface $field, ModelInterface $model) {
        if (!$model) {
            return;
        }
        $names = Util\asArray($field->get('field'), ',');
        $prop = $field->get('property');
        $types = Util\asArray($field->get('propertyType'), ',');
        switch(count($names)) {
            case 0 :
                break;
            case 1 :
                if ($prop) {
                    // if ($field->get('debug')) var_dump($field->getValue());
                    $model->setPropertyValue($prop, $field->getValue(), array_shift($names), array_shift($types));
                } else {
                    $model->set(array_shift($names), $field->getValue('id'));
                }
                break;
            default :
                if ($prop) {
                    $model->setPropertyValue($prop, $field->getValue('id'), array_shift($names), array_shift($types));
                    $model->setPropertyValue($prop, $field->getValue('name'), array_shift($names), array_shift($types));
                } else {
                    $model->set(array_shift($names), $field->getValue('id'));
                    $model->set(array_shift($names), $field->getValue('name'));
                }
                break;
        }
    }
}
