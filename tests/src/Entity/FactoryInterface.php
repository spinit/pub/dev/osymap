<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FactoryInterface;

class FactoryDummy implements FactoryInterface {
    function getApplication($name) : ApplicationInterface {
        return new ApplicationDummy();
    }
    function getMenu() : iterable {
        return [];
    }
    function getBuilder($name) : BuilderInterface {
        return new BuilderDummy();
    }
}