<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\ExtendInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;
use Spinit\Dev\Opensymap\Type\OverlayInterface;

class ApplicationDummy implements ApplicationInterface {
    public function run(Request $request) : Response {
        return new Response();
    }
    public function getForm($name) : FormInterface {
        return new FormDummy();
    }
    public function getInstance() : OpensymapInterface {
        throw new \Exception();
    }
    public function getModuleList() : iterable {
        return [];
    }
    public function getModule($name) : ModuleInterface {
        throw new \Exception();
    }
    public function getExtendList() : iterable {
        return [];
    }
    public function addExtend(ExtendInterface $extend) {
        return null;
    }
    public function getBuilder($name) : ?BuilderInterface {
        return null;
    }
    public function getOverlay($name) : OverlayInterface {
        throw new \Exception();
    }
    public function getOverlayList() : iterable {
        return [];
    }
}