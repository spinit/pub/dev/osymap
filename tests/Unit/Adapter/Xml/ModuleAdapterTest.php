<?php
namespace Spinit\Test\Unit\Adapter\Xml;

use PHPUnit\Framework\TestCase;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;

use function Spinit\Dev\AppRouter\debug;

class ModuleAdapterTest extends TestCase {
    private $obj;
    private $app;

    protected function setUp() : void
    {
        parent::setUp();
        $this->app = $app = new TestApplicationAdapter();
        $this->obj = $app->getModule('Core');
    }
    
    public function testApplication() {
        $this->assertEquals($this->app, $this->obj->getApplication());
    }
    
    public function testBuilder() {
        $this->assertEquals('Builder:OK', $this->obj->getBuilder('builder-ok'));
    }

    public function testAttribute() {
        $this->assertEquals('', $this->obj->get('key'));
        $this->obj->set('key', 'test');
        $this->assertEquals('test', $this->obj->get('key'));
    }

    public function testForm() {
        $form = $this->obj->getForm('Test');
        $this->assertEquals('Core:Test', $form->getName());
        $this->assertEquals('Test', $form->getName(false));
    }

    public function testGetFormList() {
        $list = $this->obj->getFormList();
        $this->assertContains('Test', $list);
    }
}