<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

interface ModalAdapterInterface extends ViewAdapterInterface {
    function getForm() : FormAdapterInterface;
    function getCommandList() : iterable;
    function getInitCode(): string;
}