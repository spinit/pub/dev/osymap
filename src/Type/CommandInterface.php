<?php

namespace Spinit\Dev\Opensymap\Type;

interface CommandInterface {
    public function exec($param = null);
}