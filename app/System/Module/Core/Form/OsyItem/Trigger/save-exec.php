<?php

use Webmozart\Assert\Assert;

$model = $this->getModel();
if ($this->getItem('hdn_par')->getValue()) {
    Assert::notEmpty($this->getItem('txt_cod')->getValue(), "Campo codice non può essere vuoto");
    $par = $this->getApplication()->getModel('OsyItem');
    $par->load($this->getItem('hdn_par')->getValue());
    $urn = $par->get('urn').'#'.strtolower($this->getItem('txt_cod')->getValue());
    $model->set('urn', $urn);
    $model->set('id', xid($urn));    
}
