<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of ItemController
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class ItemController
{
    private $item;

    public function __construct($item) {
        $this->item = $item;
    }
    public function getItem() {
        return $this->item;
    }
    
    protected function makeParam($field, $args)
    {
        $name = $field->getName();
        $value = $field->getValue();
        array_shift($args); // $field
        array_shift($args); // $item
        if (count($args)) {
            $name = array_shift($args);
        }
        if (count($args)) {
            $value = array_shift($args);
        }
        return [$name, $value];
    }
    
    abstract function render($field, $item);
}
