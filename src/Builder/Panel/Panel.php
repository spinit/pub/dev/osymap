<?php
namespace Spinit\Dev\Opensymap\Builder\Panel;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util;
use Spinit\Dev\Opensymap\Builder\Util as BuilderUtil;
/**
 * Description of Panel
 *
 * @author ermanno
 */
class Panel extends Builder {
    //put your code here
    public function build(ItemInterface $field, $withLabel=true) {

        $this->addResource('Panel.css');

        $okStruct = !$field->get('noStruct');
        $main = $this->makeComponent($field, 'osy-panel row ');
        if ($okStruct) {
            $main->child(0)->remove();
        }
        $cc = 0;
        if (!$field->get('noLabel') AND $withLabel AND $field->get('label')) {
            $main->add(Util\Tag::create('div'))
                    ->att('class', 'osy-header col-12')
                    ->add('<label>'.$field->get('label').'</label>');
        }
        if ($field->get('noSpace')) {
            //$main->att('class', 'osy-no-field', ' ');
        }
        if ($field->get("content")) {
            $main->add(Util\Tag::create('div'))
                    ->att('class', 'col-12')
                    ->add($field->get("content"));
        }
        foreach($field->getItemList() as $child) {
            $cc+= 1;
            if (!$child->isVisible()) {
                continue;
            }
            $cnt = $child->build();
            if ($child->getBuilder()->noWrap($child)) {
                $main->add($cnt);
            } else {
                $col = $main->add(Util\Tag::create('div'))->att('class', $okStruct ? 'col col-12': '');
                $this->makeColumn($child, $col);
                $col->add($cnt);
            }
        }
        $main->att('data-info-cc', $cc);
        return $main;
    }

    private function makeColumn($child, $col) {
        $width = trim($width = $child->get('col'));
        if ($width) {
            foreach(explode(' ', $width) as $typ) {
                $col->att('class', "col-{$typ}", ' ');
            }
        }
    }

    public function initTag($field, $div) {}
}
