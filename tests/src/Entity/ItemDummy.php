<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;

class ItemDummy implements ItemInterface {
    function getForm() : FormInterface {
        throw new \Exception();
    }
    function getName() : string {
        return 'dummy';
    }
    function get($name) {
        return $name;
    }
    function getItemList() : iterable {
        return [];
    }
    function addItem(ItemInterface $child) : ItemInterface {
        return $child;
    }
}