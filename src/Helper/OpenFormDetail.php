<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Helper;

/**
 * Description of FieldFormDetail
 *
 * @author ermanno
 */

use ArrayObject;
use Spinit\Dev\AppRouter\Data;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Util;
use Webmozart\Assert\Assert;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ModalInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\asArray;
use function Spinit\Util\normalize;

/**
 * Scopo di OpenFormDetail è :
 *  - di caricare la form richiesta e generare il contenuto da inviare per la sua visualizzazione
 *  - predisporre lo stato nella history del brower per poter utilizzare i tasti freccia 
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class OpenFormDetail
{
    private $field;
    private $form;
    private $url;
    private $dataInit;
    private $newForm;
    private $query;

    public function __construct(ItemInterface $field, $form = '', $dataInit = [])
    {
        $this->field = $field;
        $this->form = $form;
        $this->url = $field->getForm()->getUrl();
        $this->dataInit = $dataInit;
        $this->newForm = '';
        $this->setQuery([]);
    }
    
    public function setQuery($query, $a = '') {
        if (is_array($query)) {
            $this->query = $query;
        } else {
            $this->query[$query] = $a;
            if (!$a) unset($this->query[$query]);
        }
    }
    public function setNewForm($newForm) {
        $this->newForm = $newForm.'';
        return $this;
    }

    public function exec($url='', $event = '')
    {
        $formName = $this->form ?: $this->field->get('formDetail');
        if(!$formName) {
            return;
        }
        if (is_array($url)) {
            if (is_array($url)) ksort($url);
            $url = base64_encode(json_encode($url));
        }

        $newForm = $this->getFormDetail($formName);
        $this->execNewForm($newForm, $url, $event);
    }

    private function saveCurrentFormData() {

        $form = $this->field->getForm();
       
        // vengono salvati i dati ... se c'è un errore viene stoppato il processo
        $form->trigger('save', [], function() use ($form) {
            $form->getResponse()->stop();
        });
        $this->url = $form->getUrl();
    }        

    private function getFormDetail($formName) {
        $view = $this->field->getForm();
        $app = $view->getApplication();

        $formDetail = explode('/Form/', $formName);
        if (count($formDetail)>1) {
            $app = $view->getApplication()->getInstance()->getApplication(array_shift($formDetail));
            $formDetail = Util\biName(array_shift($formDetail));
        } else {
            if ($view instanceof ModalInterface) {
                $getForm = function($view) : FormInterface {
                    return $view->getForm();
                };
            } else {
                $getForm = function($view) : FormInterface {
                    return $view;
                };
            }
            $formDetail = Util\biName($formName, $getForm($view)->getModule()->getAdapter()->getName());
        }
        $newForm = $app->getForm($formDetail);
        //$newForm->setReferer($form);
        return $newForm;
    }

    private function execNewForm($newForm, $url, $event) {
        // prima si salvano i vecchi dati
        $this->saveCurrentFormData();
        
        // la nuova form rimpiazza quella da cui è stata invocata o si accoda nella history?
        $newForm->set('replace', $this->field->get('formReplace'));
        $request = $this->makeRequest($url, $event);

        $newForm->setResponse($this->makeResponse());
        foreach($this->dataInit as $field => $value) {
            $newForm->getItem($field)->setValue($value, 1);
        }
        $newForm->set('debug', 1);
        $response = $newForm->run($request);
        $response->stop();
    }
    
    private function makeResponse() {
        $form = $this->field->getView()->getForm();
        $app = $form->getApplication();

        $request = $form->getRequest();
        $init = $request->getPost()?:[];
        // occorre memorizzare nella stato della pagina corrente il nome del componente
        // che ha effettuato la richiesta di dettaglio
        $init['_']['osy']['back-name'] = $this->field->getName(); // componente che ha chiamato la form child
        // viene memorizzato se è stata richiesta
        $init['_']['osy']['back-newForm'] = $this->newForm;
        $model = $form->getModel();
        $dataExec = [[
            "cmd",
            "window.history.replaceState({'init':args[0]}, args[2], args[1]); document.title = args[2]; // ".__FILE__.':'.__LINE__,
            $init,
            $this->url,
            normalize($form->get('title'), $model ? $model->asArray() : [])[0],
        ]];

        $response = new Response();
        $response->set('data,exec', $dataExec);
        return $response;
    }

    private function makeRequest($url, $event) {
        $form = $this->field->getForm();
        $request = $form->getRequest();
        $data = $this->getDataDetail($this->field, $request);
        $this->field->trigger('@checkFormData', [$data]);
        $newRequest = new Request('POST', [$this->url, $url]);
        $newRequest->setPost($data);
        $newRequest->setQuery($this->query);
        $newRequest->setApp($this->getSysDetail($this->field, $event));
        $newRequest->setHeaders(array_merge($request->getHeaders(), ['X-Opensymap-Call'=>'']));
        return $newRequest;
    }
    /**
     * Carica i valori da dover passare alla from
     * In "formData" c'è la lista dei campi della form che devono essere riempiti con il valore dei relativi campi locali
     * in "formValue" c'è la lista dei campi della form che devono ricevere il valore indicato 
     * @param type $field
     * @return \Spinit\Util\Dictionary
     */
    private function getDataDetail($field, $request)
    {
        $data = new ArrayObject();
       
        $conf = $field->get('formData');
        if ($conf and !is_array($conf)) {
            $conf = json_decode($conf, 1);
        }
        if ($field->get('formData') and !$conf) {
            throw new \Exception('"formData" non formattato correttamente : '.$field->get('formData'));
        }
        foreach($conf?:[] as $detField => $locField) {
            if (is_array($locField)) {
                foreach($locField as $typ=>$name) {
                    if ($typ == 'sys') {
                        $data[$detField] = $request->getApp($name);
                    } else {
                        $data[$detField] = $request->getPost($name);
                    }
                }
            } else {
                $locAr = asArray($locField, '.');
                $item = $field->getForm()->getItem(array_shift($locAr));
                $data[$detField] = $item->getValue($locAr);
            }
        }
        $conf = $field->get('formValue');
        if ($conf and !is_array($conf)) {
            $conf = json_decode($conf, 1);
        }
        if ($field->get('formValue') and !$conf) {
            throw new \Exception('"formValue" non formattato correttamente : '.$field->get('formValue'));
        }
        foreach($conf?:[] as $detField => $value) {
            // se è un array-lista allora il primo elemento è la funzione da richiamare sui restanti
            if (is_array($value) and array_key_exists(0, $value)) {
                $func = array_shift($value);
                $path = explode('.', $func);
                // se la funzione contiene un punto .. allora è l'invocazione di un metodo di una classe [class:path:di:una:classse.metodo]
                if (count($path)>1) {
                    array_unshift($value, $field);
                    $object = Util\getInstance($path[0]);
                    $data[$detField] = call_user_func_array([$object, $path[1]], $value);
                } else {
                    // altrimenti è una semplice funzione [md5]
                    $data[$detField] = call_user_func_array($func, $value);
                }
            } else {
                $data[$detField] = $value;
            }
        }
        return $data;
    }
    
    private function getSysDetail($field, $event)
    {
        // viene memorizzato nello stato che questo field ha effettuato la richiesta 
        // del dettaglio .. e quindi potrebbe essere interessato alla sua chiave
        $sys = ['back'=>$field->getName()];
        $sys['user'] = $field->getForm()->getApplication()->getInstance()->getUser();
        if ($event) {
            $sys['osy']['event'] = $event;
        }
        return $sys;
    }
}
