<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasModuleTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ExtendAdapterInterface;

use function Spinit\Dev\AppRouter\fspath;

class ExtendAdapter implements ExtendAdapterInterface {

    use HasModuleTrait;
    use AttributeTrait;

    public function __construct(ModuleAdapter $module, $index) {
        $this->setModule($module);
        $this->index = $index;    
    }
    public function getApplicationName() : string {
        return (string) $this->index;
    }
    public function getApplication() : ApplicationAdapterInterface {
        $appFile = fspath($this->getModule()->getRoot(), 'Extend', (string) $this->index['dir']);
        return new ApplicationAdapter([$appFile]);
    }
}