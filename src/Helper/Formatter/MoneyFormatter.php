<?php

namespace Spinit\Dev\Opensymap\Helper\Formatter;

use Spinit\Dev\AppRouter\FormatterIdentity;

use function Spinit\Dev\AppRouter\debug;

class MoneyFormatter extends FormatterIdentity {
    function format($value): string
    {
        return number_format(floatval($value), 2, ',', '.');
    }
}