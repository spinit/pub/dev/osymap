<?php
namespace Spinit\Test\Builder\DatagridTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\ResponseException;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class DatagridTest extends TestCase {

    private $osy;
    private $req;

    protected function setUp() : void
    {
        parent::setUp();
        // 
        $this->osy = $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $osy->setDataSource('', new DataSource('test'));
        $this->req = $osy->getFactory()->makeRequest('POST', 'app/app@test/DataGrid');
    }
    
    public function testView(){
        $res = $this->osy->run($this->req);
        $this->assertStringContainsString('Data Grid', $res->get('content'));
        //$this->assertStringContainsString('Ciao', $res->get('content'));
        //$this->assertStringContainsString('Ok', $res->get('content'));
    }

    public function testOpen() {
        $this->req->setApp('osy,init', '1');
        $this->req->setApp('osy,event', 'open');
        $this->req->setApp('osy,item', 'dgr');
        $this->req->setApp('osy,param', json_encode(['id'=>1]));
        try {
            $res = $this->osy->run($this->req);
            $this->fail('No Response was created');
        } catch (ResponseException $e) {
            $res = $e->getResponse();
        }
        $this->assertStringContainsString('Panel Tab', $res->get('content'));
        $this->assertStringContainsString('test1', $res->get('content'));
        $this->assertStringContainsString('test2', $res->get('content'));
    }
}