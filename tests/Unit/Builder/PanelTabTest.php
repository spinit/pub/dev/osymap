<?php
namespace Spinit\Test\Builder\DatagridTest;

use PHPUnit\Framework\TestCase;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class PanelTabTest extends TestCase {

    public function testView(){
        $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $osy->setDataSource('', new DataSource('test'));
        $req = $osy->getFactory()->makeRequest('POST', 'app/app@test/PanelTab');
        $res = $osy->run($req);
        $this->assertStringContainsString('Panel Tab', $res->get('content'));
        $this->assertStringContainsString('test1', $res->get('content'));
        $this->assertStringContainsString('test2', $res->get('content'));

    }
}