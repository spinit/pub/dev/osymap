/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {

    
    $(document).off('.osy-datetime');

    function moveOption(event, src)
    {
        switch(event.keyCode) {
            case 13: // INVIO
            case 37:
            case 39:
            case 38:
            case 40:
                return true;
        }
        return false;
    }
$(document).on('keyup.osy-datetime', '.osy-datetime .osy-search-name', function(event) {
    // se si digita nella casella di testo allora si caricano i dati
    var el = $(this);
    event.stopPropagation();
    if (event.keyCode != 8) {
        if (/^[0-9]{2}$/.test(this.value)) {
            this.value += '/';
        }
        if (/^[0-9]{2}\/[0-9]{2}$/.test(this.value)) {
            this.value += '/';
        }
        if (/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}$/.test(this.value)) {
            this.value += ':';
        }
    }
    $(this).closest('.osy-datetime').find('.osy-search-id').val('');
    var src = el.closest('.osy-datetime');
    if (!moveOption(event, src)) {
        // se non è un carattere di movimento allora occorre aggiornare il valore del componente
        src.trigger('check-data');
    }
});

$(document).on('setDate.osy-datetime', '.osy-datetime .osy-box', function() {
//    $(this).trigger('close');
});

$(document).on('setDate.osy-datetime', '.osy-datetime', function(event, datVal) {
    let hval = $('.hval', this).val();
    let arDatVal = typeof(datVal) == typeof('') ? JSON.parse(datVal) : datVal;
    $(this).trigger('select-data', [(arDatVal[0]+' '+(hval?hval:'')).trim(), (arDatVal[1]+' '+(hval?hval:'')).trim()]);
});

$(document).on('setMonth.osy-datetime', '.osy-datetime', function(event, month) {
    $(this).trigger('load-data', [month]);
});

$(document).on('click.osy-datetime', '.osy-datetime .hour', function(event) {
    var hour = $(this);
    if ($(event.target).is('.help')) {
        $('input', hour).val($(event.target).html());
    }
});
// se si clicca sul pulsante di ricerca si caricano i dati
$(document).on('click.osy-datetime', '.osy-datetime', function(event) {
    event.stopPropagation();
    event.preventDefault();
    if ($(event.target).is('.osy-search-btn')) {
        $('.osy-search-name', this).focus();
        $(this).trigger('load-data');
    } else if ($(event.target).is('.osy-search-item')) {
        $(this).trigger('select-data', [$(event.target).attr('data-id'), $(event.target).attr('data-name')]);
    }
});


// lettura dati
$(document).on('load-data.osy-datetime', '.osy-datetime', function(event, month) {
    var el = this;
    el.xhr && el.xhr.abort && el.xhr.abort();
    el.xhr = desktop.sendEvent(this, ['load', month ? month : ''], desktop.getFormData(el));
});

// verifica dati
$(document).on('check-data.osy-datetime', '.osy-datetime', function(event) {
    var el = this;
    el.xhr && el.xhr.abort && el.xhr.abort();
    el.xhr = desktop.sendEvent(
        this,
        'check',
        desktop.getFormData(el)
    );
});

// seleziona dato
$(document).on('select-data.osy-datetime', '.osy-datetime', function(event, arDate) {
    event.stopPropagation();
    var el = this;
    $(el).removeClass('modified');
    $('.osy-search-id', el).val(arDate[0]);
    $('.osy-search-name', el).val(arDate[1]);
    el.xhr && el.xhr.abort && el.xhr.abort();
    el.xhr = desktop.sendEvent(this, ['select', arDate[0].trim()], desktop.getFormData(this));
    $('.osy-box', this).trigger('close');
});


$(document).on('setValue.osy-datetime', '.osy-datetime', function(event, data) {
    data = data ? data : {'id':'', 'name':''};
    $('.osy-search-id', this).val(data['id']);
    $('.osy-search-name', this).val(data['name']);
    if (!data['id'] && data['name']) {
        $(this).addClass('error');
    } else if (data['err'] && data['err']['warning_count']) {
        $(this).addClass('error');
    } else if (data['id']) {
        $(this).removeClass('error');
    } 
});

})(desktop.$);