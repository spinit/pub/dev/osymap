<?php
namespace Spinit\Test;

use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Request;

use function Spinit\Util\arrayGet;

class InteractorDummy implements MainInteractorInterface {

    private $user = null;
    private $authUsers = null;

    public function install(iterable $instanceList) {

    }
    public function initRoute(AddRouteInterface $main) {

    }
    public function publish(string $topic, array $param, $realm = '') {

    }
    public function getConsole() {

    }
    public function getHome() {
        return "/tmp";
    }
    public function decrypt($token) {
        return $token;
    }
    public function encrypt($data) {
        return $data;
    }

    public function getUser($request)
    {
        return $this->user;
    }
    public function setUser($request, $user, $path)
    {
        $this->user = $user;       
    }
    public function hasUser() {
        return !!$this->user;
    }
    public function authUsers($users) {
        $this->authUsers = $users;
    }
    public function searchUser($id_url, $name, $passwd, $ds = null)
    {
        $list = $this->authUsers?:[];
        if (array_key_exists($name, $list) and $passwd === arrayGet($list[$name], 'password',$list[$name])) {
            if (is_array($list[$name])) {
                return $list[$name];
            }
            return ['user'=>$name];
        }
        return null;
    }
}