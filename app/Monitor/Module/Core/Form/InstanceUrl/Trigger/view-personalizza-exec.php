<?php
use Spinit\Dev\Opensymap\Exception\MessageError;
use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGetAssert;

$apps = arrayGetAssert(
    json_decode($this->getItem("txt_ini")->getValue(), true),
    "apps");
$apps[] = "system@opensymap.org";

$formsList = [];
$instance = $this->getApplication()->getInstance();

//debug($apps, array_keys($instance->getApplicationList()));

$mask = '<div class="osy-item" style="text-align:left"><code><a osy-event href="{{url}}">{{name}}</a></code></div>';
foreach($apps as $app) {
    $initApp = $instance->getFactory()->getApplication($app);
    if ($initApp) try {
        $initApp->setExternApplication($this->getApplication());
        $initForm = $initApp->getForm("Admin:InitInstanceUrl");
        $initForm->setPkey($this->getPkey());
        $formsList[] = str_replace(
            ["{{url}}", "{{name}}"], 
            [$initForm->getUrl(), $initApp->getName()], 
            $mask
        );
    } catch(\Exception $e) {}
}
if (count($formsList)) {
    $response->addCommand("desktop.openBox(this, args[0])", $formsList);
} else {
    throw new MessageError("Nessun Configuratore trovato");
}

