<?php

function checkNewAddressMapBox($addr) {
    $token = 'pk.eyJ1IjoidGltZWFnZW5jeSIsImEiOiJja280aDI4OXQxaXluMnhwZ3pxeTg3cGwwIn0.J9IYcF9jWp3C1M76BOkaCQ';
    $link = "https://api.mapbox.com/geocoding/v5/mapbox.places/[address].json?access_token=".$token;
    $src = \str_replace('[address]', urlencode($addr), $link);
    $dataStr = \file_get_contents($src);
    return json_decode($dataStr, 1)['features'][0];
}

function checkNewAddressGMap($addr, $token) {
    $link = "https://maps.googleapis.com/maps/api/geocode/json?address=[address]&key=".$token;
    $src = \str_replace('[address]', urlencode($addr), $link);
    $dataStr = \file_get_contents($src);
    return json_decode($dataStr, 1)['results'][0];
}
/*
// ricerca coordinate dei nuovi indirizzi
foreach($rs as $rec) {
    $addr = $rec['adr'].($rec['num']?', '.$rec['num']:'').', '.$rec['zip'].' '.$rec['cty_nme'].', '.$rec['nat_nme'];
    $data = 
    $dataStr = file_get_contents($src);
    $data = json_decode($dataStr, 1);
    foreach($data['results'] as $f) {
        $rec['lng'] = $f['geometry']['location']['lng'];
        $rec['lat'] = $f['geometry']['location']['lat'];
        $stato = '';
        foreach($f['address_components'] as $c) {
            if ($c['types'][0] == 'country') {
                $stato = $c['short_name'];
                break;
            }
        }
        $DS->update('osy_adr', ['lat'=>$rec['lat'], 'lng'=>$rec['lng'], 'prp'=>$data['results'], 'nat_ext'=>$stato], $rec['id_adr']);
        // viene preso solo il primo
        break;
    }
}
*/
$form = $this;

$rec = [
    'adr' => $this->getField('txt_adr')->getValue(),
    'num' => $this->getField('txt_num')->getValue(),
    'zip' => $this->getField('txt_zip')->getValue(),
    'cty_nme' => $this->getField('txt_cty')->getValue(),
    'nat_nme' => $this->getField('txt_nat')->getValue()
];

$addr = $rec['adr'].($rec['num']?', '.$rec['num']:'').', '.$rec['zip'].' '.$rec['cty_nme'].', '.$rec['nat_nme'];

$token = $form->getInstance()->getEnv('google:gmap:api-key');
if ($token) {
    $data = checkNewAddressGMap($addr, $token);
    $lng = $data['geometry']['location']['lng'];
    $lat = $data['geometry']['location']['lat'];
} else {
    $data = checkNewAddressMapBox($addr);
    $lng = $data['center'][0];
    $lat = $data['center'][1];
}

$hid = $this->getField('hdn_id')->getValue();

$this->getField('txt_lng')->setValue($lng, 1);
$this->getField('txt_lat')->setValue($lat, 1);
$this->getField('txt_prp')->setValue(json_encode($data, JSON_PRETTY_PRINT), 1);

$this->getResponse()->addCommand("$('#map_adr').trigger('setMarker', ['{$hid}', [{$lng}, {$lat}], 1])");
