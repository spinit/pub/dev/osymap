<?php

namespace Spinit\Dev\Opensymap\App\Monitor\Source\Command;

use Spinit\Dev\AppRouter\Entity\MainInteractorDataSource\Interactor;
use Spinit\Dev\AppRouter\Response;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getInstance;

class InstanceUpgrade {

    private $interactor;
    private $idInstance;
    private $datasource;

    public function __construct(Interactor $interactor, $datasource, $idInstance)
    {
        $this->interactor = $interactor;
        $this->datasource = $datasource;
        $this->idInstance = $idInstance;
    }
    
    public function exec() {
        foreach($this->interactor->getInstanceUrlList($this->idInstance) as $rec) {
            $manager = getInstance($rec['manager'], $this->interactor, $rec['conf'], $rec['init'], $rec);
            $manager->setDataSource('main', $this->interactor->getDataSource());
            $manager->setDataSource('', $this->datasource);
            $manager->install();
        }
        $response = new Response();
        $response->set('message', 'Operazione effettuata');
        $response->stop();
    }
}