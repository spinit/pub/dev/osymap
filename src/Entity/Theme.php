<?php

namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Type\ThemeInterface;

use function Spinit\Dev\AppRouter\fspath;
class Theme implements ThemeInterface {
    private $root;
    private $std;
    public function __construct($home, $root) {
        $this->std = fspath(dirname(dirname(__DIR__)), 'theme', 'osymap');
        $base = "";
        if (is_dir(fspath($home, "theme"))) {
            $base = fspath($home, "theme");
        }
        if ($root) {
            if ($root[0] == "/") {
                $base = $root;
            } else if ($base) {
                $base = fspath($base, $root);
            }
        }
        $this->root = $base ?: $this->std;
    }
    public function getPath($name) : string {
        return fspath($this->root, $name);
    }
    public function getStdPath($name) : string {
        return fspath($this->std, $name);
    }

}