<?php
namespace Spinit\Dev\Opensymap\Builder;

use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util\Tag;

class Util {
    static function makeLabelBody($class, $id, $label, $cnt = null) {
        $div = new Tag('div', $id);
        $div->att('osy-item', $id)->att('class', $class);
        $title = $div->add(new Tag('div'))->att('class', 'osy-title');
        $title->add(new Tag('label'))->add($label);
        $body = $div->add(new Tag('div'))->att('class', 'body');
        if ($cnt) $body->add($cnt);
        return $div;
    }
}