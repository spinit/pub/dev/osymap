tinymce.PluginManager.add('osy-editor-box', function(editor, url) {
    var openConfiguration = function (col) {
      return editor.windowManager.open({
        title: 'Column Configuration',
        body: {
          type: 'panel',
          items: [
            {
              type: 'input',
              name: 'cls',
              label: 'Class'
            }
          ]
        },
        initialData : {
          cls : col.className
        },
        buttons: [
          {
            type: 'cancel',
            text: 'Close'
          },
          {
            type: 'submit',
            text: 'Save',
            primary: true
          }
        ],
        onSubmit: function (api) {
          var data = api.getData();
          // Insert content when the window form is submitted
          col.className = data.cls;
          api.close();
        }
      });
    };

    let maskBoxCol = '<div class="col-sm" role="box"><div></div></div>';
    let maskBoxRow = '<div class="row">'+maskBoxCol+'</div>';
    let maskBox = '<div class="container-fluid box ">'+maskBoxRow+"</div>";
    
    function mkTag(tag, cls, html) {
      let box = editor.contentDocument.createElement(tag ? tag : 'div');
      cls && (box.className = cls);
      html && (box.innerHTML = html);
      if (!tag) box = box.firstChild;
      return box;
    }

    function activeCol (col) {
      if (!isBoxCol(col)) {
        col = col.querySelector('[role=box]');
      }
      editor.execCommand('mceSelectNode', false, col.querySelector('div'));
      editor.execCommand('mceStartTyping');
    }

    function isBox(itm) {
      return !! itm.classList.contains('box');
    }

    function isBoxRow(itm) {
      return itm.classList.contains('row') && isBox(itm.parentNode);
    }

    function isBoxCol(itm) {
      return itm.getAttribute('role') == 'box' && isBoxRow(itm.parentNode);
    }

    // inserisce colonne in una riga o accanto ad una colonna
    function addCol(itm, left) {
      col = mkTag('', null, maskBoxCol);
      itm.insertAdjacentElement(left ? 'beforebegin' : 'afterend', col);
      activeCol(col);
    }
    function rmCol(col) {
      let row = col.parentNode;
      let box = col.closest('.osy-editor-box');
      col.remove();
      if (!row.querySelector('[role=box]')) {
        // occorre cancellare tutto il box 
        row.remove();
        if (!box.querySelector('[role=box]')) {
          box.remove();
        }
      }
    }
    // inserisce una riga sopra o sotto
    function addRow(itm, top) {
      row = mkTag('', null, maskBoxRow);
      itm.parentNode.insertAdjacentElement(top ? 'beforebegin' : 'afterend', row);
      activeCol(row);
    }

    // Add a button that opens a window
    editor.ui.registry.addButton('osy-editor-box', {
      text: 'Box',
      onAction: function () {
        let box = mkTag('div', 'osy-editor-box', maskBox);
        let base = editor.selection.getNode();
        base.append(box);
        base.append(mkTag('br'));
        activeCol(box);
      }
    });

    editor.on('keyup', function(event) {
      // console.log(event);
      if(event.which==13) {
        event.preventDefault();
        event.stopPropagation();
        // console.log(event.which);
        return false;
      }
    });

    function getContextCol(col) {
      const contextMenu = [
        {
          type : "submenu",
          text : "Column",
          getSubmenuItems : () => [
            {
              type : "item",
              text : "Copy",
              onAction : function() {
                Desktop.setClipboard(col.querySelector('div').innerHTML, {'context' : 'col-inner'});
                Desktop.showMessage("Colonna Copiata", 1000);
              }
            },
            {
              type : "item",
              text : "Config",
              onAction : function() {
                openConfiguration(col);
              }
            },
            {
              type : "separator"
            },
            {
              type : "item",
              text : "Add Rigth",
              onAction : function() {
                addCol(col);
              }
            },
            {
              type : "item",
              text : "Add Left",
              onAction : function() {
                addCol(col, 1);
              }
            }
          ]
        },
        {
          type : "submenu",
          text : "Row",
          getSubmenuItems : () => [
            {
              type : "item",
              text : "Copy",
              onAction : function() {
                Desktop.setClipboard(col.parentNode.innerHTML, {'context' : 'row-inner'});
                Desktop.showMessage("Riga Copiata", 1000);
              }
            },
            {
              type : "separator"
            },
            {
              type : "item",
              text : "Add Top",
              onAction : function() {
                addRow(col, 1);
              }
            },
            {
              type : "item",
              text : "Add Bottom",
              onAction : function() {
                addRow(col);
              }
            }
          ]
        },
        {
          type : "submenu",
          text : "Box",
          getSubmenuItems : () => [
            {
              type : "item",
              text : "Copy",
              onAction : function() {
                Desktop.setClipboard(col.closest('.osy-editor-box').outerHTML, {'context' : 'box-inner'});
                Desktop.showMessage("Box Copiato", 1000);
              }
            },
            {
              type : "separator"
            },
            {
              type : "item",
              text : "Add",
              onAction : function() {
                addBox(col);
              }
            }
          ]
        },
        {
          type : "separator"
        },
        {
          type : "item",
          text : "Paste",
          onAction : function() {
            let info = Desktop.getClipboardInfo();
            let cnt = col.querySelector('div');
            switch (info.context) {
              case 'row-inner':
                let box = mkTag('div', 'osy-editor-box', maskBox);
                cnt.insertAdjacentElement('beforeend', box);
                console.log( box.querySelector('[role=box]').parentNode, Desktop.getClipboardText());
                box.querySelector('[role=box]').parentNode.innerHTML = Desktop.getClipboardText();
                break;
              default:
                cnt.innerHTML = Desktop.getClipboardText();
                break;
            }
          }
        },
        {
          type : "separator"
        },
        {
          type : "item",
          text : "Delete",
          onAction : function() {
            rmCol(col);
          }
        }
      ];
      return contextMenu;
    }
    function getContextHml(element) {
      if(element.closest('.osy-editor-box')) {
        return '';
      }
      return [
        {
          type : "item",
          text : "Copy",
          onAction : function() {
            console.log(arguments);
          }
        },
        {
          type : "item",
          text : "Paste",
          onAction : function() {
            console.log(arguments);
          }
        }
      ]
    }
    editor.ui.registry.addContextMenu('osy-editor-box', {
        update: function (element) {
            return isBoxCol(element) ? getContextCol(element) : getContextHml(element);
        }
      });
    /*
    // Adds a menu item, which can then be included in any menu via the menu/menubar configuration
    editor.ui.registry.addMenuItem('osy-editor-box', {
      text: 'Example plugin',
      onAction: function() {
        // Open window
        openDialog();
      }
    });
  */
    return {
      getMetadata: function () {
        return  {
          name: 'Opensymap Editor Box',
          url: 'http://www.opensymap.org/res/app/tinycme/plugin/osy-editor-box/'
        };
      }
    };
  });
  