<?php

namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ModalInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Dev\Opensymap\ViewEvent\FormBuild;
use Spinit\Dev\Opensymap\ViewEvent\FormBuilder;
use Spinit\Dev\Opensymap\ViewEvent\FormDelete;
use Spinit\Dev\Opensymap\ViewEvent\FormFinish;
use Spinit\Dev\Opensymap\ViewEvent\FormLoad;
use Spinit\Dev\Opensymap\ViewEvent\FormRestore;
use Spinit\Dev\Opensymap\ViewEvent\FormSave;
use Spinit\Lib\Model\ModelInterface;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\biName;
use function Spinit\Util\getInstance;

class Form extends View implements FormInterface {
    

    private $response;

    /**
     * @var ApplicationInterface
     */
    private $resources = [];
    private $request;
    private $model;
    private $pkey;
    private $query = [];
    private $referer;
    private $module;
    private $controller = null;
    private $debug = 0;

    public function __construct(ModuleInterface $module, FormAdapterInterface $adapter) {
        $this->module = $module;    

        parent::__construct($adapter);

        $this->bindExec('@rewriteValue', function($event) {
            if ($event->getParam(0)) {
                if ($this->request) {
                    $this->getRequest()->setPost($event->getParam(0), $event->getParam(1));
                }
                $this->getResponse()->set('init,'.$event->getParam(0), $event->getParam(1));
            }
        });
        $this->bindExec('@build', function() {
            (new FormBuild($this))->build();
        });
        $loader = (new FormLoad($this));
        $this->bindBefore('@load', [$loader, 'doBefore']);
        $this->bindExec('@load', [$loader, 'doExec']);
        $this->bindExec('@load-model', [$loader, 'doExecLoadModel']);
        $this->bindAfter('@load', [$loader, 'doAfter']);

        $saver = new FormSave($this);
        $this->bindExec('save', [$saver, 'doStore']);
        $this->bindAfter('save', [$saver, 'doSave']);    
        $this->bindEnd('save', [$saver, 'doEnd']);
        
        $this->bindExec('delete', function($event) {
            (new FormDelete($this))->doExec($event);
        });

        $this->bindExec('restore', function($event) {
            (new FormRestore($this))->doExec($event);
        });

        $this->bindExec('@finish', function($event) {
            (new FormFinish($this))->doExec($event);
        });

    }

    public function getForm(): FormInterface
    {
        return $this;
    }
    public function setQuery($name, $value = '', $debug = 0) {
        if (is_array($name)) {
            $this->query = [];
            foreach($name as $k=>$v) $this->setQuery($k, $v);
        } else {
            $part = array_map(function($item) {return trim($item, ']');}, explode('[', $name));
            $ptr = &$this->query;
            $old = &$ptr;
            $last = $part;
            foreach($part as $p) {
                $old = &$ptr;
                $last = $p;
                if (!array_key_exists($p, $ptr)) {
                    $ptr[$p] = [];
                }
                $ptr = &$ptr[$p];
            }
            if ($value) {
                $ptr = $value;
            } else {
                unset($old[$last]);
            }
            //if ($debug) debug($part, $this->query);
        }
        return $this;
    }
    public function getQuery() {
        $args = func_get_args();
        if (count($args)==0) {
            return $this->query;
        }
        
        return call_user_func_array('Spinit\\Util\\arrayGet', array_merge([$this->query], $args));
    }
    public function getPKey() {
        return $this->pkey;
    }
    public function setPkey($pkey) {
        $this->pkey = $pkey;
        return $this;
    }
    public function getModule() : ModuleInterface {
        return $this->module;
    }
    public function getUri() : string {
        return $this->getApplication()->getName().'/Form/'.$this->getName();
    }
    public function getUrl() : string {
        $app = $this->getApplication();
        $pkey = '';
        if($key = $this->getPkey()) {
            if (is_array($key)) {
                $key = array_map(function($item) {return $item?:'';}, $key);
                ksort($key);
            }
            $pkey = base64_encode(json_encode($key)).'/';
        }
        $queryList = [];
        $queryWrite = function($name, $value) use (&$queryList, &$queryWrite) {
            if (is_array($value)) {
                foreach($value as $k=>$v) $queryWrite($name.'['.$k.']', $v);
            } else {
                $queryList[] = $name.'='.urlencode($value);
            }
        };
        
        foreach($this->getQuery()?:[] as $name=>$value) $queryWrite($name, $value);
        $path = "";
        // se l'app corrente è stata caricata da una app e non dal runner ... allora
        // imposta tale app come principale
        if ($app->getExternApplication()) $path = $app->getExternApplication()->getName()."/";
        $path .= $app->getName().'/'.
                $this->getName().'/'.
                $pkey.(count($queryList)?'?'.implode('&', $queryList):'');

        $url = $app->getInstance()->makeSectionPath('app', $path);
        return $url;
    }
    
    public function getResponse() : Response {
        return $this->response ?: $this->getApplication()->getResponse();
    }
    
    public function setResponse(Response $response) {
        $this->response = $response;
        return $this;
    }

    public function setRequest(Request $request) {
        $this->request = $request;
        $this->setQuery($request->getQuery());
        return $this;
    }
    
    public function getRequest() : ?Request {
        return $this->request;
    }

    public function getApplication() : ApplicationInterface {
        return $this->getModule()->getApplication();
    }

    public function run (Request $request) : Response {

        $view = $target = $this;
        $this->setRequest($request);
        $this->trigger('@load', [$request->getPath(0)]);
        $eventName = ltrim((string) $request->getApp('osy,event'), '@');
        $eventParam = json_decode((string) $request->getApp('osy,param'), 1)?:(string) $request->getApp('osy,param');
        if($modalName = (string) $request->getApp('osy,modal')) {
            $view = $target = $this->getModal($modalName, $request->getApp('modal'));
        }

        if($itemName = (string) $request->getApp('osy,item')) {
            // per un Item l'evento è obbligatorio
            Assert::notEmpty($eventName, 'Evento non impostato');
            $target = $target->getItem($itemName);
        } else {
            // l'evento reload rievoca il @build
            if ($eventName == "reload") $eventName = "";
        }
        $target->trigger($eventName?:'@build', [$eventParam]);
        $view != $this AND $view->trigger('@finish');
        $this->trigger('@finish');
        return $this->getResponse();
    }

    function addResource($file)
    {
        $args = func_get_args();
        if (count($args)<2) {
            array_unshift($args, '');
        }
        $section = array_shift($args);
        $file = array_shift($args);
        $part = explode('?', $file);
        $fileClean = array_shift($part);
        $section = $section ?: pathinfo($fileClean, PATHINFO_EXTENSION);
        if (!array_key_exists($section, (array) $this->resources)) 
        {
            if ($this->debug) var_dump(['section', $section]);
            $this->resources[$section] = [];
        }
        if (!in_array($file, $this->resources[$section])){
            if ($this->debug) var_dump($section, $file, $this->getUrl());
            $this->resources[$section][] = $file;
        }
    }
    /**
     * Recupera il builder gestito dalla factory (così da essere condiviso tra gli item)
     */
    public function getBuilder($name) : BuilderInterface {
        $factory = $this->getApplication()->getInstance()->getFactory();
        $builder = null;
        if ($builderClass = $this->getAdapter()->getBuilder($name)) {
            $builder = $factory->makeBuilder($builderClass, $this);
        } else if ($builderClass = $this->getModule()->getBuilder($name)) {
            $builder = $factory->makeBuilder($builderClass, $this);
        }
        Assert::notNull($builder);
        return $builder;
    }

    function getResourceList() : iterable {
        if ($this->debug) var_dump($this->resources);
        return (array) $this->resources;
    }

    public function getDataSource($name = '') {
        return $this->getApplication()->getInstance()->getDataSource($name);
    }

    /**
     * Il model può essere quello di un'altra app
     */
    public function getModel($new=false) {
        // se non c'è o occorre crearne uno nuovo ...
        if (!$this->model or $new) {
            $part = explode('/Model/', $this->get('model'));
            $app = $this->getApplication();
            $defaultModule = $this->getModule()->getName();
            if (count($part)>1) {
                $app = $app->getInstance()->getFactory()->getApplication(array_shift($part));
                $defaultModule = 'Core';
            }
            $modelName = array_shift($part);
            $model = null;
            if ($modelName) {
                $part = biName($modelName, $defaultModule);
                $model = $app->getModel($part);
            }
            // se occorre crearne solo uno nuovo allora viene ritornato
            if ($new === true) return $model;

            if ($model) $this->model = $model;

            if ($new == 'reload' and $this->getPkey() and $this->model) {
                $this->model->load($this->getPkey());
                //debug($this->getPkey(), $this->model->asArray());
            }
        }
        return $this->model;

    }
    public function setModel(ModelInterface $model) {
        $this->model = $model;
        return $this;
    }
    public function getController() {
        if (!$this->controller) {
            if ($this->get('controller')) $this->controller = getInstance($this->get('controller'), $this);
        }
        return $this->controller;
    }
    
    public function getReferer()
    {
        return $this->referer;
    }
    
    public function setReferer($form)
    {
        $this->referer = $form;
        return $this;
    }

    public function getModal($name): ModalInterface
    {
        $adapter = function () : FormAdapterInterface {
            return $this->getAdapter();
        };
        $args = func_get_args();
        $modal = new Modal($this, $adapter()->getModal($name));
        if (count($args)>1) {
            $modal->trigger('@load', [$args[1]]);
        }
        return $modal;
    }
}
