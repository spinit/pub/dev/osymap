<?php

namespace Spinit\Dev\Opensymap\Core;

use Spinit\Dev\AppRouter\Core\Asset as CoreAsset;
use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\AppRouter\Request;

class Asset {

    private $asset;
    
    public function __construct(InstanceInterface $instance, $root) {
        $this->asset = new CoreAsset($instance, $root);
    }
    
    public function run(Request $request, $processor = null) {
        return $this->asset->run($request->getPath(), ''.$request->getApp('osy,event'), $request, $processor);    
    }

    public function getOrigin() {
        return $this->asset;
    }
}