<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\CommonTrait;
use Spinit\Dev\Opensymap\Core\Xml\FileTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasApplicationTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasBuilderTrait;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ModuleAdapterInterface;
use Spinit\Lib\Model\Adapter\ModelAdapter;
use Spinit\Lib\Model\Adapter\Xml\ModelAdapterXml;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Dev\AppRouter\debug;

class ModuleAdapter implements ModuleAdapterInterface {

    use FileTrait;
    use AttributeTrait;
    use HasBuilderTrait;
    use HasApplicationTrait;

    private $name;
    private $extern;
    public function __construct(ApplicationAdapter $app,  string $name, $extern = "")
    {
        $this->setApplication($app);
        $this->name = $name;
        $this->extern = $extern;
        $root = $app->getRoot();
        if ($extern) {
            $root = fspath($root, 'Extern', $app->getExternDir($extern));
        } 
        $this->init(fspath($root, 'Module', $name, 'index.xml'), false);
    }
    public function getName() : string {
        return $this->name;
    }
    public function getFormList(): iterable
    {
        return $this->listResourceIn('Form');
    }
    public function getForm($name) : FormAdapterInterface {
        return new FormAdapter($this, $name);
    }
    public function getModelList(): iterable
    {
        return $this->listResourceIn('Model', 'is_file', function($item) {
            return substr($item, 0, -4);
        });
    }
    public function getModel($name) : ModelAdapter {
        $file = fspath($this->getRoot(), 'Model', $name.'.xml');
        return new ModelAdapterXml($name, $file);
    }
    public function getExtendList(): iterable
    {
        foreach($this->index->extend as $extend) {
            yield new ExtendAdapter($this, $extend);
        }
    }
    /**
     * Menubar da caricare
     * $names : lista dei nomi dei menubar di interesse (i nomi sono globali)
     */
    public function getMenuBars($names) {
        $list = [];
        if (!is_array($names)) $names = asArray($names, ",");
        foreach($this->index->menubar as $mbar) {
            // se il menú è presente nella lista ... allora viene caricato
            $mname = (string)$mbar["name"] ?: "std";
            if (in_array($mname, $names)) {
                $list[$mname] = [
                    'label' => (string) $mbar['label'], 
                    'name' => (string) $mbar['name'], 
                    'menu'=>[]
                ];
                $this->extractMenu($mbar, $list[$mname]['menu']);
            }
        }
        return $list;
    }
    private function extractMenu($box, &$list) {
        foreach($box->menu ?: []as $menu) {
            $item = (array) arrayGet((array) $menu->attributes(), '@attributes', []);
            $item['menu'] = [];
            if (array_key_exists('form', $item)) {
                if (!array_key_exists('app', $item)) {
                    $item['app'] = $this->getApplication()->getName();
                }
                if (!strpos($item['form'], ':')) {
                    $item['form'] = $this->getName().':'.trim($item['form'], ':');
                }
            }
            $this->extractMenu($menu, $item['menu']);
            if (@$menu['name']) {
                $list[(string) $menu['name']] = $item;
            } else {
                $list[] = $item;
            }
        }
    }
    public function getItemList() {
        return UtilCheck::getItemList($this->index->items->item);
    }

    public function getInitRunner($when) : iterable {
        foreach($this->index->init->$when?:[] as $el) yield (string) $el;
    }
}