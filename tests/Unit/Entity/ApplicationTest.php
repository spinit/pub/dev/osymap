<?php
namespace Spinit\Test\Entity\ApplicationTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class ApplicationTest extends TestCase {

    private $osy;
    /**
     * @var ApplicationInstance
     */
    private $app;
    protected function setUp() : void
    {
        parent::setUp();
        $this->osy = new OpensymapDummy();
        $this->app = $this->osy->getFactory()->getApplication('app@test');
    }
    
    public function testGetInstance() {
        $this->assertEquals($this->osy, $this->app->getInstance());
    }

    public function testGetForm() {
        $form = $this->app->getForm('Core:Test');
        $this->assertEquals('Core:Test', $form->getName());
    }

    public function testOverlay() {
        $list = $this->app->getOverlayList();
        $this->assertContains('OrderPrefix', $list);
        foreach($list as $overlayName) {
            $app = $this->app->getOverlay($overlayName)->getApplication();
            $this->assertEquals('Core:Main', $app->getForm('Main')->getName());
        }
    }

    public function testRun() {
        $response = $this->app->run(new Request('POST', 'Test'));
        $expected = '[(direct class)[itm1(in form)]][itm2(in module)[(in app)][(type empty)]]';
        $this->assertStringContainsString($expected, $response->get('content'));
    }

    public function xtestGetModule() {
        $module = $this->app->getModule('Core');
        $list = $module->getModelList();
        $this->assertCount(1, $list); // contiene il model M1
        try {
            $module->getModel('Qualsiasi');
            $this->fail ('Modello non doveva esssere trovato');
        } catch (\Exception $e) {
            $this->AssertTrue(true);
        }

        $list = $module->getFormList();
        $this->assertContains('Test', $list);
            
    }
}