<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasViewTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ItemAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ViewAdapterInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\file_get_contents;

class ItemAdapter implements ItemAdapterInterface {

    use AttributeTrait {
        get as protected getAtt;
    }

    use HasViewTrait;

    private $parent;

    public function __construct(ViewAdapterInterface $view,  $item, $parent = null)
    {
        $this->setView($view);
        $this->index = $item;
        $this->parent = $parent;
    }
    public function getParent() : ?ItemAdapterInterface {
        return $this->parent;
    }
    public function get($name, $default = '') {
        if (($value = $this->getAtt($name))!=='') {
            return $value;
        }
        foreach($this->index->prop as $prop) {
            if ($prop['name'] == $name) {
                $value = json_decode(str_replace('"@attributes":','"@":', json_encode($prop)), 1);
                $attr = null;
                if (array_key_exists('@', $value)) {
                    $attr = $value['@'];
                    unset($value['@']);
                }
                if (count($value) == 1 and array_key_exists(0, $value)) {
                    return $value[0];
                }
                if (is_array($attr)) {
                    $value['@'] = $attr;
                }
                return $value;
            }
        }
        return $default;
    }
    public function getName() : string {
        return $this->get('name');
    }
    public function getITemList() : iterable {
        foreach($this->index->item as $item) {
            yield new self($this->getView(), $item, $this);
        }
    }

    public function getTrigger($event, $when): string
    {
        $fname = fspath($this->getView()->getRoot(), 'Item', $this->getName(), 'Trigger', "{$event}-{$when}.php");
        if (is_file($fname)) {
            return file_get_contents($fname);
        }
        return '';
    }
}