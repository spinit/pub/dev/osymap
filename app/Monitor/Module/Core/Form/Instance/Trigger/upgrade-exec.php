<?php

use function Spinit\Dev\AppRouter\debug;

use Spinit\Dev\Opensymap\App\Monitor\Source\Command\InstanceUpgrade;
use Spinit\Lib\DataSource\DataSource;

$cmd = new InstanceUpgrade(
    $this->getApplication()->getInstance()->getMainInteractor(),
    new DataSource($this->getItem('txt_str')->getValue()),
    $this->getItem('hdn_id')->getValue()
);

$cmd->exec();
