<?php

namespace Spinit\Dev\Opensymap\Type;

use Spinit\Lib\Model\ModelInterface;

interface ModuleInterface {
    function getName() : string;
    function getExtendList() : iterable;
    function getFormList() : iterable;
    function getForm($name) : FormInterface;
    function getModelList() : iterable;
    function getModel($name) : ModelInterface;
    function getBuilder($name) : string;
    function getApplication() : ApplicationInterface;
    function getMenuBars($role);
    function init();
}