<?php 
namespace Spinit\Dev\Opensymap\Exception;

class MessageError extends \Exception {
    public function __construct($message, $title = '')
    {
        parent::__construct($message);
        $this->title = $title;
    }
    public function getTitle($default = '') {
        return $this->title ?: $default;
    }
}