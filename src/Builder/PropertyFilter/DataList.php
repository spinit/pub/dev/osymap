<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;

use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\loadConfig;
use function Spinit\Util\arrayGet;

/**
 * Description of DataList
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataList
{
    private $field;
    private $DS;
    private $conf;
    static private $ctor = [
        'num'=>__CLASS__.'::fncNumber',
        'combo'=>__CLASS__.'::fncCombo'
    ];
    
    public function __construct($field)
    {
        $this->field = $field;
        $this->DS = $field->getForm()->getDataSource();
        $this->conf = loadConfig($this);
    }
    
    public static function setCtor($name, $callback)
    {
        self::$ctor[$name] = $callback;
    }
    
    public function getFilter($debug = 0)
    {
        $queryArgs = $this->DS->getCommand($this->conf->get('filter'), [
            'master' => $this->field->get('master'), 
            'item' => $this->field->get('item'), 
            'rel-type' => $this->field->get('rel-type'), 
        ]);
        $query = call_user_func_array([$this->DS->getAdapter(), 'normalize'], $queryArgs);
        $list = $this->DS->query(array_shift($query));
        if ($this->field->get('debug') or $debug) {
            debug($list);
        }
        $tid = '';
        $item = null;
        foreach($list as $rec) {
            if ($tid != $rec['id']) {
                if ($item != null) {
                    // se l'item è già stato popolato ...
                    yield $item;
                }
                $tid = $rec['id'];
                $item = ['id' => $rec['id'], 'urn'=>$rec['urn'], 'nme' => $rec['nme'], 'det' => []];
                // workaround : se un elemento ha dettaglio ma non ha un costruttore ... allora viene assunto come "combo"
                if ($rec['did'] and !$rec['cnf']) $rec['cnf'] = 'combo';
                $fnc = null;
                if ($rec['cnf']) {
                    // la funzione di valutazione viene prima cercata sui costruttori
                    $fnc = arrayGet(self::$ctor, $rec['cnf']);
                    // se non presente, allora viene interpretata come classe da instanziare
                    $fnc = $fnc?:[Util\getInstance($rec['cnf'], $this->field), 'render'];
                } else {
                    $fnc = __CLASS__.'::fncCheck';
                }
                $item['fnc'] = function() use ($fnc) {
                    $args = func_get_args();
                    array_unshift($args, $this->field);
                    return call_user_func_array($fnc, $args);
                };
            }
            if ($rec['did']) {
                $item['det'][] = [$rec['did'], $rec['dnme'], $rec['durn']];
            }
        }
        if ($item != null) {
            yield $item;
        }
    }
    
    private static function getParam($field, $args)
    {
        //default
        $name = $field->getName();
        $value = $field->getValue();
        array_shift($args); // $field
        array_shift($args); // $item
        //effettivi;
        if (count($args)) {
            $name = array_shift($args);
        }
        if (count($args)) {
            $value = array_shift($args);
        }
        return [$name, $value];
    }
    
    public static function fncNumber($field, $item)
    {
        list($name, $value) = self::getParam($field, func_get_args());
        throw new \Exception();
        $el = Util\Tag::create('input')->att([
            'value'=> Util\arrayGet($value, $item['id']), 
            'size'=> '5', 
            'class'=>'numeric form-control w-origin', 
            'name'=>$name."[{$item['id']}]"
        ]);
        return $el;
    }
    
    public static function fncCheck($field, $item)
    {
        list($name, $value) = self::getParam($field, func_get_args());
        $el = Util\Tag::create('input')->att([
            'value'=> '1',
            'type'=> 'checkbox', 
            'name'=>$name."[{$item['id']}]"
        ]);
            
        if (Util\arrayGet($value, $item['id'])) {
            $el->prp('ckecked');
        }
        return $el;
    }
    
    public static function fncCombo($field, $item)
    {
        if (!count($item['det'])) return null;

        $item['typ'] = 'uuid';
        list($name, $value) = self::getParam($field, func_get_args());
        $el = Util\Tag::create('select')->att([
            'name'=>$name."[{$item['id']}]",
            'class'=>"form-control w-origin"
        ]);
        $el->add(Util\Tag::create('option'))->att('value', '')->add(' ');
        foreach($item['det'] as $val) {
            $op = $el->add(Util\Tag::create('option'))->att('value', $val[0]);
            $op->add($val[1]);
            if ($val[0] == Util\arrayGet($value, $item['id'])) {
                $op->prp('selected');
            }
        }
        return $el;
    }
}
