<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;

class BuilderDummy implements BuilderInterface {
    public function build(ItemInterface $item)
    {
        return '';
    }
}