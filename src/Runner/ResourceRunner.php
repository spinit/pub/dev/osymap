<?php
namespace Spinit\Dev\Opensymap\Runner;

use ReflectionClass;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Core\Asset;
use Spinit\Dev\Opensymap\Runner;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\getClassPath;

class ResourceRunner extends Runner {
    public function run(Request $request) : Response {
        $appName = $request->getPath(0);
        $request = $request->shiftPath();
        $request = $request->shiftPath();
        $formName = $request->getPath(0);
        $request = $request->shiftPath();

        $app = $this->getInstance()->getApplication($appName);
        $form = $app->getForm($formName);
        $formID = 'form-'.md5($form->getUri());
        
        $adapter = $form->getAdapter();
        $fs = new Asset($this->getInstance(), fspath($adapter->getRoot(), 'Asset'));
        return $fs->run($request, function($cnt) use ($formID) {
            return str_replace('form--id', $formID, $cnt);
        });
    }
}