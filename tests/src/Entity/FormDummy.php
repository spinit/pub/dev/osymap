<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;

class FormDummy implements FormInterface {
    function run(Request $request) : Response {
        return new Response();
    }
    function getApplication() : ApplicationInterface {
        throw new \Exception();
    }
    function getItemList() : iterable {
        return [];
    }
    function getItem($name) : ItemInterface {
        throw new \Exception();
    }
    function getBuilder($name) : ?BuilderInterface {
        return null;
    }
    function getModule(): ModuleInterface
    {
        throw new \Exception();
    }
}