<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\DataGrid;

use Spinit\Util;

/**
 * Description of MakerHederComponent
 *
 * @author ermanno
 */
class MakerHeder {
    public function exec($field, $header) {

        if ($cmd = $field->get('command')) {
            // occorre far vedere i comand se le linee vengono selezionate
            $header->att('class', 'viewCommand', ' ');
            $has_header = true;
            $bar = $header->add(Util\Tag::create('div'))
                              ->att('class', 'commandBar');
            foreach($cmd as $name => $cnf) {
                //$this->buildCommand($bar, $name, $cnf);
            }
        }
        if ($field->get('formDetail') or $field->get('formAddDetail') or $field->get('modalAdd') or $field->get('add')) {
            if ($field->get('add')!='no') {
                $header->add(Util\Tag::create('button'), 1)
                    ->att('type', 'button')
                    ->att('class', 'float-end btn btn-outline-info btn-sm osy-btn')
                    ->att('osy-event', 'add')
                    ->add($field->get('addLabel', 'Aggiungi'));
            }
        }
        if ($field->get('search')) {
            $header->add(Util\Tag::create('input'))
                ->att('type', 'text')
                ->att('name', $field->getName().'[search]')
                ->att('osy-event', 'search')
                ->att('value', Util\arrayGet($field->getValue(),'search'))
                ->att('class', 'float-right search');
        }
        
    }
}
