<?php
use Spinit\Dev\Opensymap\Helper\LoginCheck;
use function Spinit\Dev\AppRouter\debug;

$checker = new LoginCheck($this->getInstance());
$checker->check($response, $request->getPost("txt_login"), $request->getPost("txt_passwd"));