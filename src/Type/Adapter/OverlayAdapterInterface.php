<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

interface OverlayAdapterInterface extends AdapterInterface {
    function getApplication() : ApplicationAdapterInterface;
}