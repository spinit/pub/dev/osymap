(function($) {

    let log = console.log;

    function rgbToHex(col)
    {
        if(col.charAt(0)=='r')
        {
            col=col.replace('rgb(','').replace(')','').split(',');
            var r=parseInt(col[0], 10).toString(16);
            var g=parseInt(col[1], 10).toString(16);
            var b=parseInt(col[2], 10).toString(16);
            r=r.length==1?'0'+r:r; g=g.length==1?'0'+g:g; b=b.length==1?'0'+b:b;
            var colHex='#'+r+g+b;
            return colHex;
        }
    }
    $(document).on("click", ".osy-color .color", function() {
        $(".osy-color .picked").removeClass("picked");
        $(this).addClass("picked");
    });
    $(document).on("pick", ".osy-color", function() {
        let info = {};
        $(".color", this).each(itm=>info[$(itm).attr("name")] = rgbToHex(itm.style.backgroundColor)||"");
        $(".info", this).val(JSON.stringify(info));
        $(".res", this).css({"color": info.txt, "background-color":info.bkg, "border-color":info.brd});
    });
    $(document).on("coloris:pick", function(event) {
        $(".osy-color .picked").
            css("background-color", event.detail.color).
            closest(".osy-color").
            trigger("pick");  
    })
})(desktop.$)