<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace  Spinit\Dev\Opensymap\Builder\DataGrid;

use Spinit\Dev\Opensymap\Builder\Util;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Helper\OpenFormDetail;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\normalize;

/**
 * Description of DataGrid
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataGrid extends Builder
{
    private $head;
    private $body;

    protected function registerEvent() {
        $this->bindBefore('add', [$this, 'onAddFormDetailBefore']);
        $this->bindEnd('add', [$this, 'onOpenFormDetailEnd']);
        $this->bindBefore('open', [$this, 'onOpenFormDetailBefore']);
        $this->bindEnd('open', [$this, 'onOpenFormDetailEnd']);
        $this->bindExec('load', [$this, 'onLoadData']);
        $this->bindExec('search', [$this, 'onLoadData']);
        $this->head = new MakerHeder();
        $this->body = new MakerBody();
    }
    
    public function build(ItemInterface $item, $withLabel = true) {
        $this->addResource('DataGrid.js');
        $this->addResource('DataGrid.css');

        $div = $this->makeComponent($item, 'osy-datagrid', !$item->get('noLabel'));
        $data = new MakerData();
        $cnt = $div->add(new Tag('div'));
        $this->head->exec($item, $div->child(0));
        try {
            $this->body->exec($item, $cnt, $data->build($item, '', $cnt));
        } catch(\Exception $e) {
            $err = $div->add(Tag::create('div'))->att('class','err-code');
            $err->add(Tag::create('code'))->att('class', 'title')->add($e->getMessage());
            if ($data->getDataSource())

            $cmd = $data->getDataSource()->getCommandLast();
            $err->add(Tag::create('div'))->att('class', 'source')->add(htmlspecialchars($cmd[0]));
        }
        return $div;
    }

    protected function onLoadData($event, $field)
    {
        $div = Tag::create('');
        $value = $field->getValue();
        //debug($event->getParam(0));
        foreach($event->getParam(0)?:[] as $k=>$v) $value[$k] = $v;
        $field->setValue($value);
        $data = new MakerData();
        $this->body->exec($field, $div, $data->build($field, $field->getValue("search"), $div));
        $this->getForm()->getResponse()->addContent($field->getName(), ['.osy-body' => $div]);
    }

    protected function onAddFormDetailBefore($event, $field) {
        $this->getForm()->trigger('save',['noBuild']);
        if ($field->get('formAddDetail')) {
            $field->set('formDetail', $field->get('formAddDetail'));
            $field->set('formData', $field->get('formAddData'));
            $field->set('formValue', $field->get('formAddValue').'');
        }
    }
    protected function onOpenFormDetailBefore($event, $field) {
        $param = $event->getParam(0);
        foreach(['formDetail', 'formData', 'formValue'] as $ff) {
            if ($val = arrayGet($param, $ff)) {
                $field->set($ff, $val);
            }
        }
    }
    protected function onOpenFormDetailEnd($event, $field)
    {
        if ($event['name'] == 'add' and $modal = $field->get("modalAdd")) {
            $field->getForm()->getModal($modal)->build();
            return;
        }
        // viene caricata la nuova form con la chiave eventualmente indicata
        $param = $event->getParam(0);
        $key = arrayGet($param, 'pkey');
        if ($key) {
            if (is_array($key)) ksort($key);
            $key = json_encode($key);
        }
        $url = $key ? base64_encode($key) : '';
        $newForm = new OpenFormDetail($field);
        // modifica parametri in base al tipo di chiamata (nuovo elemento o apertura)
        if ($event['name'] == 'add') {
            $newForm->setNewForm($field->get('formDetail'));
        }
        $newForm->exec($url);
    }

    public function backValue($field, $value)
    {
        return $this;
    }
    /*
    private function buildBody($field, $bdy) {
        try {
            // generazione dati
            $list = $this->getData($field, $bdy);
            if ($field->get('debug')) {
                $bdy->add('debug');
            } else {
                // scrittura dati
                (new MakerStruct())->build($field, $list, $bdy);
            }
        } catch (\Exception $e) {
            $bdy->add("<div>".$e->getMessage().'</div><pre>'.print_r($field->getForm()->getDataSource()->getCommandLast(), 1).'</pre>');
        }
    }
    
    public function getData($field) {
        $args = func_get_args();
        array_shift($args);
        return (new MakerData())->build($field, $field->getValue('search'), array_shift($args));
    }
    
    
    */
}

