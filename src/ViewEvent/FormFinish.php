<?php
namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Command\ResponseAddCommand;
use Spinit\Dev\Opensymap\Helper\FormUtil;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\normalize;

class FormFinish extends EventHandlerForm {

    public function doExec($event) {
        // vengono aggiunte le risorse alla risposta
        $form = $this->getForm();
        $model = $form->getModel();
        $response = $form->getResponse();
        $response->set('data,res', $form->getResourceList());
        
        $cmd = new ResponseAddCommand($response);
        $url = $form->getUrl();
        $arTitle = normalize($form->get('title'), $model ? $model->asArray() : []);
        $cmd->exec("window.history.replaceState({init : response.init}, args[1], args[0]); desktop.$(document).trigger('setTitle', args[1]);", $url, $arTitle[0]);
    }
}
