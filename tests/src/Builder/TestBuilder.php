<?php

namespace Spinit\Test\Builder;

use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\TriggerTrait;

class TestBuilder implements BuilderInterface{
    use TriggerTrait;
    public function __construct()
    {
        
    }
    function build(ItemInterface $item, $withLabel = true) {
        $content = '['.$item->getName().'('.$item->get('descr').')';
        foreach($item->getItemList() as $ii) {
            $content .= $ii->build();
        }
        return $content.']';
    }

    function checkValue(ItemInterface $item, $value) {
        return $value;
    }

    public function getForm(): FormInterface
    {
        throw new \Exception();
    }

    public function setForm(FormInterface $form)
    {
        
    }

    public function getInstance(): InstanceInterface
    {
        throw new \Exception();
    }

    public function loadFromModel(ItemInterface $item, ModelInterface $model)
    {
        
    }
    public function getItemList(ItemInterface $item) {
        return $item->getItemList();
    }
}