<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PanelTab;

use Spinit\Dev\Opensymap\Command\ResponseSetContent;
use Spinit\Util;
use Spinit\Dev\Opensymap\Entity\Builder as EntityBuilder;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of TextBox:Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PanelTab extends EntityBuilder
{
    protected function registerEvent() {
        $this->bindExec('open', [$this, 'onOpenData']);
        $this->bindEnd ('open', [$this, 'onOpenDataEnd']);
        $this->bindBefore('@load', function($event, $field) {
            // se non può essere vuoto si controlla il valore in post ... 
            // e in ultima istanza si prende il primo valore del datasource
            //if ($field->getForm()->getParam('debug')) debug($field->getValue());
            $this->loadData($field);
            $valueGet = $field->getForm()->getRequest()->getQuery($field->getName());
            if (!$field->getValue()) {
                $rec = $field->getParam('data')->current();
                if ($rec) {
                    if (in_array($valueGet, $rec)) {
                        $field->setValue($valueGet);
                    } else {
                        $kk = array_keys($rec);
                        $field->setValue($rec[$kk[0]]);
                    }
                } else {
                    $first = "";
                    foreach($field->getItemList() as $child) {
                        if (!$child->isVisible()) continue;
                        if (!$first) $first = $child->get('name');
                        if ($valueGet == $child->get('name')) {
                            $field->setValue($child->get('name'));
                            break;
                        }
                    }
                    if (!$field->getValue()) {
                        $field->setValue($first);
                    }
                }
            }
        });
        $this->bindEnd('@load', function ($event, $field) {
            // vengono disabilitati tutti gli altri figli
            foreach($field->getItemList() as $child) {
                if ($child->get('name') != $field->getValue()) {
                    $child->set('disable', '1');
                }
            }
        });
    }

    private function loadData($field) {
        if ($field->getParam('data')) {
            return;
        }
        $cmnt = new Util\Tag('');
        $data = (new MakerData)->build($field, '', $cmnt);
        $field->setParam('data', $data);
        $field->setParam('comment', $cmnt);
    }

    public function build(ItemInterface $field, $withTitle = true)
    {
        $this->addResource('PanelTab.css');
        
        $div = $this->makeComponent($field, 'osy-panelTab osy-no-container osy-field', strlen($field->get('label')) and $withTitle);
        $body = $div->Add(Tag::create('div'))->att('class', 'osy-body');
        // viene passato il valore ... così posso richiamare la funzione anche sull'evento open mettendo il tab che si vuole aprire
        $this->makeContent($field, $body, $field->getValue());
        return $div;
    }
    
    private function makeContent($field, $div, $value)
    {
        $field->setValue($value, 1);
        if ($field->get('inUrl') != 'no') {
            $field->getForm()->setQuery($field->getName(), $value);
        }
        $field->trigger("@open");
        $current = $this->makeHeader($field, $div, $value);
        //debug('stop', $field->getParam('data'));
        $bdy = $div->add(Util\Tag::create('div'))->att('class', 'tabBody');
        if ($current) {
            $current[1]->set('noLabel', '1');
            $current[1]->set('noSpace', '1');
            $bdy->add($current[1]->build());
        }
    }
    
    /**
     * 
     * @param type $field
     * @param type $div
     * @param type $value
     */
    private function makeHeader($field, $div, $value)
    {
        // se ha un datasource, vengono prima mostrati gli elementi e poi le intestazioni dal secondo figlio in poi
        $k = 0;
        $inp = $div->add(Util\Tag::create('input'))
            ->att([
                'name'=>$field->getName(), 
                'type'=>'hidden'
            ]);
        
        $hdr = $div->add(Util\Tag::create('div'))->att('class', 'tabHeader');
        $first = false;
        $current = false;
        $fieldList = $field->getItemList();
        foreach($fieldList as $idx => $child) {
            if (!$child->isVisible()) continue;
            if(!$child->get('name')) {
                $child->set('name', $field->getName().'-TAB-'.$idx);
            }
            if (!$first) { // la prima volta sicuro
                if ($data = $field->getParam('data')) {
                    $idx = 0;
                    $hdr->add($field->getParam('comment'));
                    foreach($data as $rec) {
                        $lbl = $this->makeHeaderItem($field, $hdr, $value, array_shift($rec), array_shift($rec)?:$child->get('label'));
                        if (!$first) {
                            $first = [$lbl, $child];
                        }
                        if ($lbl->getParam('isCurrent')) {
                            $current = [$lbl, $child];
                        }
                        $idx += 1;
                    }
                    $field->setParam('data', null);
                    // se non è stato inserito alcun un tab ... inserisci il tab vuoto
                    if (!$idx) {
                        $lbl = $this->makeHeaderItem($field, $hdr, $value, $child->getName(), $child->get('label'));
                        if (!$first) {
                            $first = [$lbl, $child];
                        }
                        if ($lbl->getParam('isCurrent')) {
                            $current = [$lbl, $child];
                        }
                    }
                    // il primo child viene preso come base per il contenuto dei tab generati dalla query
                    continue;
                }
            }
            $lbl = $this->makeHeaderItem($field, $hdr, $value, $child->getName(), $child->get('label'));
            if ($child->get('right')) {
                $lbl->att('class', 'float-end', ' ');
            }
            if (!$first) {
                $first = [$lbl, $child];
            }
            if ($lbl->getParam('isCurrent')) {
                $current = [$lbl, $child];
            }
        }
        $current = $current ?: $first;
        if ($current) {
            $current[0]->att('class', 'current',' ');
            $inp->att('value', $current[0]->getParam('value'));
            $field->setValue($current[0]->getParam('value'));
        }
        return $current;
    }
    
    private function makeHeaderItem($field, $hdr, $value, $nameTab, $label)
    {
        $tagLbl = $hdr->add(Util\Tag::create('div'))->att('class', 'tabLabel')->att('osy-event', 'open')->att('osy-param', $nameTab);
        $tagLbl->setParam('value', $nameTab);
        if ($label) {
            $wrap = $tagLbl->add(Util\Tag::create('div'))->att('class', 'wrap');
            $wrap->add($label);
        }
        if ($nameTab == $value) {
            $tagLbl->setParam('isCurrent', 1);
        }
        return $tagLbl;
    }
    
    protected function onOpenData($event, $field)
    {
        $newTabName = $event->getParam(0);
        // Caricamento dati esterni all'estrattore principale
        $form = $field->getForm();
        //$form->trigger('save', [$field->get('buildOnSave')=='no'?'nobuild':'']);
        $form->trigger('save', ['noBuild']);
        if (false) { //$form->getResponse()->hasError()) {
            $form->getResponse()->throwException();
        }

        $field->setValue($newTabName, 1);
        $this->loadData($field);
        // se si vogliono fare modifiche prima di generare il contenuto
        $field->trigger('@open');
    }
    
    protected function onOpenDataEnd($event, $field) {
        $response = $field->getForm()->getResponse();
        $div = Util\Tag::create('');
        // viene prima impsotato il contenuto
        (new ResponseSetContent($response))->exec('[id="'.$field->getName().'"]>.osy-body', $div);
        // e poi viene "prodotto" così che gli eventuali comandi generati lavorino sulla struttura già presente
        $this->makeContent($field, $div, $field->getValue());

    }
}
