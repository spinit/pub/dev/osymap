<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Spinit\Dev\Opensymap\Adapter\Xml\FormAdapter;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;

use function Spinit\Util\arrayGet;

trait HasFormTrait {
    private $form;

    protected function setForm(FormAdapter $form) {
        $this->form = $form;
        return $this;
    }
    public function getForm() : FormAdapterInterface {
        return $this->form;
    }
}