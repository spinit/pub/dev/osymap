<?php

namespace Spinit\Dev\Opensymap\Entity;

use ReflectionObject;
use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\Tag;
use Spinit\Util\TriggerTrait;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getColonsPath;

abstract class Builder implements BuilderInterface {

    use TriggerTrait;

    private $form;

    public function __construct(FormInterface $form)
    {
        $this->setForm($form);

        $this->bindBefore('@loading', function($event, $field) {
            $req = $field->getView()->getForm()->getRequest();
            if (!$req->getApp('osy,init') AND !$req->hasPost($field->getName()) AND $field->get('default')) {
                $field->setValue($field->get('default'), 1);
            } else {
            }
        });
        $this->bindBefore('@load', function($event, $field) {
            $req = $field->getView()->getForm()->getRequest();
            if (!$req->getApp('osy,init') AND !$req->hasPost($field->getName()) AND $field->get('default')) {
            } else {
                $field->setValueByRequest($req);
            }
        });
        $this->bindExec('@init', [$this, "onInitExec"]);
        $this->bindExec('@load', [$this, "onLoadExec"]);
        
        $this->bindExec('@backValue', function($event, $target) {
            $this->backValue($target, $event->getParam(0));
        });

        $this->registerEvent();
    }
    protected function onInitExec($event, $target) {}
    protected function onLoadExec($event, $target) {

    }
    public function getForm() : FormInterface {
        return $this->form;
    }
    public function setForm(FormInterface $form) {
        $this->form = $form;
        return $this;
    }
    public function getDataSource($what='') {
        return $this->getForm()->getDataSource($what);
    }
    public function getInstance() : InstanceInterface {
        return $this->getForm()->getApplication()->getInstance();
    }
    
    protected function registerEvent() {}

    public function getItemList(ItemInterface $item) {
        return $item->getItemList();
    }
    
    public function checkValue(ItemInterface $item, $value) {
        if (!is_array($value)) {
            $value = (string) $value;
        }
        $value = is_string($value) ? trim($value) : $value;
        return $value;
    }

    public function getResourcePath($resource) {
        $ref = new ReflectionObject($this);
        $fname = fspath(dirname($ref->getFileName()), $resource);
        $sha = '';
        if (is_file($fname)) $sha = sha1_file($fname);
        $fileResource = fspath(getColonsPath(get_class($this)), $resource.'?v='.$sha);
        return $this->getForm()->getApplication()->getInstance()->makeSectionPath('builder', $fileResource);
    }

    public function addResource($resource, $type = '') {
        $form = $this->getForm();
        if (strpos($resource, '://')>0) {
            $form->addResource($type, $resource);
        } else {
            $form->addResource($type, $this->getResourcePath($resource));
        }
    }

    public function loadFromModel(ItemInterface $item, ModelInterface $model) {
        $field = $item->get('field');
        $prop  = $item->get('property');
        if ($prop) {
            $value = $model->getPropertyValue($prop, $field);
            $item->setValue($value, true);
        } else {
            $field AND $item->setValue($model->get($field), true);
        }
        $item->trigger('@modelLoad', [$model]);
    }

    public function storeInModel(ItemInterface $item, ModelInterface $model) {
        $field = $item->get('field');
        $prop  = $item->get('property');
        $value = $item->getValue();
        if (!is_array($value) and !$value) {
            $value .= '';
        }
        if ($prop) {
            $model->setPropertyValue($prop, $value, $field);
        } else {
            $field AND $this->storeInModelFieldValue($model, $field, $value, $item);
        }
        $item->trigger('@modelStore', [$model]);
    }

    protected function storeInModelFieldValue($model, $field, $value, $item) {
        $model->set($field, $value);
    }
    // TODO
    public function nowrap($item) {
        return false;
    }

    public function makeComponent($field, $class, $withHeader = true) {
        $div = Tag::create('div')->att('class', $class)->att('osy-item', $field->get('name'));
        $this->initTag($field, $div);
        if ($field->get('name')) {
            $div->att('id', $field->getName());
        }
        $div->att('style', $field->get('style'), ' ');
        $div->att('class', $field->get('class'), ' ');
        
        $header = $div->add(Tag::create('div'))->att('class', 'osy-header');
        if ($label = $field->get('label') and $withHeader) {
            $header->add(Tag::create('label'))->add($label);
            if ($field->get('notNull')) {
                $header->child(0)->add(' <span>*</span>');
            }
        }
        return $div;
    }
    public function initTag($field, $main) {
        if (!$field->get('noSpace')) {
            $main->att('class', 'osy-field', ' ');
        }
    }

    public function isNull(ItemInterface $field) {
        return !$field->getValue();
    }

    public function backValue($field, $value) {
        $field->setValue($value, 1);
        return $this;
    }
}