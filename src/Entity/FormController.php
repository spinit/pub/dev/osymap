<?php
namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Type\FormInterface;

class FormController
{
    private $form;
    
    public function __construct(FormInterface $form)
    {
        $this->form = $form;
        $this->init();
    }
    
    public function getForm() : FormInterface
    {
        return $this->form;
    }
    
    public function getDataSource($name = '')
    {
        return $this->getForm()->getDataSource($name);
    }
    public function getInstance() {
        return $this->getForm()->getApplication()->getInstance();
    }
    protected function init()
    {
        
    }
}