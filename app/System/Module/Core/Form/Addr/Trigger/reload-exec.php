<?php

function checkNewAddress($addr) {
    $token = 'pk.eyJ1IjoidGltZWFnZW5jeSIsImEiOiJja280aDI4OXQxaXluMnhwZ3pxeTg3cGwwIn0.J9IYcF9jWp3C1M76BOkaCQ';
    $link = "https://api.mapbox.com/geocoding/v5/mapbox.places/[address].json?access_token=".$token;
    $src = \str_replace('[address]', urlencode($addr), $link);
    $dataStr = \file_get_contents($src);
    return json_decode($dataStr, 1)['features'][0];
}
$rec = [
    'adr' => $this->getField('txt_adr')->getValue(),
    'num' => $this->getField('txt_num')->getValue(),
    'zip' => $this->getField('txt_zip')->getValue(),
    'cty_nme' => $this->getField('txt_cty')->getValue(),
    'nat_nme' => $this->getField('txt_nat')->getValue()
];

$addr = $rec['adr'].($rec['num']?', '.$rec['num']:'').', '.$rec['zip'].' '.$rec['cty_nme'].', '.$rec['nat_nme'];
$data = checkNewAddress($addr);
$txt_prp = $this->getField('txt_prp');
$txt_prp->setValue($data);
$this->getResponse()->addContent('txt_prp', $txt_prp->build());

