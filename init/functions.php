<?php

use function Spinit\Util\getenv;

function loadEnv($file) {
    $data = parse_ini_file($file);
    foreach($data as $k => $v) {
        getenv($k, $v);
    }
}

function xid($id) {
    return md5($id);
}