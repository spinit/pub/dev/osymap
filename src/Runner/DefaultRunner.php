<?php
namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Runner;

use function Spinit\Dev\AppRouter\debug;

class DefaultRunner extends DesktopRunner {

    protected function start(Request $request) {
        $response = $this->getInstance()->getResponse();
        $response->set('test', ['runner'=>__CLASS__, 'path'=>$request->getPath()]);
        return $response;
    }
}