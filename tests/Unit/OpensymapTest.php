<?php
namespace Spinit\Test\Unit\OpensymapTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\ResponseException;
use Spinit\Dev\Opensymap\Opensymap;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;
use Spinit\Test\Helper\TestChannel;
use Spinit\Test\InteractorDummy;
use Spinit\Test\OpensymapDummy;
use Spinit\Test\Unit\Entity\Appli\TestApplication;

use function Spinit\Dev\AppRouter\debug;

class OpensymapTest extends TestCase {

    protected function cast (MainInteractorInterface $i) : InteractorDummy {
        return $i;
    }

    public function testInstance() {
        $osy = new OpensymapDummy();
        $this->assertNotEmpty($osy);
        $request = new Request('GET', ['localhost/osy','test/ok']);
        $osy->addRoute('test', function($request) {
            return 'test '.$request->getPath();
        });
        $response = $osy->run($request);
        $this->assertEquals('test ok', $response);
    }

    public function testInstanceApp() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
        $this->assertNotEmpty($osy);

        $appTest = $osy->getFactory()->getApplication('app@test');
        $extList = $appTest->getExtendList();
        foreach($extList as $extend) {
            $app = $extend->getApplication();
            $this->assertEquals('ok app', $app->getAdapter()->get('test'));
        }
        $request = new Request('GET', ['localhost/osy','test/ok']);
        $osy->addRoute('test', function($request) {
            return 'test '.$request->getPath();
        });
        $response = $osy->run($request);
        $this->assertEquals('test ok', $response);
    }

    public function testInstanceAppInstallation() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
        $DS = new DataSource('sqlite::memory');
        $osy->setDataSource('', $DS);
        $osy->install();
        $urn = 'urn:myTest';
        $data = $DS->query("select urn from osy_itm where urn={{url}}", ['url'=>$urn])->first();
        $this->assertEquals(['urn'=>$urn], $data);
    }
    public function testCheckUser() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
        $osy->setRequest($request = new Request('POST', 'test'));
        $user = ['Uno'=>'Due'];
        $osy->getMainInteractor()->setUser($osy->getRequest(), $user, '');
        $this->assertEquals($user, $osy->getUser()->get(''));
        $osy->setUser('');
        $this->expectException(\Exception::class);
        $osy->getUser();
        
    }

    public function testRunWithoutLogin() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
        $response = $osy->run(new Request('GET', ''));
        $this->assertEquals('login', $response->get('section'));
        $channel = new TestChannel();
        $response->send($channel);
        $this->assertStringContainsString('Login', $channel->getContent());
    }

    public function testBadAccessLogin() {

        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
    
        $this->cast($osy->getMainInteractor())->authUsers(['utente'=>'di prova']);

        $request = (new Request('POST', ''))->setApp('osy,event', 'login');
        $request->setPost(['txt_email'=>'utente', 'txt_passwd'=>'pwd errata']);
        $response = $osy->run($request);
        
        $this->assertEquals('login-access', $response->get('test'));
        $this->assertEquals('error', $response->get('status'));
    }


    public function testGoodAccessLogin() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);

        $this->cast($osy->getMainInteractor())->authUsers(['utente'=>'di prova']);

        $request = (new Request('POST', ''))->setApp('osy,event', 'login');
        $request->setPost(['txt_login'=>'utente', 'txt_passwd'=>'di prova']);
        
        $this->expectException(ResponseException::class);
        $osy->run($request);
    }

    public function testInitializer() {
        $value = 10;
        $init = function() use (&$value) {
            $value += 1; 
        };
        Opensymap::addInitializer($init);
        $osy = new OpensymapDummy();
        $this->assertEquals(11, $value);
        $osy = new OpensymapDummy();
        $this->assertEquals(12, $value);

        Opensymap::rmInitializer($init);
        $osy = new OpensymapDummy();
        $this->assertEquals(12, $value);
    }

    public function testApplication() {
        $osy = new OpensymapDummy([], ['apps'=>['app@test']]);
        $request = (new Request('POST', 'app/app@test/Main'));
        $osy->getMainInteractor()->setUser($request, ['user'=>'test'], '');
        
        $response = $osy->run($request);
        $this->assertEquals('app@test', $response->get('test'));
    }

    public function testA () {
        $args = [1, 2, 3];
        array_unshift($args, 0);
        $this->assertEquals([0,1,2,3], $args);
        array_unshift($args, [1,2]);
        $this->assertEquals([[1,2], 0,1,2,3], $args);
        array_unshift($args, 8, 9);
        $this->assertEquals([8, 9, [1,2], 0,1,2,3], $args);
    }
}
