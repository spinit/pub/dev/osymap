tinymce.PluginManager.add('osy-editor-image', function(editor, url) {
  let target = editor.targetElm;
  let cmp = target.closest('.osy-editor');
  
  function coalesce() {
    let val = '';
    for (let i in arguments) {
      val = arguments[i];
      if(val) return val;
    }
    return val;
  }
  function attr(el, att) {
    return coalesce(el && el.getAttribute(att), '');
  }
  function updImg(img, data) {
    img.src = data.src;
    img.title = data.ttl;
    img.alt = data.alt;
    img.className = data.cls;
  }
  function makeImg(data) {
    let t = document.createElement('img');
    updImg(t, data);
    return t.outerHTML;
  }
  function getTabPages(img) {
    let pages = [ {
      type: 'panel', // The root body type - a Panel or TabPanel
      name:'conf',
      title: 'Configurazione',
      items: [ // A list of panel components
        {
          type: 'input',
          name: 'ttl',
          label: 'Title'
        },
        {
          type: 'input',
          name: 'src',
          label: 'Url'
        },
        {
          type: 'input',
          name: 'cls',
          label: 'Class'
        },
        {
          type: 'input',
          name: 'alt',
          label: 'Alt'
        },
        {
          type: 'button',
          text: 'Conferma',
          name: 'conf-image',
          borderless : false,
          primary : true
        }
      ]}, 
      {
        type: 'panel', // The root body type - a Panel or TabPanel
        title: 'Remote',
        name: 'remote',
        items: [ // A list of panel components
          {
            type: 'htmlpanel', // A HTML panel component
            html: '<iframe src="'+attr(cmp, 'data-resource-url')+'" onload="this.trigger = this.closest(\'[role=dialog]\').trigger"></iframe>'
          }
        ]
      }
    ];
    tts = JSON.parse(coalesce(cmp.getAttribute('data-tabs'), '[]'));
    coalesce(tts, []).forEach(cnf => {
      pages.push({
        type:'panel', 
        title: cnf.title, 
        name: cnf.name, 
        items : [{
          type:'htmlpanel',
          html: '<iframe src="'+cnf.url+'" onload="this.trigger = this.closest(\'[role=dialog]\').trigger"></iframe>'
        }]
      });
    });
    return pages;
  }

  let ActionList = {
    'conf-image': (api, img, detail) => {
      var data = api.getData();
      // Insert content when the window form is submitted
      img ? updImg(img, data) : editor.insertContent(makeImg(data));
      api.close();
    }
  }

  var openConfiguration = function (img) {
    return editor.windowManager.open({
      title: 'Immagine', // The dialog's title - displayed in the dialog header
      size: 'medium',
      initialData : {
        ttl : attr(img, 'title'),
        src : attr(img, 'src'),
        cls : attr(img, 'class'),
        alt : attr(img, 'alt')
      },
      body: {
        type: 'tabpanel',
        class: 'osy-tab-image',
        tabs : getTabPages(img)
      },
      buttons: [ // A list of footer buttons
        /*
        {
          type: 'submit',
          text: 'Salva'
        }*/
      ],
      onAction: function (api, detail) {
        if (detail.name in ActionList) {
          ActionList[detail.name](api, img, detail);
        }
      }
    });
  };


  // Add a button that opens a window
  editor.ui.registry.addButton('osy-editor-image', {
    text: 'img',
    icon : 'image',
    onAction: function () {
      let sel = editor.selection.getNode();
      if (sel.tagName.toLowerCase() != 'img') {
        sel = null;
      }
      let f = openConfiguration(sel);
      let d = document.querySelector('.tox-dialog[role=dialog]');
      d.conf = f;
      console.log(f);
      d.classList.add('osy-editor-image-dialog');
      d.addEventListener('setResource', function(event) {
        let data = event.detail;
        f.showTab('conf');
        console.log(data);
        f.setData({'src': data.url, 'ttl' : data.title});
        console.log(event.detail, f);
      }, false);
      d.trigger = function (eventName, data) {
        d.dispatchEvent(new CustomEvent(eventName, {bubbles: true, detail : data}));
      }
    }
  });
  return {
    getMetadata: function () {
      return  {
        name: 'Opensymap Editor Image',
        url: 'http://www.opensymap.org/res/app/tinycme/plugin/osy-editor-image/'
      };
    }
  };
});
