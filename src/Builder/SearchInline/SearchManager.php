<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\SearchInline;

use Spinit\Dev\Opensymap\Command\ResponseSetContent;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Helper\OpenFormDetail;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

/**
 * Description of SearchRunner
 *
 * @author ermanno
 */
class SearchManager {
    
    private static $instance = null;
    
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function onLoadData($event, $field)
    {
        $search = $event->getParam(0)?:'';
        $form = $field->getForm();
        $response = $form->getResponse();
        
        $div = Util\Tag::create('');
        $response->addCommand('$(this).trigger("open", args[0])', $div);
        //$response->addBox($field->getName(), ['.osy-search-place'=>$div]);
        //$response->addContent($field->getName(), ['.osy-search-place'=>$div]);
        try {
            $list = (new MakerData())->build($field, $search, $div);
        } catch (\Exception $e) {
            debug($e->getMessage(), $field->getForm()->getDataSource()->getCommandLast());
        }
//        $list = $this->buildData($field, $event->getParam(1), $div);
        $this->makeData($field, $div, $list);
    }
    
    private function makeData($field, $div, $lista)
    {
        $idx = 0;
        $limit = Util\nvl($field->get('limit'), 12);
        $groupCurr = '';
        if ($field->get('empty')!='no') {
            $item = $div->add(Util\Tag::create('div'))
                    ->att('class','osy-search-item idx'.$idx)
                    ->att('data-id', '')
                    ->att('data-name', '');
            $item->add('<div style="text-align:center; color:silver;">'.$field->get('emptyMessage', 'Empty').'</div>');
            $idx += 1;
        }
        foreach($lista as $data) {
            if ($idx>$limit) {
                $div->add(Util\Tag::create('div'))
                        ->att('class','osy-search-stop idx'.$idx)
                        ->add('... more ...');
                break;
            }
            $group = '';
            if (array_key_exists('__group', $data)) {
                $group = $data['__group'];
                unset($data['__group']);
            }
            // i primi due campi senza "_" sono l'id e la descrizione
            $kid = $knme = $klbl = trim(array_shift($data));
            if (count($data)) {
                $dsc = trim(array_shift($data));
                if ($dsc) {
                    $knme = $klbl = $dsc;
                }
            }
            if ($groupCurr != $group) {
                $groupCurr = $group;
                $div->add(Util\Tag::create('div'))
                    ->att('class','osy-search-group')
                    ->add($group);
            }
            
            foreach($data as $nf => $vf) {
                if ($nf[0] != '_') {
                    $klbl = trim(array_shift($data));
                    break;
                }
            }
            $item = $div->add(Util\Tag::create('div'))
                        ->att('class','osy-search-item idx'.$idx);
            if (Util\arrayGet($data, '__disable')) {
                $item->att('class', 'disable', ' ');
            }
            unset($data['__disable']);
            if (Util\arrayGet($data, '__class')) {
                $item->att('class', Util\arrayGet($data, '__class'), ' ');
            }
            unset($data['__class']);
            $item->att('data-id', $kid)
                 ->att('data-name', $knme)
                 ->att('data-param', $data);
            $item->add('&nbsp'.$klbl);
            $idx += 1;
        }
        if ($field->get('empty')=='no' && $idx == 0) {
            $div->add(Util\Tag::create('div'))
                    ->att('class','osy-search-group')
                    ->att('style', 'color: red; txt-align:center')
                    ->add('Not Found');
        }
    }
    
    private function setOtherItemInForm($field, $param) {
        foreach($param?:[] as $name=>$jvalue) {
            $value = json_decode($jvalue, 1)?:$jvalue;
            try {
                $field->getForm()->getItem($name)->setValue($value, 1);
            } catch (\Exception $e) {}
        }
    }
    public function onSelectData($event, $field)
    {
        $response = $field->getForm()->getResponse();
        // il valore passato viene caricato come effettivo
        // - per controllarlo usare select-before
        $newValue = $event->getParam(0);
        $field->setValue($newValue, 1);
        // vengono impostati i campi passati come parametro
        $this->setOtherItemInForm($field, json_decode((string) arrayGet($newValue, 'param'), true));
        //$response->addCommand("$(args[0]).trigger('setValue', args[1])", '[id="'.$field->getName().'"]', $field->getValue());
        switch ($field->get('onchange')) {
            case 'submit':
                $response->addExec('submit');
                break;
            case 'refresh':
                $response->addExec('refresh');
                break;
            default:
                if ($field->get('mask')) {
                    // forzo il carimaento dei dati nei parametri
                    $data = []; //$this->getDataValue($field, $event->getParam(1));
                    $value = ['id' => array_shift($data), 'name'=>array_shift($data)];
                    $field->setValue($value);
                    $response->addContent($field->getName(), ['.mask'=> $this->getMaskData($field, 0)]);
                }
                else {
                    $field->build($response);
                }
                //$response->addCommand('desktop.trigger(this, "close")');
        }
    }

    private function getMaskData($field, $wrapper = 1)
    {
        if ($field->get('mask')) {
            $data = $field->getParam('data');
            if (!Util\arrayGet($field->getValue(), 'id')) {
                $data = false;
            }
            return '---'; //$this->makeMask($field->get('mask'), $data, $wrapper);
        }
        return '';
    }
    
    public function onOpenFormDetail($event, $field)
    {
        // viene caricata la nuova form con la chiave eventualmente indicata
        $key = $event->getParam(1);
        if (is_array($key)) ksort($key);
        $url = $key ? base64_encode(json_encode($key)) : '';
        $newForm = new OpenFormDetail($field);
        $newForm->exec($url);
    }
    
    public function onOpenSelfDetail($event, $field)
    {
        $data = (array) $field->getParam('data');
        if ($fpk = (string) $field->get('formDetailPkey')) {
            $key = json_decode((string) Util\arrayGet($data, $fpk), 1);
            if (!$key) {
                $key = ['id'=>Util\arrayGet($data, $fpk)];
            }
        } else {
            $key = ['id' => Util\arrayGet($field->getValue(), 'id')];
        }
        // valori non validi per aprire una form
        if (in_array(implode('', $key), ['', '--'])) {
            return '';
        }
        // viene caricata la nuova form con la chiave eventualmente indicata
        if (is_array($key)) ksort($key);
        $url = base64_encode(json_encode($key));
        $newForm = new OpenFormDetail($field);
        $newForm->exec($url);
    }
    
    public function onModelLoad($event, $field)
    {
        $model = $event->getParam(0);
        if (!$model) {
            return;
        }
        // se non è una proprietà ... viene verificato se non sia stato
        // impostato un multi-field
        if ($field->get('property')) {
            return;
        }
        $ff = Util\asArray($field->get('field'), ',');
        switch (count($ff)) {
            case 0:
                return;
            case 1:
                $rec =$this->getRecord($field, $model->get($ff[0]));
                $key = array_shift($rec);
                $dsc = array_shift($rec);
                break;
            default:
                $key = $ff[0];
                $dsc = $ff[1];
        }
        $field->setValue(['id'=>$key, 'name'=>$dsc]);
    }
    
    public function onModelStore($event, $field)
    {
        $model = $event->getParam(0);
        if ($field->get('property')) {
            return;
        }
        if (!$model) {
            return;
        }
        $ff = Util\asArray($field->get('field'), ',');
        $value = $field->getValue();
        switch (count($ff)) {
            case 0:
                return;
            case 1:
                $model->set($ff[0], Util\arrayGet($value, 'id'));
                break;
            default:
                $model->set($ff[0], Util\arrayGet($value, 'id'));
                $model->set($ff[1], Util\arrayGet($value, 'name'));
        }
    }
    public function onCheckValue($event, $field) {
        if (Util\arrayGet($field->getValue(), 'id') == '') {
            $field->setValue(['id'=>'', 'name'=>Util\arrayGet($field->getValue(), 'name')]);
        }
        if (Util\arrayGet($field->getValue(), 'id') and !Util\arrayGet($field->getValue(), 'name')) {
            $rec = $this->getRecord($field, Util\arrayGet($field->getValue(), 'id'));
            $field->setValue(['id'=>array_shift($rec), 'name'=>array_shift($rec)]);
        }
    }
    public function getRecord($field, $key, $cmnt = '') {
        if (!$key) {
            return [];
        }
        if ($dsPrp = $field->get('dataset')) {
            debug($dsPrp);
            if (is_string($dsPrp)) {
                $ds = Util\getInstance($dsPrp, $field, '', $key);
                return $ds->current();
            } else if (is_array($dsPrp)) {
                foreach(Util\arrayGet($dsPrp, 'item') as $option) {
                    if (strtoupper((string)Util\arrayGet($option, 'key')) == strtoupper($key) and $key) {
                        return [Util\arrayGet($option, 'key'), Util\arrayGet($option, 'value')];
                    }
                }
            }
        }
        $rec = (new MakerData())->buildPkey($field, $key, $cmnt);
        return $rec;
    }
}
