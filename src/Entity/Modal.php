<?php
namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ModalAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ModalInterface;
use Spinit\Dev\Opensymap\ViewEvent\ModalLoad;
use Spinit\Util\ExecCodeTrait;
use Spinit\Util\Tag;
use Spinit\Util\TriggerTrait;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

class Modal extends View implements ModalInterface {
    
    private $form;
    private $cmds;

    public function __construct(FormInterface $form, ModalAdapterInterface $modal)
    {
        $this->form = $form;
        parent::__construct($modal);
        $loader = (new ModalLoad($this));
        $this->bindExec('@load', [$loader, 'doExec']);
        $this->bindAfter('@load', [$loader, 'doAfter']);
        $this->bindExec('@rewriteValue', function($event) {
            $this->getForm()->getRequest()->setApp('modal,'.$event->getParam(0), $event->getParam(1));
            $this->getForm()->getResponse()->set('values,_[modal]['.$event->getParam(0).']', $event->getParam(1));
        });
        $this->bindExec('@build', function() {
            $this->writeResponse();
        });
    }

    protected function init() {
        $modal = function () : ModalAdapterInterface {
            return $this->getAdapter();
        };
        $this->cmds = $this->makeItemList($modal()->getCommandList());
    }
    public function getPath() {
        $modal = function () : ModalAdapterInterface {
            return $this->getAdapter();
        };
        return $modal()->getPath();
    }
    public function getForm(): FormInterface
    {
        return $this->form;
    }
    public function getDataSource($what = '')
    {
        return $this->getForm()->getDataSource($what);
    }
    /**
     * Recupera il builder gestito dalla factory (così da essere condiviso tra gli item)
     */
    public function getBuilder($name) : BuilderInterface {
        $factory = $this->getForm()->getApplication()->getInstance()->getFactory();
        $builder = null;
        if ($builderClass = $this->getAdapter()->getBuilder($name)) {
            $builder = $factory->makeBuilder($builderClass, $this->getForm());
        } else {
            $builder = $this->getForm()->getBuilder($name);
        }
        Assert::notNull($builder);
        return $builder;
    }

    protected function makeItem($data): ItemInterface
    {
        return new ItemModal($this, $data);
    }

    public function getUri() : string {
        return $this->getForm()->getUri().'/Modal/'.$this->getName();
    }

    private function getContent($data)
    {
        $content = new Tag('');
        foreach($data?:[] as $field) {
            try {
                // aggiunta del contenuto
                $content->Add($field->build());
            } catch (\Exception $e) {
                $content->add($e->getMessage());
                $content->add('<pre>'.$e->getTraceAsString().'</pre>');
            }
        }
        return $content;
    }
    
    public function writeResponse($response = null)
    {
        if (!$this->getAdapter()) {
            return;
        }
        $adapter = function () : ModalAdapterInterface {
            return $this->getAdapter();
        };

        if (!$response) {
            $response = $this->getForm()->getResponse();
        }
        $response->addExec('modal', [
            'name'=>$this->getName(),
            'title'=>$adapter()->get('title'),
            'body'=>$this->getContent($this->getItemList()),
            'command' => $this->getContent(arrayGet($this->cmds, 'root')),
            'noBtnClose' => $adapter()->get('noBtnClose'),
            'yesBtnBack' => $adapter()->get('yesBtnBack')
        ], $adapter()->getInitCode());
        return $response;
    }
    public function build() {
        $this->trigger('@build');
    }
}