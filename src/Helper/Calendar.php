<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Util\Tag;
use Spinit\Util\Table;

use function Spinit\Dev\AppRouter\debug;

class Calendar
{
    private $date;
    private $month;
    private $obs = array();
    private $min;
    private $max;
    private $hasHour = false;
    private $hour;
    private $notEmpty = false;
    
    public function __construct($date, $month = '', $min='', $max='')
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        $part = explode(' ', $date);
        if (count($part)>1) {
            $hh = explode(':', $part[1]);
            if (count($hh)>1) {
                $this->hour = $hh[0].':'.$hh[1];
            }
        }
        $this->date = $this->normalize($part[0]);
        $date = explode('-', $this->date);
        if (!$month) {
            $month = $date[0].'-'.$date[1];
        }
        $this->month = explode('-', $month);
        $this->min = $this->normalize($min);
        $this->max = $this->normalize($max);
    }
    
    /**
     * Disabilita/abilita l'icona per impostare la data nulla
     * @param type $notEmpty
     * @return $this
     */
    public function setNotEmpty($notEmpty = true)
    {
        $this->notEmpty = $notEmpty;
        return $this;
    }
    public function withHour($has = true)
    {
        $this->hasHour = $has;
        return $this;
    }
    
    private function normalize($date)
    {
        list($date) = explode(' ', $date);
        $d = explode('-', $date);
        if (count($d)<2) {
            $d = array_reverse(explode('/', $date));
        }
        if (count($d) < 2) return '';
        $d[1] = str_pad($d[1], 2, '0', STR_PAD_LEFT);
        $d[2] = str_pad($d[2], 2, '0', STR_PAD_LEFT);
        return implode('-', $d);
    }
    
    public function getTag($name)
    {
        $cnt = new Tag('div');
        $inp = $cnt->Add(new Tag('input'))
            ->Att('name',$name.'[cur_month]')
            ->Att('class', 'curmonth')
            ->Att('type', 'hidden')
            ->att('value', implode('-', $this->month));
        $wrap = $cnt->att('class','calendar')->add(new Table());
        $table = new Table();
        $table->att('align', 'center')
              ->att('style', 'text-align:center;');
        $this->writeHead($table->Cell(new Tag('div')));
        $table->Row();
        $this->writeBody($table->Cell(new Tag('div')));
        $wrap->cell($table)->att('style','vertical-align:top;');
        if ($this->hasHour) {
            $wrap->cell(new Tag('div'))->att('style', 'border-left:1px solid silver;border-top:1px solid silver;vertical-align:top;')
                  ->add($this->getHour());
        }
        return $cnt;
    }
    protected function getHour()
    {
        $cnt = new Tag('div');
        $cnt->att('class', 'center-text')->att('class','hour');
        $cnt->add(new Tag('input'))->att('size','4')->att('value', $this->hour)->att('class', 'hval');
        for($i = 6; $i<23; $i += 2) {
            $cnt->add(new Tag('div'))->att('class','help')->add(str_pad($i, 2, '0', STR_PAD_LEFT).':00');
        }
        return $cnt;
    }
    public function writeHead($content)
    {
        $premonth = date('Y-m',mktime(0,0,0,$this->month[1]-1, 1, $this->month[0]));
        $sucmonth = date('Y-m',mktime(0,0,0,$this->month[1]+1, 1, $this->month[0]));
        $ll = $content->Add(new Tag('div'))->att('style','text-align:center');
        $ll->add(new Tag('div'))
            ->att('style','float:left; cursor:pointer; font-size:150%; padding:2px 5px; margin-top:-5px;')
            ->att('osy-trigger', 'setMonth')
            ->att('osy-param', $premonth)
            ->add('&laquo;');
        $ll->add(new Tag('div'))
            ->att('style','float:right; cursor:pointer; font-size:150%; padding:2px 5px; margin-top:-5px;')
            ->att('osy-trigger', 'setMonth')
            ->att('osy-param', $sucmonth)
            ->add('&raquo;');
        
        $mon = array(1=>'Gen', 'Feb', 'Mar', 'Apr','May', 'Giu','Jul', 'Aug', 'Sep', 'Opt', 'Nov', 'Dec');
        $ll->add(new Tag('span'))->add($mon[intval($this->month[1])].' '.$this->month[0]);
        if (!$this->notEmpty) {
            $ll->add(new Tag('code'))
               ->att('class', 'day-empty')
               ->att('osy-trigger', 'setDate')
               ->att('osy-param', ['', ''])
               ->add('[ X ]');
        }
    }
    public function writeBody($content)
    {
        $cal = $content->add(new Table());
        $day = array('L','M','M', 'G', 'V', 'S', 'D');
        foreach($day as $d) {
            $cal->head($d)->att('class','week');
        }
        $cal->Row();
        $w = date('w', mktime(0,0,0,$this->month[1], 1, $this->month[0]));
        $end = date('d', mktime(0,0,0,$this->month[1]+1, 0, $this->month[0]));
        if (!$w) {
            $w = 7;
        }
        $w -= 1;
        // intestazione
        for($i=0; $i<$w; $i++) {
            $cal->cell('');
        }
        for($i=1; $i<=7-$w; $i++) {
            $cal->cell($this->makeDay($i));
        }
        while($i<$end) {
            $cal->Row();
            for($j=0; $j<7; $j++, $i++) {
                $cal->cell( $i <= $end ? $this->makeDay($i) : '');
            }
        }
        
    }
    
    private function makeDay($i)
    {
        $day = (new Tag('div'))->att('class','day');
        $day->add($i);
        $date = date('Y-m-d', mktime(0,0,0,$this->month[1], $i, $this->month[0]));
        if ($date == $this->date) {
            $day->att('class', 'thisday', ' ');
        }
        $wdate = date('d/m/Y', mktime(0,0,0,$this->month[1], $i, $this->month[0]));
        if ($this->min and $this->min > $date) {
            $day->att('class', ' disabled', ' ');
            $day->att('debug', 'min '.$this->min.' '.$date);
            return $day;
        }
        if ($this->max and $this->max < $date) {
            $day->att('class', ' disabled', ' ');
            $day->att('debug', 'max '.$this->max);
            return $day;
        }
        
        foreach($this->obs as $obs) {
            try {
                $obs->checkDay($date, $day);
            } catch (\Exception $e) {
                // se c'è un errore allora viene disabilitato
                $day = (new Tag('div'))->att('class','day disabled');
                $day->add($i);
                return $day;
            }
        }
        $day->att('osy-trigger', 'setDate');
        $day->att('osy-param', '["'.$date.'","'.$wdate.'"]');
        return $day;
    }
    
    static public function dtCheck($value, $from = '/', $to = '-')
    {
        $value = trim($value);
        if (!$value) {
            return '';
        }
        $part = explode(' ', trim($value));
        $tick = explode($from, $part[0]);
        if (count($tick)!=3) {
            return $value;
        }
        $tock = array_reverse($tick);
        $part[0] = implode($to, $tock);
        return implode(' ', $part);
    }
    
    static public function dtCheckNoH($value, $from = '/', $to = '-')
    {
        $val = explode(' ', $value);
        return self::dtCheck(array_shift($val), $from, $to);
    }
}