(($)=>{
    // clear event namespace
    $(document).off('.osy-input-text');
    $(document).on('setValue.osy-input-text', '[osy-item].osy-input-text', function(event, param) {
        if (this.matches('input[type=hidden]')) {
            this.value = param;
        } else {
            let inp = this.querySelector('input,textarea');
            if (inp.matches('[type=checkbox]')) {
                inp.checked = !!param;
            } else {
                if (typeof(param)==typeof({})) {
                    inp.value = param['name'];
                } else {
                    inp.value = param;
                }
            }
        }
    });
    $(document).on('keydown.osy-input-text', '[osy-item].osy-input-text', function(event, param) {
        let inp = event.srcElement;
        inp.ovalue = inp.value;
        console.log(event, inp.value, $(inp).attr('pattern'));
    });
    $(document).on('keyup.osy-input-text', '[osy-item].osy-input-text', function(event, param) {
        let inp = event.srcElement;
        let ptr = $(inp).attr('pattern'); 
        if (ptr) {
            let re = new RegExp(ptr, 'i');
            console.log(re, re.test(inp.value), inp.ovalue, inp.value);
            if (!re.test(inp.value)) inp.value = inp.ovalue;
        }
    });
})(desktop.$);