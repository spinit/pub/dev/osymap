<?php
namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\Request;

interface FactoryInterface {
    function getApplication($name) : ApplicationInterface;
    function getMenu() : iterable;
    function makeBuilder(string $name, ViewInterface $form) : BuilderInterface;
    function getBuilder(string $name) : string;
    function setBuilder($name, $ctor);
    function addApplication(string $name, $initer, $use = '', $conf = []);
    function makeRequest($method, $path, $data = null) : Request;
    function getApplicationUseList($use = '');
}