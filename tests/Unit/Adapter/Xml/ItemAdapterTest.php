<?php
namespace Spinit\Test\Unit\Adapter\Xml;

use PHPUnit\Framework\TestCase;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;

class ItemAdapterTest extends TestCase {
    private $obj;
    protected function setUp() : void
    {
        parent::setUp();
        $app = new TestApplicationAdapter();
        foreach($app->getForm('Core:Test')->getItemList() as $item) {
            $this->obj = $item;
            break; // prende il primo elemento
        }
    }

    public function testGetItemList() {
        $items = $this->obj->getItemList();
        $item = $items->current();
        $this->assertEquals($this->obj, $item->getParent());
        $this->assertEquals('itm1', $item->getName());
        $this->assertEquals('value', $item->get('data'));
        $this->assertEquals([
            '@'=>['name'=>'struct'],
            'uno'=>'due', 
            'cols'=>[
                'col'=>[
                    ['a'=>'1', 'b'=>'2', '@'=>['name'=>'ciao']], 
                    ['a'=>'1', 'b'=>'2'], 
                    ['a'=>'1', 'b'=>'2']
                ]
            ]
        ], $item->get('struct'));
        $items->next();
        $this->assertNull($items->current());
    }
}