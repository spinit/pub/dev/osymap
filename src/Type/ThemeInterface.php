<?php

namespace Spinit\Dev\Opensymap\Type;

interface ThemeInterface {
    public function getPath($dir) : string;
    public function getStdPath($dir) : string;
}