<?php

use function Spinit\Dev\AppRouter\debug;

$dgr = $this->getItem('dgr_aut_only');
$id_el = $this->getItem('hdn_id')->getValue();

$model = $this->getModel();
$tt = $this->getDataSource()->select("osy_itm", ['urn'=>"urn:opensymap.org:user-rel#instance-url"])->first();
$ckList = $this->getDataSource()->select(
    "osy_ang__rel", 
    [
        'id_ang'=>$id_el, 
        'id_typ' => $tt['id']
    ],
    'rel_id, id'
)->getList();

foreach($dgr->getValue('check')?:[] as $ck) {
    if (array_key_exists($ck, $ckList)) {
        unset($ckList[$ck]);
        continue;
    }
    $this->getDataSource()->insert('osy_ang__rel', ['id'=>$this->getDataSource()->getCounter()->next(), 'id_ang'=>$id_el, 'id_typ'=>$tt['id'], 'rel_id'=>$ck]);
}
foreach($ckList as $idck) {
    $this->getDataSource()->delete('osy_ang__rel', $idck);
}
