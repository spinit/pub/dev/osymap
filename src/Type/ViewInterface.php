<?php
namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Util\ParamInterface;
use Spinit\Util\TriggerInterface;

interface ViewInterface extends  TriggerInterface, ParamInterface {
    function getItemList() : iterable;
    function getItem($name) : ItemInterface;
    function getBuilder($name) : ?BuilderInterface;
    function getUri() : string;
    function getForm() : FormInterface;
    function get($name);
    function getDataSource($what = '');
    function getData();
    function setParam($name);
    function checkItemsValue($funcOkCheck = null);
}