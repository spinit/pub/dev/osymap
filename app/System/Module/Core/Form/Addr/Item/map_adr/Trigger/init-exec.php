<?php
$form = $this->getForm();
$hid = $form->getField('hdn_id')->getValue();
$lat = $form->getField('txt_lat')->getValue();
$lng = $form->getField('txt_lng')->getValue();

$form->getResponse()->set('map',['center'=>[$lng, $lat], 'zoom'=>9]);
$token = $form->getInstance()->getEnv('google:gmap:api-key');

if ($token) {
    $form->getResponse()->addCommand("$(this).trigger('setMarker', ['{$hid}', ['{$lng}', '{$lat}'],
        (marker)=>{
            marker.setDraggable(true);
            google.maps.event.addListener(marker, 'dragend', ()=>{
                var lngLat = marker.getPosition();
                $('#txt_lng').trigger('setValue', [lngLat['lng']]);
                $('#txt_lat').trigger('setValue', [lngLat['lat']]);
            });
        }
    ])");
} else {
    $form->getResponse()->addCommand("$(this).trigger('setMarker', ['{$hid}', ['{$lng}', '{$lat}'],
        (marker)=>{
            marker.setDraggable(true);
            marker.on('dragend', ()=>{
                var lngLat = marker.getLngLat();
                $('#txt_lng').trigger('setValue', [lngLat['lng']]);
                $('#txt_lat').trigger('setValue', [lngLat['lat']]);
            });
        }
    ])");
}
