<?php
namespace Spinit\Test\Helper\MergeMenuBarTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\Opensymap\Helper\MergeMenuBar;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class MergeMenuBarTest extends TestCase {

    public function testMerge() {
        $merger = new MergeMenuBar();
        $osy = new OpensymapDummy();
        $app = $osy->getApplication('app@test');
        $this->assertNotNull($app);
        $module = $app->getModule('Core');
        $this->assertNotNull($module);
        $mbars = $module->getMenuBars('std');
        $this->assertCount(1, $mbars);
        $mbars = $module->getMenuBars('std,test');
        $this->assertCount(2, $mbars);
    }
}