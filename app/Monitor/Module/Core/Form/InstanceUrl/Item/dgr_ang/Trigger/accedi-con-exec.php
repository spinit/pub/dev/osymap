<?php

use function Spinit\Dev\AppRouter\debug;

$user = $event->getParam(0);

$form = $this->getForm();
$instance = $form->getApplication()->getInstance();
$main = $instance->getMainInteractor();
$openIce = $main->getInstance($form->getItem('hdn_id')->getValue());
$openIce->setRequest($instance->getRequest());
$openIce->setUser($user);
$res = $openIce->getResponse();
$res->addCommand('location.href = args[0]', $openIce->makePath(''));
$res->stop();