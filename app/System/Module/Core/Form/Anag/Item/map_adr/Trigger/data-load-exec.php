<?php
use Spinit\Dev\Opensymap\Core\Helper\MakerData;
$list = [];
$bound = false;
foreach((new MakerData())->build($this, $this->getValue('search')) as $rec) {
    $delta = 0.05;
    if (!$bound) $bound = [$rec['lng'] - $delta, $rec['lat'] - $delta, $rec['lng'] + $delta, $rec['lat'] + $delta];
    
    if ($bound[0] > $rec['lng']) $bound[0] = $rec['lng'] - $delta;
    if ($bound[1] > $rec['lat']) $bound[1] = $rec['lat'] - $delta;
    if ($bound[2] < $rec['lng']) $bound[2] = $rec['lng'] + $delta;
    if ($bound[3] < $rec['lat']) $bound[3] = $rec['lat'] + $delta;
    $list []= $rec;
}
if (!count($list)) {
    return;
}

$resp = $this->getForm()->getResponse();
$resp->set('map', ['data'=>$list,'bound'=>$bound, 'mask'=>$this->get('mask')]);

