<?php 
namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\AppRouter\Data;
use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Dev\Opensymap\Type\Adapter\ViewAdapterInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\ViewInterface;
use Spinit\Util\ExecCodeTrait;
use Spinit\Util\ParamTrait;
use Spinit\Util\TriggerInterface;
use Spinit\Util\TriggerTrait;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\arrayGetAssert;

abstract class View implements ViewInterface{

    use HasAdapterTrait {
        setAdapter as private _setAdapter;
        getAdapter as private _getAdapter;
    }
    use ExecCodeTrait;
    use TriggerTrait;
    use ParamTrait;
    
    private $items;
    private $data;

    public function __construct(ViewAdapterInterface $adapter)
    {
        $this->bindExec('@init', function () {
            $this->items = $this->makeItemList($this->getAdapter()->getItemList());
            $this->init();
        });
        $this->data = new Data();
        $this->setAdapter($adapter);
    }
    protected function init() {

    }
    protected function makeItemList($list) {
        $items = ['root'=>[], 'list'=>[]];
        $createItem = function($aa, $parent = null) use (&$createItem, &$items) {
            $ii = $this->makeItem($aa);
            if ($parent) $parent->addItem($ii);
            if ($name = $ii->get('name')) {
                $items['list'][$name] = $ii;
            }
            foreach($aa->getItemList() as $ab) {
                $createItem($ab, $ii);
            }
            $ii->trigger('@init');
            return $ii;
        };
        foreach($list as $aitem) {
            $items['root'][] = $createItem($aitem);
        }
        return $items;
    }
    public function getAllItemsAsList() {
        return $this->items['list'];
    }

    public function getItem($name) : ItemInterface {
        $f = function($i) : ItemInterface {
            return $i;
        };
        list($firstPart, ) = explode('[', $name);
        if ($firstPart == '__disable') debug($name);
        return $f(arrayGetAssert($this->items['list'], $firstPart));
    }

    public function getItemList() : iterable {
        return (array) arrayGet($this->items,'root', []);
    }

    protected function setAdapter(ViewAdapterInterface $adapter) {
        $this->_setAdapter($adapter);
        $this->trigger('@init');
    }
    public function getAdapter() : ViewAdapterInterface {
        return $this->_getAdapter();
    }

    public function getData() {
        $args = func_get_args();
        if (count($args) == 0) {
            return $this->data;
        }
        return call_user_func_array([$this->data, 'get'], $args);
    }
    protected function makeItem($data) : ItemInterface {
        return new Item($this, $data);
    }
    public function set($name, $value) {
        $this->getAdapter()->set($name, $value);
        return $this;
    }
    public function get($name, $default = '') {
        return $this->getAdapter()->get($name, $default);
    }
    public function has($name) {
        return $this->getAdapter()->has($name);
    }
    public function getName($full = true) {
        return $this->getAdapter()->getName($full);
    }
    
    public function afterTrigger($event, $target) {
        // esecuzione codice personalizzato
        $response = $this->getForm()->getResponse();
        $request = $this->getForm()->getRequest();

        $this->execCode(
            $this->getAdapter()->getTrigger($event['name'], $event['when']),
            array('event' => $event, 'target'=>$target, 'request'=>$request, 'response'=>$response),
            "{$event['name']}-{$event['when']}_".str_replace('/', '-', $this->getUri())
        );
    }

    public function checkItemsValue($funcOkCheck = null) {
        $errors = [];
        $updater = function($list) use (&$updater, &$errors, $funcOkCheck) {
            foreach($list?:[] as $item) {
                if (!$item->isVisible()) continue;
                if ($item->get('disable')) continue;
                //if ($this->getForm()->get('debug')=='1') var_dump($item->getValue());
                if ($item->get('notNull') and $item->getBuilder()->isNull($item)) {
                    if (!$item->getName()) throw new MessageError('Nome non impostato sul componente : '.$item->get('type'));
                    $errors[]= $item->getName();
                } else if ($funcOkCheck) {
                    if (is_callable($funcOkCheck)) call_user_func($funcOkCheck, $item);
                }
                $updater($item->getBuilder()->getItemList($item));
            }    
        };
        $updater($this->getItemList());
        return $errors;
    }
}