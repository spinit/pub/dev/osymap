<?php
namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;
use Spinit\Dev\Opensymap\Type\ViewInterface;

class ItemController {
    /**
     * @type ItemInterface
     */
    private $field;
    
    public function __construct(ItemInterface $field) {
        $this->field = $field;
    }
    public function getItem() : ItemInterface {
        return $this->field;
    }
    public function getView() : ViewInterface {
        return $this->getItem()->getView();
    }
    public function getForm() : FormInterface {
        return $this->getView()->getForm();
    }
    public function getApplication() : ApplicationInterface {
        return $this->getForm()->getApplication();
    }
    public function getInstance() : OpensymapInterface {
        return $this->getApplication()->getInstance();
    }
    public function getDatasource($name='') {
        return $this->getInstance()->getDataSource($name);
    }
}
