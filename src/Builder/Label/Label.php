<?php
namespace Spinit\Dev\Opensymap\Builder\Label;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Helper\OpenFormDetail;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

/**
 * Description of TextBox:Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Label extends Builder
{
    
    public function registerEvent() {

        $this->bindExec('open', function($event, $item) {
            //debug('ok');
            if (!$item->get('open-disable')) {
                $form = new OpenFormDetail($item);
                $form->exec($item->getValue());
            }
        });

        $this->bindExec('@modelLoad', function($event, $item) {
            $model = $event->getParam(0);
            $part = asArray($item->get('field'), ',');
            if (count($part)>1) {
                $item->setValue(['id'=>$model->get($part[0]), 'name'=>$model->get($part[1])], true);
            }
        });

        $this->bindExec('@load', function ($event, $field) {
            // se c'è una fonte dati allora occorre caricare i dati da mostare
            $div = Util\Tag::create('');
            try {
                if ($field->get('datasource') or $field->get('dataset')) {
                    $rec = (new MakerData())->buildPkey($field, $field->getValue(), $div);
                } else {
                    // altrimenti si utilizza il model della form corrente
                    $rec = $this->getForm()->getModel();
                }
                $field->setParam('data', $rec);
                $field->setParam('cmnt', $div);
            } catch (\Exception $e) {
                $field->setParam('error', [
                    'message'=>$e->getMessage(), 
                    'command'=> $field->getForm()->getDataSource()->getCommandLast(),
                    'trace'=>$e->getTraceAsString()
                ]);
            }
        });
    }


    public function build(ItemInterface $field, $withTitle = true)
    {
        $this->setForm($field->getForm());
        $this->addResource('Label.js');
        $this->addResource('Label.css');
        
        $div = $this->makeComponent($field, 'osy-label', $withTitle);
        if ($field->getParam('error')) {
            $div->add("<div style='padding:20px; color:red;'>ERROR</div><script type='text/error'>".print_r($field->getParam('error'),1)."</script>");
            $div->att('class', 'error', ' ');
        } else {
            // così i dati sono disponibili anche agli altri field della form/modal
            if ($mask = $field->get('mask')) {
                $req = $field->getForm()->getRequest();
                list($cnt, ) = Util\normalize($mask, 
                    $field->getParam('data'), 
                    $req->getPost(), 
                    $req->getApp(),
                    $field->getView()->getData()->get()
                );
            } else if (is_array($data = $field->getParam('data'))) {
                array_shift($data);
                $cnt = array_shift($data);
            } else {
                $cnt = $field->getValue();
                if (is_array($cnt)) $cnt = array_key_exists('name', $cnt)?$cnt['name']:$cnt['id'];
                if ($frm = $field->get('formatter')) {
                    $cnt = $this->getInstance()->getFormatter($frm)->format($cnt);
                }
            }
            if ($field->get('name')) {
                $vid = $field->getValue();
                if (is_array($vid)) $vid = $vid['id'];
                $div->add(Util\Tag::create('input'))->att('type', 'hidden')
                ->att('name', $field->getName().'[id]')
                ->att('value', $vid)
                ->att('data', $field->getValue());
            }
            $btn = '';
            if ($field->get('formDetail') and !$field->get('open-disable')) {
                $btn = '<div class="osy-label-btn" osy-event="open">...</div>';
            }
            $div->add(Util\Tag::create('div'))
                ->att('class', 'osy-body align-'.$field->get('align'))
                ->add($btn.'<div class="content">'.$cnt.'</div>');
            $div->add($field->getParam('cmnt'));
        }
        return $div;
    }
    
    public function checkValue($field, $value) {
        if (!is_array($value)) {
            $value = ['id'=>$value, 'name'=>$value];
        }
        return $value;
    }
    public function getValue($field) {
        return (string) $field->getValue('id');
    }

    protected function storeInModelFieldValue($model, $field, $value, $item)
    {
        // Label non dovrebbe poter salvare
        if (!$item->get('save')) {
            return;
        }
        $fieldList = asArray($field, ',');
        $firstField = array_shift($fieldList);
        $model->set($firstField, arrayGet($value, 'id'));
        $secondField = array_shift($fieldList);
        $secondField and $model->set($secondField, arrayGet($value, 'name'));
    }

}
