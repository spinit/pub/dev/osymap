<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Command\ResponseAddCommand;

use function Spinit\Dev\AppRouter\debug;

class FormDelete extends EventHandlerForm {

    public function doExec($event) {
        $model = $this->getForm()->getModel();
        $model->load($this->getForm()->getPkey());

        $pkey = $model->getPkey();
        foreach($pkey as $k=>$v) $pkey[$k] = '';

        if($model->getPkey()) $model->delete();
        
        // viene passata la chiave vuota
        $this->getForm()->getResponse()->addExec('back', $pkey);
}
}