<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

interface ViewAdapterInterface extends AdapterInterface {
    function getName($full = true) : string;
    function getItemList() : iterable;
    function getBuilder($name) : string;
    function getRoot();
    function getTrigger($event, $when): string;
    function getAssetList() : iterable;
}