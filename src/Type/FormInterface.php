<?php

namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;

interface FormInterface extends ViewInterface {
    function run(Request $request) : Response;
    function getResponse() : Response;
    function getRequest() : ?Request;
    function getApplication() : ApplicationInterface;
    function getModule() : ModuleInterface;
    function addResource($file);
    function getResourceList() : iterable;
    function getModel();
    function setPkey($pkey);
    function getPkey();
    function getModal($name): ModalInterface;
    function getUrl() : string;
}