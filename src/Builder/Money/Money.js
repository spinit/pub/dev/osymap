(($)=>{
    $(document).off('.osy-money');
    $(document).on('setValue.osy-money', '[osy-item].osy-money', function(event, param) {
        this.querySelector('input.osy-money-id').value = param['id'];
        this.querySelector('input.osy-money-name').value = param['name'];
    });
 
    let chx = null;
    $(document).on('keyup', '.osy-money', function(event) {
        let $inp = $(event.target);
        if (!$inp.is('.osy-money-name')) {
            return;
        }
        clearTimeout(chx);
        chx = setTimeout(() => desktop.sendEvent(this, ['change']), 500);
    });

    $(document).on('check', '.osy-money .osy-money-name', function(event) {
        clearTimeout(chx);
        chx = 0;
        if (this.closest('form.osy-sending')) return;
        desktop.sendEvent(this, ['change', 'blur']);
    });

})(desktop.$);