<?php

namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Type\Adapter\OverlayAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\OverlayInterface;

use function Spinit\Dev\AppRouter\debug;

class Overlay implements OverlayInterface {

    use HasAdapterTrait {
        getAdapter as private _getAdapter;
    }

    private $app;

    public function __construct(ApplicationInterface $app, OverlayAdapterInterface $adapter)
    {
        $this->app = $app;
        $this->setAdapter($adapter);    
    }

    public function getAdapter() : OverlayAdapterInterface {
        return $this->_getAdapter();
    }
    
    public function getApplication(): ApplicationInterface
    {
        return new Application(
            $this->app->getInstance(), 
            $this->getAdapter()->getApplication()
        );
    }
}