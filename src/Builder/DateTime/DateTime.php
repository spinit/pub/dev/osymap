<?php
namespace Spinit\Dev\Opensymap\Builder\DateTime;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Util;

use Spinit\Dev\Opensymap\Helper\Calendar;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

/**
 * Description of SearchInline
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DateTime extends Builder
{
    public function registerEvent()
    {
        $this->bindExec("@init", function ($event, $field) {
            if ($field->get("default") == "today") $field->set("default", date("Y-m-d"));
        });
        $this->bindExec('load', [$this, 'onLoadData']);
        $this->bindExec('check', [$this, 'onCheckData']);
    }
    public function isNull(ItemInterface $field)
    {
        return !$field->getValue('id');
        return trim((string) Util\arrayGet($field->getValue(), 'id')) == '';
    }
    public function build(ItemInterface $field, $withTitle = true)
    {
        $this->addResource('DateTime.js');
        $this->addResource('DateTime.css');
        
        // contenitore principale
        $div = $this->makeComponent($field, 'osy-datetime', $withTitle);
        $cmp = $div->add(Util\Tag::create('div'))->att('class','osy-search-cmp');
        $cmp->add(Util\Tag::create('div'))->att('class','osy-search-btn')->add('...');
        $value = $field->getValue("id");
        if(!$value and $df = $field->get('default')) {
            $field->setValue(['id'=>Calendar::dtCheck($df), 'name'=>Calendar::dtCheck($df, '-', '/')], 1);
        } else {
            $field->setValue($value, 1);
        }
        $value = $field->getValue();
        $cmp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[id]')
            ->att('value', Util\arrayGet($value, 'id'))
            ->att('type', 'hidden')
            ->att('class','osy-search-id');
        $cmp->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[name]')
            ->att('value', Util\arrayGet($value, 'name'))
            ->att('osy-id',$field->getName())
            ->att('class','osy-search-name form-control');
        $div->add(Util\Tag::create('div'))->att('class','osy-content')
            ->add('');
        return $div;
    }
    public function onLoadData($event, $field)
    {
        $month = $event->getParam(0);
        if (is_array($month)) $month = array_shift($month);
        $form = $field->getView();
        $response = $field->getForm()->getResponse();
        $div = Util\Tag::create('div')->att('class', 'osy-box');
        $response->addContent($field->getName(), ['.osy-content'=>$div]);
        $min = $max = '';
        if ($field->get('min')) {
            $min = $form->getItem($field->get('min'))->getValue();
            if (is_array($min)) $min = Util\arrayGet($min, 'id');
        }
        if ($field->get('max')) {
            $max = $form->getItem($field->get('max'))->getValue();
            if (is_array($max)) $max = Util\arrayGet($max, 'id');
        }
        $cal = new Calendar(Util\arrayGet($field->getValue(), 'id'), $month, $min, $max);
        $cal->setNotEmpty($field->get('empty') == 'no');
        if ($field->get('type') == 'datetime') {
            $cal->withHour();
        }
        $div->add($cal->getTag($field->getName()));
    }
    
    public function onCheckData($event, $field)
    {
        $month = $event->getParam(0);
        //debug([$field->getName(), $field->getValue()]);
        $form = $field->getForm();
        $response = $form->getResponse();
        $data = $field->getValue();
        $dt = null;
        try {
            $data['val'] = Calendar::dtCheck($data['name']);
            if (strlen($data['val']) == 0) {
                $data['id'] = '';
                $data['err'] = [];
            } else {
                $dt = new \DateTime($data['val']);
                $data['id'] = $dt->format('Y-m-d h:i:s');
                $data['err'] = \DateTime::getLastErrors();
            }
            $response->addExec('setValue', 'this', $data);
        } catch (\Exception $e) {
            $data['id'] = '';
            $response->addExec('setValue', 'this', $data);
        }
    }
    
    private function makeData($field, $div, $lista)
    {
        $idx = 0;
        $limit = Util\nvl($field->get('limit'), 20);
        foreach($lista as $data) {
            if ($idx>$limit) {
                $div->add(Util\Tag::create('div'))
                        ->att('class','osy-search-stop idx'.$idx)
                        ->add('... more ...');
                break;
            }
            $kid = $knme = $klbl = array_shift($data);
            if (count($data)) {
                $knme = $klbl = array_shift($data);
            }
            if (count($data)) {
                $klbl = array_shift($data);
            }
            $item = $div->add(Util\Tag::create('div'))
                    ->att('class','osy-search-item idx'.$idx)
                    ->att('data-id', $kid)
                    ->att('data-name', $knme);
            $item->add($klbl);
            $idx += 1;
        }
    }
        
    static public function format($value, $type='-', $mode = '') {
        $cur = explode(' ', $value);
        $part = explode( $type == '-' ? '/' : '-', $cur[0]);
        if (count($part)>1) {
            $part = array_reverse($part);
            $cur[0] = implode($type != '-' ? '/' : '-', $part);
        }
        $part = explode( $type, $cur[0]);
        if (count($part)>2) {
            $part[1] = str_pad($part[1], 2, '0', STR_PAD_LEFT);
            if ($type == '/') {
                $part[0] = str_pad($part[0], 2, '0', STR_PAD_LEFT);
            } else {
                $part[2] = str_pad($part[2], 2, '0', STR_PAD_LEFT);
            }
        }
        $cur[0] = implode($type, $part);
        if ($mode == 'date' or count($cur)<2) {
            return $cur[0];
        }
        // formatta l'orario senza i secondi
        
        $part = explode(':', $cur[1]);
        $cur[1] = $part[0].':'.$part[1];
        return implode(' ', $cur);
    }
    
    public function checkValue(ItemInterface $field, $value) {
        
        if (!is_array($value)) {
            $value = ['id'=>self::format($value, '-'), 'name'=>self::format($value, '/')];
        }
        //$valueID = explode(' ', (string) Util\arrayGet($value, 'id', ''));
        //array_shift($valueID); // la prima parte viene calcolata dal nome
        if (strlen($name = trim((string) Util\arrayGet($value, 'name')))) {
            try {
                if (trim($name, '0') == '') {
                    throw new \Exception();
                }
                $ck = new \DateTime(self::format($name, '-', 'date'));
                
            } catch(\Exception $e) {
                $ck = '';
            }
            if (!$ck) {
                $value['id'] = '';
            } else {
                $value['id'] = self::format($value['id']?:$name, '-');
                $value['name'] = self::format($name, '/', $field->get('type'));
            }
        } else {
            $value['id'] = self::format($value['id'], '-');
            $value['name'] = self::format($value['id'], '/', $field->get('type'));
        }
        //if ($value['id'] and count($valueID)) $value['id'].= ' '.array_shift($valueID);
        return $value;
    }
    
    public function onSetDate($event, $field) {
        $param = json_decode($event->getParam(0), 1) ?: [];
        $field->setValue(['id'=>trim(array_shift($param)), 'name'=>trim(array_shift($param))]);
        $field->getForm()->getResponse()->addContent($field->getName(), $field->build());
        $field->getForm()->getResponse()->addCommand('$("#'.$field->getName().'").trigger("click");');
    }

    protected function storeInModelFieldValue($model, $field, $value, $item)
    {
        $fieldList = asArray($field, ',');
        $firstField = array_shift($fieldList);
        $model->set($firstField, arrayGet($value, 'id'));
        $secondField = array_shift($fieldList);
        $secondField and $model->set($secondField, arrayGet($value, 'name'));
    }
    /*
    protected function modelLoad($field, $model) {
        if (!$model) {
            return;
        }
        $names = Util\asArray($field->get('field'), ',');
        $prop = $field->get('property');
        switch(count($names)) {
            case 0 :
                break;
            case 1 :
                if ($prop) {
                    $field->setValue($model->getPropertyValue($prop, array_shift($names)));
                } else {
                    $field->setValue($model->get(array_shift($names)));
                }
                break;
            default :
                if ($prop) {
                    $field->setValue([
                        'id'=>$model->getPropertyValue($prop, array_shift($names)), 
                        'name'=>$model->getPropertyValue($prop, array_shift($names))
                    ]);
                } else {
                    $field->setValue([
                        'id'=>$model->get(array_shift($names)), 
                        'name'=>$model->get(array_shift($names))
                    ]);
                }
                break;
        }
    }
    
    protected function modelStore($field, $model) {
        if (!$model) {
            return;
        }
        $names = Util\asArray($field->get('field'), ',');
        $prop = $field->get('property');
        $types = Util\asArray($field->get('propertyType'), ',');
        switch(count($names)) {
            case 0 :
                break;
            case 1 :
                if ($prop) {
                    $model->setPropertyValue($prop, $field->getValue(), array_shift($names), array_shift($types));
                } else {
                    $model->set(array_shift($names), $field->getValue('id'));
                }
                break;
            default :
                if ($prop) {
                    $model->setPropertyValue($prop, $field->getValue('id'), array_shift($names), array_shift($types));
                    $model->setPropertyValue($prop, $field->getValue('name'), array_shift($names), array_shift($types));
                } else {
                    $model->set(array_shift($names), $field->getValue('id'));
                    $model->set(array_shift($names), $field->getValue('name'));
                }
                break;
        }
    }
    */
}
