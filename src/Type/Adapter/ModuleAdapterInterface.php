<?php
namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Lib\Model\Adapter\ModelAdapter;

interface ModuleAdapterInterface extends AdapterInterface {
    function getForm($name) : FormAdapterInterface;
    function getFormList(): iterable;
    function getModel($name) : ModelAdapter;
    function getModelList(): iterable;
    function getBuilder($name);
    function getApplication(): ApplicationAdapterInterface;
    function getName(): string;
    function getExtendList() : iterable;
    function getMenuBars($role);
    function getInitRunner($when) : iterable;
}