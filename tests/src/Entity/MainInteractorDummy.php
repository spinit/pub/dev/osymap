<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\AppRouter\Core\AddRouteInterface;
use Spinit\Dev\AppRouter\Core\MainInteractorInterface;

class MainInteractorDummy implements MainInteractorInterface {
    public function install(iterable $instanceList) {}
    public function initRoute(AddRouteInterface $main) {}
    public function publish(string $topic, array $param, $realm = '') {}
    public function getConsole() {}
    public function getHome() {}
    public function decrypt($token) {}
    public function encrypt($data) {}

}