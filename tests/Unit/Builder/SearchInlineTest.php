<?php
namespace Spinit\Test\Builder\DatagridTest;

use PHPUnit\Framework\TestCase;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class SearchInlineTest extends TestCase {

    public function testView(){
        $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $osy->setDataSource('', new DataSource('test'));
        $req = $osy->getFactory()->makeRequest('POST', 'app/app@test/SearchInline');
        $res = $osy->run($req);
        $this->assertStringContainsString('Search', $res->get('content'));

    }
}