(function($){
    $(document).off('.osy-editor-mce');

    $('main#main').on('closeForm.osy-editor-mce', function() {
        tinymce.remove();
    });

    let parDefault = {
        skin: "oxide",
        // icons: 'material' ,
        preview_styles: 'font-size color',
        resize: 'both',
        plugins: 'link osy-editor-image media code autolink lists media table osy-editor-box',
        toolbar: 'undo redo | styleselect| forecolor  | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link osy-editor-image media| code table | osy-editor-box',
        toolbar_mode: 'floating',
        /* enable title field in the Image dialog*/
        //image_title: true,
        /* Disabilita il tag p */
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        /* enable automatic uploads of images represented by blob or data URIs*/
        //automatic_uploads: false,
        //images_upload_url: 'postAcceptor.php',
        //file_picker_types: 'image',
        contextmenu : "osy-editor-box table",
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        menubar : false,
        valid_children : '+body[style]',
        //file_picker_types: 'image',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        content_css : 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'
    }

    $(document).on('init.osy-editor-mce', function() {
        
        $('.osy-editor.osy-tinymce:not(.inited)').each(function (editor) {

            item.editorBody = $(editor).find('.body').first();

            let param = { ...parDefault,
                height: parseInt(editor.dataset.height) || 550,
                selector: '#'+editor.id+' .body',
                file_picker_callback: function(cb, value, meta) {
                    let f = Desktop.openForm($(editor).data('resource-url'));
                    $('iframe', f)[0].trigger = function(evName, res) {
                        cb(res.url, res);
                        f.trigger('close');
                    };
                },
                document_base_url : editor.getAttribute('data-base-url'),
                content_css : parDefault.content_css+','+editor.getAttribute('data-content-css')
            }
            //console.log(param);
            tinymce.init(param);
            $(editor).addClass('inited');
        });
    });

    $(document).on('beforeSerializeArray.osy-editor-mce', function(event, form, post) {
        $('.osy-editor.inited').each(function(item) {
          let content = tinymce.get(item.editorBody.id).getContent();
          item.editorBody.value = content;
        });
    });
        /*
       $(document).on('setValue', '.osy-editor', function (event, value) {
         let tt = this.querySelector('textarea');
         tinymce.get(tt.id).setContent(value);
       });
             // memorizzazione dati per salvataggio
       $(document).on('serializeArray', function(event, form, post) {
         $('.osy-editor').each(function(idx, div) {
             let tt = div.querySelector('textarea');
             post.push({name : this.id, value : tinymce.get(tt.id).getContent()});
         });
       });
   */
})(desktop.$);
   