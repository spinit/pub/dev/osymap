<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\CommonTrait;
use Spinit\Dev\Opensymap\Core\Xml\FileTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasBuilderTrait;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ModuleAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\OverlayAdapterInterface;
use Spinit\Dev\Opensymap\Type\OverlayInterface;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\biName;

class ApplicationAdapter implements ApplicationAdapterInterface {

    use FileTrait;
    use AttributeTrait;
    use HasBuilderTrait;

    public function __construct(array $param, string $use = '', $conf = [])
    {
        $file = array_shift($param);
        if (!is_file($file)) {
            $file = fspath($file, 'index.xml');
        }
        $this->init($file);
    }
    public function getName() {
        return (string) $this->get('name');
    }
    public function getForm($name) : FormAdapterInterface {
        $biname = biName($name);
        return $this->getModule($biname[0])->getForm($biname[1]);
    }
    public function getOverlay($name) : OverlayAdapterInterface
    {
        return new OverlayAdapter($this, $name);
    }
    public function getOverlayList() : iterable
    {
        return $this->listResourceIn('Overlay');
    }
    public function getModuleList() : iterable
    {
        return $this->listResourceIn('Module');
    }
    public function getModule($name): ModuleAdapterInterface
    {
        return new ModuleAdapter($this, $name);
    }
    public function getExternDir($extern) {
        if ($this->index->extern)
        foreach($this->index->extern->item as $ext) {
            if ((string) $ext == $extern) {
                return (string) $ext["name"];
            }
        }
        throw new MessageError("Modulo esterno non trovato : ".$extern);
    }
    public function getExternModule($app, $name)  : ModuleAdapterInterface
    {
        return new ModuleAdapter($this, $name, $app);
    }
    public function getRoleList() {
        $list = [];
        $box = $this->index->roles;
        if ($box) foreach($box->children()?:[] as $item) {
            $list[(string) $item->getName()] = trim($box['base'].':'.(string)$item, ':');
        }
        return $list;
    }
    public function getItemList()
    {
        $list = UtilCheck::getItemList($this->index->items->item);
        //if ($this->get('debug')) debug($list);
        return $list;
    }
}