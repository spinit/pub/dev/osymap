#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git zip -yqq 

# Install Xdebug
#pecl install xdebug
#docker-php-ext-enable xdebug

# Install composer
curl -sS https://getcomposer.org/installer | php

# Install all project dependencies
php composer.phar install
