<?php

namespace Spinit\Dev\Opensymap\Core;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

trait HasAdapterTrait {

    /**
     * @var AdapterInterface
     */
    private $adapter;

    protected function setAdapter(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }
    public function getAdapter() : AdapterInterface {
        return $this->adapter;
    }
}