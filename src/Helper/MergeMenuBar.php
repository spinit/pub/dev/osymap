<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

/**
 * Description of MergeMenuBar
 *
 * @author ermanno
 */
class MergeMenuBar {
    private $register = [];
    
    public function merge($mbars) {
        foreach($mbars as $name => $conf) {
            unset($item);
            $item = $conf;
            unset($item['menu']);
            
            if (!array_key_exists($name, $this->register)) {
                // creazione menubar
                $this->register[$name] = ['' => null];
                $this->register[$name][''] = &$item;
            } else {
                $this->register[$name]['']['label'] = $item['label']?:$this->register[$name]['']['label'];
            }
            $this->mergeMenu($conf, $name, '');
        }
    }
    
    private function mergeMenu($menu, $reg, $base) {
        foreach($menu['menu'] as $name=>$sub) {
            unset($item);
            $item = $sub;
            unset($item['menu']);
            if ($par = Util\arrayGet($item, 'parent')) {
                $base = '.'.$par;
                //debug($base, $this->register[$reg]);
                // viene creata una entry fittizzia nell'attesa che venga inizializzata
                if (!array_key_exists($base, $this->register[$reg])) {
                    $dummy = [];
                    $this->register[$reg][$base] = &$dummy;
                }
            }
            if (is_integer($name)) {
                $name = rand(1000, 9999);
            }
            $key = "{$base}.{$name}";
            if (array_key_exists($key, $this->register[$reg])) {
                // entry già presente forse perché inserito fittiziamente ... inizializzazione
                $this->register[$reg][$key] = array_merge($this->register[$reg][$key], $item);
            } else {
                $this->register[$reg][$key] =&$item;
            }
            unset ($this->register[$reg][$base]['menu'][$name]);
            $this->register[$reg][$base]['menu'][$name] = &$this->register[$reg][$key];
            $this->mergeMenu($sub, $reg, $key);
        }
    } 
    
    public function getList() {
        $ret = [];
        foreach(array_keys($this->register) as $name) {
            if ($menu = $this->getValidMenu($this->register[$name][''])) {
                $ret[$name] = $menu;
            }
            /*
            if (count((array) Util\arrayGet($this->register[$name][''], 'menu', []))) {
                $ret[$name] = $this->register[$name][''];
            }
            */
        }
        return $ret;
    }

    private function getValidMenu($bar) {

        if (arrayGet($bar, 'form')) return $bar;
        $list = [];
        foreach((array) arrayGet($bar, 'menu', []) as $name => $item) {
            if ($this->getValidMenu($item)) $list[$name] = $item;
        }
        if (count($list)) {
            $bar['menu'] = $list;
            return $bar;
        }
        return false;
    }
}

