<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Type\FormInterface;

class EventHandlerForm {
    
    private $form;

    public function __construct(FormInterface $form )
    {
        $this->form = $form;
    }

    public function getForm() : FormInterface {
        return $this->form;
    }
}