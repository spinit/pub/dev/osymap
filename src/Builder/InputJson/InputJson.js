(($)=>{
    // clear event namespace
    $(document).off('.osy-input-json');
    $(document).on('setValue.osy-input-json', '[osy-item].osy-input-json', function(event, param) {
        this.value = param;
    });
})(desktop.$);