<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\HasModuleTrait;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ModalAdapterInterface;

use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;

class FormAdapter extends ViewAdapter implements FormAdapterInterface {

    use HasModuleTrait;

    private $name;

    public function __construct(ModuleAdapter $mod,  string $name)
    {
        $this->setModule($mod);
        $this->name = $name;
        $this->init(fspath($mod->getRoot(), 'Form', $name, 'index.xml'));
    }
    public function getName($full = true) : string{
        $name = [];
        if ($full) $name[] = $this->getModule()->getName();
        $name[] = $this->name;
        return (string) implode(':', $name);
    }
    public function getModal($name): ModalAdapterInterface
    {
        return new ModalAdapter($this, $name);
    }
}