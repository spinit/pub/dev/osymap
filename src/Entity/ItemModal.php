<?php 
namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Type\ItemInterface;

use function Spinit\Dev\AppRouter\debug;

class ItemModal extends Item {
    public function getName() : string {
        if (!parent::getName()) return '';
        $name = '_[modal]['.parent::getName().']';
        return $name;
    }
    public function setValueByRequest(Request $req)
    {
        if ($name = $this->get('name') AND $req->hasApp('modal,'.$name)) {
            $this->setValue($req->getApp('modal,'.$name));
        }
    }
}