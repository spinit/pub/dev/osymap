(function($){
  $(document).off('.osy-editor-ck');

  $(document).on('beforeSerializeArray.osy-editor-ck', function(event, form, post) {
    $('.osy-editor.inited').each(function(item) {
      let editor = $(item).data('editor');
      let content = editor.getData();
      item.editorBody.value = content;
    });
  });

  $(document).on('init.osy-editor-ck', function() {
    $('.osy-editor.osy-ckeditor:not(.inited)').each(function(item) {
      item.editorBody = $(item).find('.body').first();

      ClassicEditor
      .create( item.editorBody )
      .then(editor => {
        $(item).data('editor', editor);
        $('.ck-editor__main .ck-content', item).css('height', $(item).attr('osy-height'));
      })
      .catch( error => {
          console.error( error );
      });
      $(item).addClass('inited');
    });
  });

})(desktop.$);
