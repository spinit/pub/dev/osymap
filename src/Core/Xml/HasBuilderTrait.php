<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use function Spinit\Util\arrayGet;

trait HasBuilderTrait {
    private $builders = null;

    public function getBuilder($name) : string {
        if ($this->builders === null) {
            $this->builders = [];
            foreach($this->index->builders?:[] as $root) {
                $base = (string) $root['base'];
                foreach($root->builder?:[] as $builder) {
                    if ($base) {
                        $classPath = $base .':'.(string) $builder;
                    } else {
                        $classPath = (string) $builder;
                    }
                    $this->builders[(string) $builder['name']] = $classPath;
                }
            }
        }
        return (string) arrayGet($this->builders, $name);
    }
    public function getBuilderList() {
        if ($this->builders === null) {
            $this->getBuilder('');
        }
        return $this->builders;
    }
}
