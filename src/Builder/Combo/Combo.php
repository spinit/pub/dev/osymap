<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\Combo;

use Spinit\Dev\Opensymap\Builder\SearchInline\SearchManager;
use Spinit\Dev\Opensymap\Command\ResponseAddCommand;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Entity\ItemModal;
use Spinit\Dev\Opensymap\Helper\MakerData;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of Combo
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Combo extends Builder
{
    public function registerEvent() {
        $this->bindAfter('select', [$this, 'onSelectData']);
        $this->bindExec('@backValue', function($event, $field) {
            $field->setValue($event->getParam(0));
        });

        $this->bindExec('@check', function($event, $field) {
            // si forza la chiamata di checkValue
            $field->setValue($field->getForm()->getRequest()->getPost($field->getName()));
        });
        $runner = SearchManager::getInstance();
        $this->bindExec('add', [$runner, 'onOpenFormDetail']);
        $this->bindExec('open', [$runner, 'onOpenSelfDetail']);
    }
    public function checkValue(ItemInterface $field, $value)
    {
        if ($field->get('empty') == 'no' and !$value) {
            //$value = $field->getForm()->getRequest()->getPost($field->getName());
            //if ($value) return $value;

            $data = $this->getData($field);
            $rec = $data->current();
            if (!$rec) return '';
            $kk = array_keys($rec);
            $value = Util\asArray($rec[$kk[0]]);
            $value = array_shift($value);
        }
        if (!$value) $value = "";
        return $value;
    }

    public function storeInModel(ItemInterface $item, ModelInterface $model)
    {
        if (!$item->get('field')) return;
        $value = $item->getValue();
        if (is_array($value)) $value = array_shift($value).'';
        $model->set($item->get('field'), $value);
    }
    public function build(ItemInterface $field, $withLabel = true)
    {
        $this->addResource('Combo.js');
        $this->addResource('Combo.css');
        // contenitore principale
        $div = $this->makeComponent($field, 'osy-combo', $withLabel);
        $header = $div->child(0);
        if ($field->get('formDetail')) {
            // non è esclusa l'aggiunta di elementi
            if ($field->get('add')!='no') {
                $header->add(Util\Tag::create('button'))
                    ->att('type', 'button')
                    ->att('class', 'btn btn-outline-info btn-sm osy-btn')
                    ->att('osy-event', 'add')
                    ->add('+');
            }/*
            $cmp->att('class', 'hasDetail', ' ');
            $viewItem = $cmp->add(Util\Tag::create('div'))
                ->att('class', 'osy-search-view')->att('osy-event', 'open');
            $viewItem->add('<i class="fa fa-fw fa-pencil"></i>');
            */
            
        } else if ($field->get('add')=='1') {
            // formDetail non è impostata ma viene forzata la visualizzazione del pulsante di aggiunta
            $header->add(Util\Tag::create('button'))
                ->att('type', 'button')
                ->att('class', 'btn btn-outline-info btn-sm osy-btn')
                ->att('osy-event', 'add')
                ->add('+');
        }
        // Caricamento dati da visualizzare
        $bdy = $div->add(Util\Tag::create('div'))->att('class', 'osy-data');
        // generazione dati .. se non sono stati già caricati in fase si inizializzazione del componente
        if ($data = $field->getParam('data')) {
            $bdy->add($field->getParam('comment'));
        } else {
            $data = $this->getData($field, $bdy);
        }
        $this->makeData($field, $data, $bdy); //, $this->getDataMapper($field, $bdy));
        return $div;
    }
    
    public function getData($field) {
        $args = func_get_args();
        array_shift($args);
        return (new MakerData())->build($field, '', array_shift($args));
    }
    
    private function makeData($field, $data, $bdy)//, $mapper)
    {
        $input = $bdy->add(Util\Tag::create('input'))
        ->att('type', 'hidden')
        ->att('class', 'osy-combo-id')
        ->att('name', $field->getName())
        ->att('value', $field->getValue());

        $select = $bdy->add(Util\Tag::create('select'))->att('class', 'form-control w-origin osy-combo-select');

        if ($field->get('empty')!='no') {
            $opt = $select->add(Util\Tag::create('option'))->att('value', '');
            $opt->add($field->get('emptyString','-- select --'));
        }
        foreach($data?:[] as $item) {
            $key = $lbl = array_shift($item);
            if (count($item)) {
                $lbl = array_shift($item);
            }
            if (!$key and $key !== '0') {
                $key = '';
            }
            $opt = $select->add(Util\Tag::create('option'))->att('value', $key);
            $opt->add($lbl);
            if ($field->getValue() == $key) {
                $opt->prp('selected');
            }
        }
    }
    
    public function onSelectData($event, $field)
    {
        $response = $field->getForm()->getResponse();
        $field->setValue($event->getParam(0), true);
        if ($field instanceof ItemModal) {
            $response->addCommand("$(this).trigger('setValue', args[0])", $event->getParam(0));
        }
        switch ($field->get('onchange')) {
            case 'submit':
            case 'refresh':
                $response->addCommand('console.log(this)');
                $response->addExec($field->get('onchange'));
                break;
        }
    }
}