<?php

use function Spinit\Dev\AppRouter\debug;

$pwd = $this->getItem('txt_pwd');
$act = $this->getItem('chk_act');

if (!$this->getPkey()) {
    $pwdValue = trim($pwd->getValue());
    if ($pwdValue) {
        $pwdValue = md5($pwdValue);
    }
    // viene impostato il modello di lavoro
    $this->getModel()->set('aut_pwd', $pwdValue);
} else {
    // viene creato un modello separato per effettuare alcuni controlli sui dati già impostati
    $model = $this->getModel(true);
    $model->load($this->getPkey());
    if ($act->getValue() and !$model->get('aut_pwd')) {
        throw new \Exception("Un utente, per essere attivo, deve avere la password impostata");
    }
    
}

