<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace  Spinit\Dev\Opensymap\Builder\Editor;

use Spinit\Dev\Opensymap\Entity\Builder as EntityBuilder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Util\Tag;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Editor extends EntityBuilder
{
    public function registerEvent()
    {
        $this->bindExec('col-cmd', [$this, 'onColCmd']);
    }
    
    public function build(ItemInterface $item, $withTitle = true)
    {
        $div = $this->makeComponent($item, 'osy-editor', $withTitle);
        $div->att('osy-height', $item->get('height'));
        switch($item->get('lib')) {
            case 'tinymce':
                $this->makeTinyEditor($item, $div);
                break;
            default:
                $this->makeCkEditor($item, $div);
                break;
        }
        return $div;
    }

    private function makeCkEditor($item, $div) {
        $div->att('class', 'osy-ckeditor', ' ');
        $this->addResource('https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js');
        $this->addResource('ckeditor5/Editor.css');
        $this->addResource('ckeditor5/Editor.js');
        $body = $div->add(Tag::create('textarea'))->att('class', 'body')->att('name', $item->getName());
        $body->add(htmlspecialchars($item->getValue(), ENT_QUOTES));
    }

    private function makeTinyEditor($item, $div) {
        $div->att('class', 'osy-tinymce', ' ');
        $this->addResource('https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.5.1/tinymce.min.js');
        $this->addResource('https://code.jquery.com/jquery-2.2.4.min.js');
        $this->addResource('tinymce5/plugin/osy-editor-box/plugin.js');
        $this->addResource('tinymce5/plugin/osy-editor-image/plugin.js');
        $this->addResource('tinymce5/plugin/osy-editor-image/style.css');
        $this->addResource('tinymce5/Editor.css');
        $this->addResource('tinymce5/Editor.js');
        $body = $div->add(Tag::create('textarea'))->att('class', 'body')->att('name', $item->getName());
        $body->add(htmlspecialchars($item->getValue(), ENT_QUOTES));
    }
}
