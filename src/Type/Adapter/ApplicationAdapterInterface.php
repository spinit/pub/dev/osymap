<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

interface ApplicationAdapterInterface extends AdapterInterface {
    function getForm($name);
    function getBuilder($name);
    function getBuilderList();
    function getModuleList();
    function getModule($name) : ModuleAdapterInterface;
    function getExternModule($app, $name) : ModuleAdapterInterface;
    function getOverlay($name);
    function getOverlayList();
    function getItemList();
    function getRoleList();
    function getName();
}