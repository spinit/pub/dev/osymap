require('./desktop.js');
const desktop = window.desktop;

describe('test dekstop', function() {
    test('window.desktop è presente', function() {
        expect(window.desktop).toBeDefined();
    });
    test('desktop è presente', function() {
        expect(desktop).toBeDefined();
        expect(desktop.alert).toBeDefined();
    });
});

describe('test myQuery', function() {
    let $ = desktop.$$;
    let d = $('<div><span>ok</span></div>');

    test('check create HtmlElement', () => {
        expect(d.filter('span').html()).toBe('ok');
    });

    test('check events', () => {
        let tt = {
            test : ''
        }
        d.on('hello', function() {
            tt.test += 'ok';
        });
        d.trigger('hello');
        expect(tt.test).toBe('ok');

        d.on('hello.world', () => {
            tt.test += '+';
        })
        d.trigger('hello');
        expect(tt.test).toBe('okok+');
        d.trigger('hello.world');
        expect(tt.test).toBe('okok++');

        d.off('hello.world');
        d.trigger('hello.world');
        expect(tt.test).toBe('okok++');

        d.trigger('hello');
        expect(tt.test).toBe('okok++ok');

        d.off('hello');
        d.trigger('hello');
        expect(tt.test).toBe('okok++ok');

        d.on('hello.world', (event, detail) => {
            tt.test += '+'+detail;
        })
        d.trigger('hello', 'test');
        expect(tt.test).toBe('okok++ok+test');
    });

});