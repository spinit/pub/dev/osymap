<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;
use Spinit\Util;

/**
 * Description of DisplayComact
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DisplayCompact extends Display
{
    public function make()
    {
        $data = new DataList($this->field);
        $table = $this->root->add(new Util\Table());
        foreach($data->getFilter() as $item) {
            $cmp = $item['fnc']($item, $this->field->getName().'[item]', $this->field->getValue('item'));
            // se il componente è stato generato ... allora viene inserita la voce
            if ($cmp) {
                $table->Row();
                $table->Head($item['nme']);
                $table->Row();
                $table->Cell($cmp);
            }
        }
    }

}
