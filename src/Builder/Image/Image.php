<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Core\Builder\Image;

use Spinit\Dev\Opensymap\Core\Builder;
use Spinit\Dev\Opensymap\Core\Type\FieldInterface;
use Spinit\Util\Tag;
use Spinit\Util;
/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Image extends Builder
{
    protected function registerEvent()
    {
        $this->bindExec('open', [$this, 'onOpen']);
    }
    public function build(FieldInterface $field, $withTitle = true)
    {
        $this->addResource('Image.css');
        $this->addResource('Image.js');
        $resname = $field->getValue();
        $div = $this->makeComponent($field, 'osy-image', $withTitle);
        $div->child(0)->add('<span><i style="margin: 0px  0px 10px 10px" event="open" class="cmd-open far fa-folder-open"></i></span>');
        $lbl = $div->add(Tag::create('div'))->att('class', 'label');
        $cnt = $div->add(new Tag('div'))
            ->att('class', 'osy-content')
            ->att('style', 'height:'.$field->get('height'), ';')
            ->att('data-src', $resname);
        if ($resname) {
            if (!is_array($resname)) {
                $arRes = json_decode($resname, 1);
                if (is_array($arRes)) {
                    $resname = $arRes;
                } else {
                    $resname = ['file'=>$resname, 'name'=>''];
                }
            }
            $lbl->add($resname['name']);
            $cnt->add($this->getIframeViewer($field, $resname['file'].'/preview/'));
        }
        return $div;
    }

    private function getIframeViewer($field, $resname) {
        if ($formResource = $field->get('formDetail')) {
            $app = $field->getForm()->getApplication();
            $form = $app->getForm($formResource);
            $form->trigger('@makeFrom', [$field->getForm()]);
        } else {
            $app = $field->getForm()->getApplication()->getInstance()->getApplicationFromID('system@opensymap.org');
            $form = $app->getForm('Core:ResourceManager');
        }
        return '<iframe src="'.$form->getUrlPath(1, $resname ).'"></iframe>';
    }
    protected function onOpen($event, $target) {
        $this->getResponse()->addCommand('Desktop.modal(args[0], args[1])', 
            ['title'=>'Resource Manager', 'body'=>$this->getIframeViewer($target, ''), 'noBtnClose'=>'1'],
            "let el = document.getElementById('".$target->getName()."');
             let mdl = this[0] ? this[0] : this;
             let frm = mdl.querySelector('iframe'); 
             frm.trigger = (e, p) => Desktop.trigger(el, e, p, mdl);
             ");
        //debug($form->getUrlPath());
    }
}
