<?php

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\getMimeType;
use function Spinit\Util\arrayGet;

$w = arrayGet($_GET, 'w', 100);

if (!$w) $w = '30';
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}
$h = 100;
$im = imagecreate( $w*7, $h);
$dest = imagecreate( $w*7, 1);

$def_gray = 240;
$brd = 210;
$def_color['dom'] = array(210, 230, 240);
$def_color['sab'] = array(210, 230, 240);

$background = array();
foreach(array('dom','lun','mar','mer','gio','ven','sab') as $day) {
    if (isset($_GET[$day]))
    {
        $col = hex2rgb($_GET[$day]);
    } else if (isset($def_color[$day])) {
        $col = $def_color[$day];
    } else {
        $col = array($def_gray, $def_gray, $def_gray);
    }
     $background[] = imagecolorallocate($im,  $col[0], $col[1], $col[2] );
}

if (isset($_GET['brd'])) {
    $brd_ = hex2rgb($_GET['brd']);
    $brd = intval(($brd_[0]+$brd_[1]+$brd_[2])/3);
}

$linecolor    = imagecolorallocate($im,  $brd, $brd, $brd );

$week = 0; //date('w');

for($k=0 ; $k<count($background) ; $k++) {
    $b = $background[($k+$week)%count($background)];
    imagefilledrectangle($im, $k*$w, 0, ($k+1)*$w, $h, $b);
    imageline($im, ($k)*$w, 0, ($k)*$w, $h, $linecolor);
}

imagecopy($dest, $im, 0, 0, 0, 0, 7 * $w, $h);

imagepng($dest);

foreach ($background as $k=>$b)
{
    imagecolordeallocate( $im, $b );
}
imagecolordeallocate( $im, $linecolor );

imagedestroy($im);
imagedestroy($dest);

$this->getInstance()->getResponse()->setContent(function() use ($im) {
}, getMimeType("png"));
