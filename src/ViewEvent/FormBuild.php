<?php

namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Command\ResponseAddCommand;
use Spinit\Dev\Opensymap\Helper\FormUtil;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\normalize;

class FormBuild extends EventHandlerForm {

    /**
     * @var FormInterface
     */
    private $content;

    public function build() {
        $cmd = new ResponseAddCommand($this->getForm()->getResponse());

        $model = $this->getForm()->getModel();
        $data = $model?$model->asArray():[];
        $title = normalize($this->getForm()->get('title'), $data)[0];
        
        $this->makeContent($title, $model);
        $this->makeHistoryResponse($cmd, $title);

        $cmd->exec('$("main#main").trigger("form-umount")');
        $cmd->getResponse()->set('content', '<input name="_[osy][init]" value="1" type="hidden"/>'.$this->content.'');
        $cmd->exec('desktop.setContent("main#main", response.content)');
        $cmd->exec('$("body").attr("id", args[0])','form-'.md5($this->getForm()->getUri()));
        $extraClass = $this->hasCommand()?"hasCommand":"";
        $cmd->exec('$("main#main").trigger("form-mount",{"class":"'.$extraClass.'"})');

    }

    private function makeContent($title, $model) {
        $deletedTitle = '';
        $contentClass = '';
        if ($model and $model->get('dat_del__')) {
            $deletedTitle = '<div class="deleted"><code>[ DELETED ]</code></div>';
            $contentClass = 'deleted';
        }
        $this->content = '<div class="content '.$contentClass.'">';
        $this->content .= '<div class="title"><h2>'.$title.'</h2>'.$deletedTitle.'</div>';
        $this->content .= '<div>';
        foreach($this->getForm()->getItemList() as $item) {
            if ($item->isVisible()) $this->content .= $item->build(); 
        }
        $this->content .= '</div></div>';
        if ($this->hasCommand()) {
            $this->content .= $this->makeCommand($model);
        }
    }

    private function makeHistoryResponse($cmd, $title) {
        $form = $this->getForm();
        $request = $form->getRequest();
        // se la richiesta non è stata generata da un back
        $typeCall = $request->getHeaders('X-Opensymap-Call');
        // viene fatto solo se non sono in una chiamata di ritorno ... oppure se lo forzo
        if (!in_array($typeCall, ['popstate', 'reload'])) {
            $url = (new FormUtil($form))->getUrl();
            $part = explode('?', ltrim($url, '/'));
            if ($form->get('replace')) {
                $func = "window.history.replaceState";
            } else if (!$form->getRequest()->getApp('osy,init') or $form->get('noReplace')) {
                $func = "window.history.pushState";
            } else {
                $func = "window.history.replaceState";
                //$func = "window.history[desktop.eq(location.host+location.pathname, args[0])?'replaceState':'pushState']";
            }
            $cmd->exec($func."({'init':response.init}, args[2], args[1]);  desktop.$(document).trigger('setTitle', args[2]);;// ".__FILE__.':'.__LINE__,  $part[0], $url, $title);
        }
    }

    private function hasCommand() {
        $form = $this->getForm();
        if ($form->getModel()) return true;
        if ($form->get('save-disable')) return true;
        if ($form->get('save-enable')) return true;
        if ($form->get('delete-disable')) return true;
        if ($form->get('delete-enable')) return true;
        return false;
    }
    private function makeCommand($model) {
        $cmd = (new Tag('div'))->att('class', 'command');
        $left = $cmd->add(new Tag('div'))->att('class', 'left');
        $right = $cmd->add(new Tag('div'))->att('class', 'right');

        $form = $this->getForm();
        if ($model and $model->get('dat_del__')) {
            $this->makeCommandRestore($form, $left, $right);
        } else {
            $this->makeCommandStandard($form, $left, $right);
        }
        return $cmd;
    }
    function makeCommandRestore($form, $left, $right) {
        if (!$form->get('restore-disable')) {
            if ($form->get('restore-enable') OR $form->getPkey()) {
                $del = $left->add(new Tag('div'))
                ->att('class', 'btn btn-warning')
                ->att('osy-message', "L'elemento verrà RIPRISTINATO.\nContinuare con l'operazione?")
                ->att('osy-event', 'restore');
                $del->add('Restore');
            }
        }
        // altrimenti si visualizza solo la chiusura della pagina
        $saveClose = $right->add(new Tag('div'))->att('class', 'btn btn-info')
            ->att('onclick', 'history.back()');
        $saveClose->add('Close');
    }
    function makeCommandStandard($form, $left, $right) {
        // se non è richiesto di disabilitare la cancellazione
        if (!$form->get('delete-disable')) {
            if ($form->get('delete-enable') OR $form->getPkey()) {
                $del = $left->add(new Tag('div'))
                ->att('class', 'btn btn-danger')
                ->att('osy-message', "L'elemento verrà eliminato.\nContinuare con l'operazione?")
                ->att('osy-event', 'delete');
                $del->add('Delete');
            }
        }
        
        // se non è richiesta di disabilitare il salvataggio
        if (!$form->get('save-disable')) {
            if ($form->get('save-enable') OR $form->getModel()) {
                if ($form->get('save-enable') != "close") {
                    // se non é richiesta la presenza del solo tasto di chiusura
                    $save = $right->add(new Tag('div'))->att('class', 'btn btn-success')
                    ->att('osy-event', 'save');
                    $save->add('Save');
                }
                
                $saveClose = $right->add(new Tag('div'))->att('class', 'btn btn-info')
                    ->att('osy-event', 'save')
                    ->att('osy-param', 'back');
                $saveClose->add('Save and Close');
            } 
        } else {
            // altrimenti si visualizza solo la chiusura della pagina
            $saveClose = $right->add(new Tag('div'))->att('class', 'btn btn-info')
                ->att('onclick', 'history.back()');
            $saveClose->add('Close');
        }
    }

}