<?php
namespace Spinit\Dev\Opensymap\Builder\DataGrid;

use Spinit\Dev\Opensymap\Builder\DataGrid\MakerStruct;
use Spinit\Dev\Opensymap\Type\ItemInterface;

class MakerBody {

    public function exec(ItemInterface $item, $body, $data) {
        $body->att('style', $item->get('body-style'), ' ');
        $body->att('class', 'osy-body', ' ');
        if ($item->get('maximize')) {
            $body->att('class', 'maximize', ' ');
        }
        if ($item->get('noHeader')) {
            $body->att('class', 'noHeader', ' ');
        }
        // gestione scrolling dati
        $body->att('onscroll', "$(this).trigger('scroll-body')");
        //$this->buildBody($field, $bdy);
        (new MakerStruct)->build($item, $data, $body);
        
    }
}