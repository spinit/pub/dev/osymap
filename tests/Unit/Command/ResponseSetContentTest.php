<?php
namespace Spini\Test\Command\ResponseSetContentTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Command\ResponseSetContent;

class ResponseSetContentTest extends TestCase {

    public function testExec() {
        $response = new Response();
        $cmd = new ResponseSetContent($response);
        $cmd->exec('test', 'Testo da Scrivere');
        $this->assertEquals('test', $response->get('data,exec,0,2'));
        $this->assertEquals('Testo da Scrivere', $response->get('data,exec,0,3'));
    }
}