<?php

use Webmozart\Assert\Assert;

$this->getItem('hdn_typ')->setValue(xid('urn:opensymap.org@type#lang'));
if (!$this->getItem('hdn_app')->getValue()) {
    try {
        Assert::notNull($this->getReferer());
        $app = $this->getReferer()->getApplication();
    } catch (\Exception $e) {
        $app = $this->getApplication();
    }
    $this->getItem('hdn_app')->setValue(xid($app->getName()));
}