<?php

$hid = $this->getField('hdn_id')->getValue();
$lng = $this->getField('txt_lng')->getValue();
$lat = $this->getField('txt_lat')->getValue();
if (strlen($lng) and strlen($lat)) {
    $this->getResponse()->addCommand("$('#map_adr').trigger('setMarker', ['{$hid}', [{$lng}, {$lat}], 1])");
}
