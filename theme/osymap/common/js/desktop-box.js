(($) => {
    posClick = {};

    const setPositionClick = function(event)
    {
        posClick = {'x' : event.pageX, 'y': event.pageY};
    }

    $(document).on('mousedown', event => setPositionClick(event));

    desktop.openBox = function box(cmp, content, option)
    {
        let target;
        let main;
        if (cmp == 'this') {
            target = $(this);
            main = target.closest('[osy-item]');
        } else if (typeof(cmp) == typeof('')) {
            main = target = $('[id="'+cmp+'"]');
        } else {
            target = $(cmp);
            main = target.closest('[osy-item]');
        }
        option = option ? option : {};
        var area = null;
        if (typeof(content) == typeof([])) {
            content = content.join(" ");
        } else if (typeof(content) != typeof('')) {
                for(let cls in content) {
                // viene considerato solo il primo
                area = main.find(cls);
                content = content[cls];
                break;
            }
        }
        // se il box è usato dal componente attuale?
        var box = $('.box-content', main);
        if (box[0]) {
            // si ... allora viene agiornato solo il contenuto
            box.html(content);
            return box;
        }
        // altrimenti occorre determinare dove metterlo
        $('.box-content').trigger('close.box-content');
        box = $('<div class="osy-box box-content" onclick="event.stopPropagation();"></div>');
        box.html(content);
        if (option.minSize != 'no') {
            box.css('min-width', main.width());
        }
        if (option.css) {
            for(k in option.css) {
                box.css(k, option.css[k]);
            }
        }
        option['class'] && box.addClass(option.class);
        
        if (option['offset']) {
            var off = $(this).closest(option['offset']);
            debug(this, off, $(this).offset().top, off.height());
        }
        if (area) {
            area.append(box);
        } else switch(option['insert']) {
            case 'click':
                var pos = {};
                var by = $('body').first().clientHeight/2;
                var bx = $('body').first().clientWidth/2;
                pos[(posClick.x > bx)? 'right': 'left'] = ((posClick.x > bx) ? 2 * bx - posClick.x : posClick.x) + 'px';
                pos[(posClick.y > by)? 'bottom': 'top'] = ((posClick.y > by) ? 2 * by - posClick.y : posClick.y) + 'px';
                box.css(pos);
                $('body').append(box);
                break;
            case 'before':
                box.before(target);
                break;
            case 'after':
                box.after(target);
                break;
            default:
                if (target.is('input')) {
                    box.after(target);                    
                } else {
                    $(target).append(box);
                }
        }
        main.addClass('box-open');
        $('input[type="text"]', box).focus();
        return box;
    }
    
})(desktop.$)