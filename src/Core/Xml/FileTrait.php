<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\fspath;

trait FileTrait {
    private $index;
    private $file;
    private $root; 
    
    protected function init($file, $assertExists = true) {
        if ($assertExists) {
            Assert::file($file, 'File not found : '.$file);
        }
        $this->file = $file;
        $this->root = dirname($file);
        if (file_exists($this->file) and is_file($this->file)) {
            $this->index = simplexml_load_file($this->file, null, LIBXML_NOCDATA);
        } else {
            $this->index = simplexml_load_string('<empty/>');
        }
    }
    public function getRoot() {
        return $this->root;
    }
    protected function listResourceIn(string $name, $filter = 'is_dir', $map = null) {
        $dir = fspath($this->getRoot(), $name);
        if (!$map) $map = function ($item) {
            return $item;
        };
        if (is_dir($dir)) {
            $res = scandir($dir);
            foreach($res as $r) {
                if (in_array($r, ['.', '..'])) continue;
                if (!$filter or call_user_func($filter, fspath($this->root, $name, $r))) {
                    yield $map($r);
                }
            }
        }
    }
    protected function getIndex() {
        return $this->index;
    }
}