<?php

namespace Spinit\Dev\Opensymap\Builder\InputJson;

use Spinit\Dev\Opensymap\Builder\Util;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

class InputJson extends Builder {
    public function build(ItemInterface $item, $withLabel = true)
    {
        $this->addResource('InputJson.css');
        $this->addResource('InputJson.js');

        $inp = $this->makeTextarea($item, $withLabel);
        $div = $this->makeComponent($item, 'osy-input-json');
        $div->add($inp);
        return $div;
    }

    private function makeTextarea($item, $withLabel) {
        $value = $item->getValue();
        if (is_array($value)) $value = json_encode($value);
        $inp = new Tag('textarea');
        $inp->att('rows', $item->get('rows')?:5);
        $inp->att('name', $item->getName());
        $inp->add($value);
        $inp->att('dat', $value);
        return $inp;
    }

    public function storeInModel(ItemInterface $item, ModelInterface $model)
    {
        $value = $item->getValue();
        if ($value) {
            $value = json_decode($value, 1);
            if (!$value) {
                throw new \Exception("Json non formattato correttamente");
            }
            $item->setValue($value);
        }
        return parent::storeInModel($item, $model);
    }
}