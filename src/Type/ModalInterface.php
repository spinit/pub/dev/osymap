<?php

namespace Spinit\Dev\Opensymap\Type;

interface ModalInterface extends ViewInterface {
    function getForm() : FormInterface;
}