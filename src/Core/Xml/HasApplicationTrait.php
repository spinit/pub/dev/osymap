<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Spinit\Dev\Opensymap\Adapter\Xml\ApplicationAdapter;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;

use function Spinit\Util\arrayGet;

trait HasApplicationTrait {
    private $app;

    protected function setApplication(ApplicationAdapter $app) {
        $this->app = $app;
        return $this;
    }
    public function getApplication() : ApplicationAdapterInterface {
        return $this->app;
    }
}