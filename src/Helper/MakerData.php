<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Dev\AppRouter\Request;
use Spinit\Util;
use Spinit\Lib\DataSource\DataSetArray;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of MakerData
 *
 * @author ermanno
 */
class MakerData {
    private $DS;
    public function getDataSource() {
        return $this->DS;
    }
    public function build($field, $query, $sourceContainer = null, $property = '')
    {
        $field->trigger('@prepare-command');
        
        // se non viene indicata la proprietà da caricare allora viene prima verificato se occorre prelevare il dataset
        // e se non c'è viene impostata la richiesta del datasource.
        if ($property == '') {
            if ($dataset = $field->get('dataset')) {
                $list = Util\getInstance($dataset, $field, $query, '', $sourceContainer);
                return $list;
            }
            $property = 'datasource';
        }
        
        // struttura query
        if (! ($ds= $field->get($property))) {
            return new DataSetArray([]);
        }
        $items = Util\arrayGet($ds, ['data', 'item']);
        if (is_array($items)) {
            return new DataSetArray($items);
        }
        // sorgente dati
        $this->DS = $DSource = $field->getView()->getDataSource(Util\arrayGet($ds, 'source'));
        // estrazione dati dalla sorgente
        $params = $this->getQueryParameter($field, $DSource, $ds, $query);
        if ($field->get('debug')=='view') debug($params);
        $list = call_user_func_array([$DSource, 'query'], $params);
        if ($field->get('debug')=='query') debug($list);
        // debug
        if ($sourceContainer) {
            if (is_callable($sourceContainer)) {
                call_user_func($sourceContainer, $DSource);
            } else if (Util\getenv('DEPLOY') != 'RELEASE') {
                $cmd = $DSource->getCommandLast();
                $sourceContainer->add("\n<script type='text/debug' info='DEBUG'><![CDATA[ {$property} \n\n".str_replace("]]>", "]]]]><![CDATA[>", print_r($cmd, 1))."\n\n]]></script>\n");
            }
        }
        $field->trigger('@Builder-buildData', [$list, $sourceContainer]);
        return $list;
    }
    public function buildPkey($field, $pkey, $sourceContainer = null) {
        // struttura query
        if (! ($ds = $field->get('datasource'))) {
            return []; //new DataSetArray([]);
        }
        if (is_array($pkey) and array_key_exists('id', $pkey)) $pkey = $pkey['id'];
        if (is_array($pkey)) $pkey = implode(':', $pkey);
        // sorgente dati
        $DSource = $field->getView()->getDataSource(Util\arrayGet($ds, 'source'));
        // estrazione dati dalal sorgente
        $par = $this->getQueryParameter($field, $DSource, $ds, '', $pkey);
        //debug($pkey, $par);
        $list = call_user_func_array([$DSource, 'query'], $par);
        if ($sourceContainer) {
            if (is_callable($sourceContainer)) {
                call_user_func($sourceContainer, $DSource);
            } else if (Util\getenv('DEPLOY') != 'RELEASE') {
                $cmd = $DSource->getCommandLast();
                $sourceContainer->add("\n<script type='text/debug'><![CDATA[\n".str_replace("]]>", "]]]]><![CDATA[>", print_r($cmd, 1))."\n\n]]></script>\n");
            }
        }
        // ricerca sulla chiave
        foreach($list as $rec) {
            $ris = $rec;
            $fval = array_shift($rec);
            if (strtoupper($fval) == strtoupper($pkey) or !$pkey) {
                $list->close();
                return $ris;
            }
        }
        return [];
    }
    private function getQueryParameter($field, $DSource, $ds, $search='', $pkey = null) {
        
        $form = $field->getForm();
        $instance = $form->getApplication()->getInstance();
        $request = $form->getRequest();
        $queryArgs = $DSource->getCommand(Util\arrayGet($ds, 'query'));
        $query = $instance->normalizeQuery(array_shift($queryArgs));
        // trasposizione delle variabili del modal su form modal. Ora ci si può accedere tramite {(modal._)}
        /*
        if ($form != $field->getMaster()) {
            $form->getStorage()->set('form.modal', $field->getMaster()->getStorage('data'));
        }
        */
        $data = (array) $request->getPost()?:[];
        
        $fdata = (array) $form->getData(); //$form->getStorage('form');
        //debug($fdata);
        $sys = array_merge(
            $request->getApp()?:[],
            ['user'=> (array) $field->getForm()->getApplication()->getInstance()->getUser()]
        );
        $vars  = array_merge(
            $this->processData($field, Util\arrayGet($ds, 'var')), 
            ['pkey' => $pkey],
            ['param' => $field->getParamList()]
        );
        // viene chiesto al main di impostare la stringa di $query secondo le regole generali
        //$data['q'] = $this->getInstance()->getMain()->makeQuerySearchString($query);
        // parametri utilizzabili
        foreach($form->getAllItemsAsList() as $field) {
            if ($nme = $field->getName()) {
                $data[$nme] = $field->getValue();
            }
        }
        $data['q'] = $search;
        //debug($query, $data, $sys, $vars);
        //              {{}}   {[]}   {()}   {||}
        return [$query, $data, $sys, $fdata, $vars];
    }
    
    private function processData($field, $vars)
    {
        if ($vars and count($vars) and !isset($vars[0])) {
            $vars = [$vars];
        }
        $data = [];
        foreach($vars?:[] as $var) {
            $value = Util\arrayGet($var, 'value');
            $name = Util\arrayGet($var, 'name');
            $apply = Util\arrayGet($var, 'apply');
            $call = Util\arrayGet($var, 'call');
            if ($apply) {
                $value = $this->makeApply($apply, $value, $var);
            } else if ($call) {
                $value = $this->makeCall($field, $call, $value, $var, $data);
            }
            $data[$name] = $value;
        }
        return $data;
    }

    /**
     * Richiama una funzione
     * @param callable $func
     * @param type $value
     * @param type $var
     * @return type
     */
    private function makeApply($func, $value, $var)
    {
        if (Util\arrayGet($var, 'applyArg') == 'struct') {
            $value = call_user_func($func, $var);
        } else {
            $value = call_user_func($func, $value);
        }
        return $value;
    }
    
    /**
     * Richiama un controller
     * @param type $field
     * @param string $func
     * @param type $value
     * @param type $var
     * @return type
     */
    private function makeCall($field, $func, $value, $var, &$item)
    {
        $funcAr = explode('.', $func);
        if (count($funcAr)>1) {
            $controller = $field->getView()->getController(array_shift($funcAr));
            $func = array_shift($funcAr);
        } else {
            $controller = $field->getView()->getController();
        }
        if (!$controller) return $value;
        return call_user_func([$controller, $func], $value, $var, $field, $item);
    }
}
