<?php 
namespace Spinit\Dev\Opensymap\Adapter\Xml;

use Spinit\Dev\Opensymap\Core\Xml\AttributeTrait;
use Spinit\Dev\Opensymap\Core\Xml\FileTrait;
use Spinit\Dev\Opensymap\Core\Xml\HasApplicationTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\OverlayAdapterInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;

class OverlayAdapter implements OverlayAdapterInterface {

    use FileTrait;
    use AttributeTrait;
    use HasApplicationTrait {
        getApplication as private _getApplication;
    }

    public function __construct(ApplicationAdapter $app,  string $name)
    {
        $this->setApplication($app);
        $this->name=$name;
        $this->init(fspath($app->getRoot(), 'Overlay', $name, 'index.xml'));
    }
    public function getApplication(): ApplicationAdapterInterface
    {
        return new ApplicationAdapter([$this->getRoot()]);
    }
}