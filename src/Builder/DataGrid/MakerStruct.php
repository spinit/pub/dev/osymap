<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\DataGrid;

use ReflectionClass;
use Spinit\Dev\Opensymap\Builder\PropertyFilter\ItemController;
use Spinit\Util;
use Spinit\Util\Tag;
use Spinit\Lib\DataSource\DataSetInterface;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\getClassPath;

/**
 * Description of MakerStruct
 *
 * @author ermanno
 */
class MakerStruct {
    
    private $skipDataColumn = [];
    private $maxRowPage = 50;
    private $minPageSize = 3;
    private $rowCounter;

    public function build($field, $list, $bdy) {
        $pager = intval(arrayGet($field->get('datasource'), 'pager', $this->maxRowPage));
        if ($pager < 1) $pager = $this->maxRowPage;
        $field->setParam('pager', $pager);
        $field->setParam('tot', $list->rowCount());
        $this->initGroup($field);
        $htbl  = $bdy->add(new Util\Tag('div'))->att('class','osy-body-title hide')->add(new Util\Table());
        $table = $bdy->add(new Util\Table());
        $cols = $this->makeColumns($field, $list->current());
        $iter = $this->makeDataIterator($cols);
        $this->makeHeader($table, $cols, $field, $htbl);
        $data = $iter($list);
        $this->skipData($field, $data);
        $this->makeBody($table, $data, $cols, $field);//, $mapper);
        $this->makePager($field, $bdy, $data);
        $bdy->add(Tag::create('input'))
            ->att('name', $field->getName().'[pag]')
            ->att('value', $field->getValue('pag'))
            ->att('type', 'hidden');
        return $bdy;
    }
    
    private function initGroup($field) {
        // se non e' definita la struttura di gruppo ... allora viene preso il campo __group come riferimento
        $group = Util\arrayGet($field->get('datasource'), 'group', "__group");
        if ($group) {
            if (is_string($group)) {
                if (strpos($group, '{') === false) {
                    $group = ['field'=> $group];
                } else {
                    $group = ['mask'=> $group];
                }
            }
            if (!isset($group['field'])) {
                $group['field'] = $field->getParam('pkey');
            }
            if (!isset($group['mask'])) {
                $group['mask'] = "{{{$group['field']}}}";
            }
        }
        $field->setParam('group', $group);
        $field->setParam('pval', '');
    }
    private function makeColumns($field, $row)
    {
        $pkey = Util\asArray(Util\arrayGet($field->get('datasource'), 'pkey', $field->getForm()->getDataSource()->getPkeyDefault()), ',');
        $cols = Util\arrayGet($field->get('datasource'), ['columns','col']);
        $keys = array_keys($row ?: []);
        // se le colonne sono definite ... occorre aggiungere le pkey
        if ($cols) {
            // se c'è una sola colonna allora viene ritornata essa stessa e non l'insieme di una colonna
            if (!isset($cols[0])) {
                $cols = [$cols];
            }
            foreach ($pkey as $colName) {
                if (in_array($colName, $keys)) {
                    $cols [] = ['field' => $colName, 'pkey'=>'1'];
                }
            }
        } else {
            $cols = [];
            foreach($keys as $colName) {
                if (in_array($colName, $pkey)) {
                    $cols []= ['field' => $colName, 'pkey'=>'1'];
                } else if ($colName[0] != '_') {
                    $cols []= ['field' => $colName, 'label'=>$colName];
                }
            }
        }
        return $cols;
    }
    
    private function makeHeader($table, $cols, $field, $table2)
    {
        if ($field->get('noHeader')=='1') {
            return;
        }
        $header = $table->row()->att('class','osy-row-head');
        $header2 = $table2->row()->att('class','osy-row-head');
        // occorre mostrare il numero riga?
        if ($field->get('rowNumber')!='no') {
            $label = '';
            $th = $header->add(Util\Tag::create('th'))->att('style', 'width:1px');
            $th->add('<div class="label"><div>'.$label.'</div></div>');
            $th2 = $header2->add(Util\Tag::create('th'))->att('style', 'width:1px');
            $th2->add('<div class="label"><div>'.$label.'</div></div>');
        }
        foreach($cols as $col) {
            /*
            if (in_array(Util\arrayGet($col, 'field'), array_keys($this->skipDataColumn))) {
                $this->skipDataColumn[$col['field']] = Util\arrayGet($col, 'label');
                continue;
            }
             * 
             */
            // se è una colonna chiave => non viene mostrata
            if (Util\arrayGet($col, 'pkey')) {
                continue;
            }
            $label = Util\arrayGet($col, 'label', Util\arrayGet($col, 'field'));
            if (is_array($label)) {
                // succede quando si mette una stringa con uno spazio
                $label = implode('', $label);
            }
            if (Util\arrayGet($col, 'noLabel')) {
                $label = '';
            }
            $th = $header->add(Util\Tag::create('th'))->att('class', 'osy-type-'.Util\arrayGet($col, 'type').' '. Util\arrayGet($col, 'class'));
            $th->add('<div class="label"><div>'.$label.'</div></div>');
            $th2 = $header2->add(Util\Tag::create('th'))->att('class', 'osy-type-'.Util\arrayGet($col, 'type').' '. Util\arrayGet($col, 'class'));
            $th2->add('<div class="label"><div>'.$label.'</div></div>');
            if ($width = Util\arrayGet($col, 'width')) {
                $th->att('style', 'width : '.$width);
                $th2->att('style', 'width : '.$width);
            }
        }
        $field->setParam('header', [$header, $header2]);
    }
    
    private function makeBody($table, $data, $cols, $field) //, $mapper)
    {
        if ($controller = Util\arrayGet($field->get('datasource'), 'controller')) {
            $field->setParam('@controller', Util\getInstance($controller, $field));
        }
        if (!$data->current()) {
            // non ci sono dati da scrivere
            $table->row();
            $table->cell('... Empty ...')->att('colspan', count($cols)+10)->att('class', 'message');
        } else {
            $this->rowCounter = 0;
            foreach($data as $rec) {
                // comunica al componente la riga da dover processare (per eventuali cambiamenti)
                if (!$rec) break;
                $item = new \ArrayObject($rec);
                try {
                    $field->trigger('@makeRow', [$item, $this->rowCounter]);
                } catch (\Exception $e) {
                    // il record non viene analizzato
                    continue;
                }
                $this->makeBodyRow($table, intval($field->getParam('pager')) * intval($field->getValue('pag')) + $this->rowCounter, $item, $cols, $field); //, $mapper);
                $this->rowCounter += 1;
                if (!$this->continueProcess($field, $this->rowCounter, $data)) break;
            }
        }
    }

    private function skipData($field, $data) {
        $pager = $field->getParam('pager')?:0;
        $curpage = $field->getValue('pag')?:0;
        for($i = 0; $i < $pager * $curpage; $i++) $data->next();
    }

    private function continueProcess($field, $k, $data) {
        $pager = $field->getParam('pager');
        if ($pager <= 0) return true;
        if(!($data instanceof DataSetInterface)) return true;
        return $k < $pager;
    }
    
    private function makePager($field, $bdy, $data) {
        if($data instanceof DataSetInterface) $this->makePagerDataset($field, $bdy, $data);
    }

    private function makePagerDataset($field, $bdy, $data) {
        $pager = max($field->getParam('pager'), 0);
        if (!$pager) return;
        $pagMax = ceil($data->rowCount() / $pager);
        if ($pagMax <= 1) return;
        
        $pagCur = min(intval(max($field->getValue('pag'), 0)), $pagMax - 1);
        
        $div = $bdy->Add(Tag::create('div'))->att('class', 'pag-sec');
        $pagSize = min($pagMax - 2, $field->get('datasource.pager-size', $this->minPageSize));
        $pagMed = floor($pagSize / 2);
        $pagl = max(1, $pagCur - $pagMed);
        if ($pagl + $pagSize > $pagMax -1 ) {
            $pagl = max($pagMax - $pagSize -1, 0);
        }
        $pagr = min($pagMax, $pagCur + $pagMed);

        // primo elemento
        $this->makePageItem($div, 0, $pagCur);
        if ($pagl > 1) {
            $div->add(Tag::create('span'))->add('...');
        }
        for($i = 0; $i < $pagSize and $i+$pagl < $pagMax -1; $i++) {
            $this->makePageItem($div, $i+$pagl, $pagCur);
        }
        if ($pagMax > $i) {
            if ($i+$pagl < $pagMax-1) {
                $div->add(Tag::create('span'))->add('...');
            }
            $this->makePageItem($div, $pagMax -1, $pagCur);
        }
    }

    private function makePageItem($div, $i, $pagCur) {
        $pag = $div->add(Tag::create('div'))
            ->att('class', 'pag')
            ->att('osy-event', 'load')
            ->att('osy-param', ['pag' => $i]);
        $pag->Add($i+1);
        if ($i == $pagCur) {
            $pag->att('class', 'select', ' ');
        }
    }

    private function makeBodyRowGroup($table, $field, $item, $cols)
    {
        $group = $field->getParam('group');
        if (!$group) {
            return;
        }
        $pkey = Util\asArray($group['field'], ',');
        $pval = [];
        foreach($pkey as $ff) {
            $pval[$ff] = arrayGet($item, $ff);
        }
        $ival = $field->getParam('pval')?:[];
       
        if (implode($ival) != implode($pval)){
            $td = $table->row()->add(Util\Tag::create('td'))->att('colspan', count($cols)+10)->att('class', 'group');
            $td->add(Util\normalize($group['mask'], $item)[0]);
            $field->setParam('pval', $pval);
        }
        
    }
    
    private function makeBodyRow($table, $k, $item , $cols, $field) //, $mapper)
    {
        $item = (array) $item;
        
        $this->makeBodyRowGroup($table, $field, $item, $cols);
        $tr = $table->row();
        $tr->att('group', $field->getParam('pval'));
        $kval = [];
        // occorre mostrare il numero riga?
        if ($field->get('rowNumber')!='no') {
            $tr->add(Util\Tag::create('td'))->att('class', 'cmd')
               ->add('<div class="label">'.($k+1).'</div>');
        }
        if ($field->get("dataInfo")) {
            $tr->att("data-info", $item);
        }
        foreach($cols as $kc => $col) {
            // prima viene chiesto al mapper come interpretare il valore della colonna
            // successivamente si analizza il valore secondo l'eventuale controller impostato
            if (in_array(Util\arrayGet($col, 'field'), array_keys($this->skipDataColumn))) {
                continue;
            }
            $value = $this->getValue($field, $item, $col); //$mapper, 
            if (is_array($value)) {
                $value = json_encode($value);
            }
            if (Util\arrayGet($col, 'pkey')) {
                $kval[Util\arrayGet($col, 'field')] = $value;
            } else {
                $gclass = '';
                if ($kng = Util\arrayGet($col, 'group')) {
                    $rval = Util\arrayGet($item, $kng);
                    $gclass = 'col-group';
                    $gval = $field->getParam('ccol-'.$kc);
                    if ($gval != $rval) {
                        $field->setParam('ccol-'.$kc, $rval);
                        $gclass .= ' col-group-new';
                    } else {
                        $value = '';
                    }
                }
                $table->cell(Util\nvl($value, '&nbsp;'))
                      ->att('class', 'osy-type-'.Util\arrayGet($col, 'type').' '. Util\arrayGet($col, 'class').' '.$gclass)
                      ->att('style', Util\arrayGet($col, 'style'));
            }
        }
        $this->makeBodyRowKey($tr, $kval, $item, $field);
        $this->makeBodyRowActive($tr, $item, $field);
        $tr->att('class', Util\arrayGet($item, '__class'),' ');
        $tr->att('style', Util\arrayGet($item, '__style'),' ');
    }
    
    private function getValue($field, $item, $col) {
        $controller = $field->getParam('@controller');
        $call = Util\arrayGet($col, 'call');
        $value = '';
        if (array_key_exists('field', $col)) {
            $value = Util\arrayGet($item, $col['field']);
            if (!is_array($value)) {
                $value = trim((string) Util\arrayGet($item, $col['field']));
            }
        }
        if ($controller and $call) {
            return call_user_func([$controller, $call], $value, $item, $col, $field);
        }
        if ($mask = Util\arrayGet($col, 'mask')) {
            list($value, ) = Util\normalize($mask, $item);
        } else {
            switch(Util\arrayGet($col, 'type')) {
                case 'html' :
                    break;
                case 'datetime' :
                case 'date' :
                    $frm = $field->getForm()->getApplication()->getInstance()->getFormatter(Util\arrayGet($col, 'type'));
                    $value = $frm->format($value);
                    break;
                case 'money' :
                    // il formatter visualizza l'importo nel formato dell'utente
                    $frm = $field->getForm()->getApplication()->getInstance()->getFormatter('money');
                    // il currency il field che ha la valuta dell'importo da visualizzare
                    if ($fieldCurrency = Util\arrayGet($col, 'currency')) {
                        $value = $frm->format($value, Util\arrayGet($item, $fieldCurrency));
                    } else {
                        $value = $frm->format($value);
                    }
                    break;
                default :
                    if (is_array($value)) {
                        $value = json_encode($value);
                    }
                    $r = str_replace('&nbsp;', ' ', strip_tags($value, '<b><em><code><u><strong>'));
                    $l = Util\arrayGet($col, 'maxlen') ? : 150;
                    if ($l) {
                        $c = substr($r, 0, $l);
                        if (strlen($c) < strlen($r)) {
                            $c .= ' ...';
                        }
                        $r = $c;
                    }
                    $value = $r;
                    break;
            }
        }
        return $value;
    }
    private function makeBodyRowKey($tr, $kval, $item, $field)
    {
        $param = [];
        if (implode('', $kval)) {
            $param['pkey'] = $kval;
        }
        foreach(['__formDetail'=>'formDetail', '__formData'=>'formData', '__formValue'=>'formValue', '__modalAdd'=>'modalAdd', ] as $f => $c) {
            if (Util\arrayGet($item, $f)) {
                $param[$c] = Util\arrayGet($item, $f);
            }
        }
        if (array_key_exists('__data', $item)) {
            $val = json_decode($item['__data'], 1);
            if (is_array($val)) {
                $param['data'] = $val;
            } else {
                $param['data'] = $item['__data'];
            }
        }
        if (!$param) {
            // se la chiave non è stata impostata ... occorre rimuovere la riga
            $tr->remove();
            $this->rowCounter--;
        }
        $tr->att('osy-param', $param);
        $tr->setParam('pkey', $kval);
        if ($field->get('clickDetail') != 'no') {
            $tr->att('class', 'osy-item-det activable', ' ')
               ->att('osy-event', 'open');
        }
    }
    private function makeBodyRowActive($tr, $item, $field)
    {
        $disable = false;
        // se è stata indicata la colonna di attivazione/disattivazione
        if (arrayGet ($item, '__disable')) {
            $tr->att('class', 'disable', ' ');
            $disable = true;
        }
        // è stata preimpostata una lista di valori da selezionare?
        $defList =$field->getValue('check')?:[];

        //debug('ok', (array) $item);
        if (array_key_exists('__check', $item)) {
            // occorre aggiungere la colonna anche nell'intestazione?
            if (!$field->getParam('header-init')) {
                foreach($field->getParam('header') ?:[] as $header) {
                    $header->add(Util\Tag::create('th'))
                    ->att('style','width:20px')
                    ->add(Util\arrayGet($this->skipDataColumn, '__check'));
                }
                $field->setParam('header-init', 1);
            }
            $kval = implode(':', $tr->getParam('pkey'));
            $chk = $tr->add(Util\Tag::create('td'))
            ->att('style','text-align:center')
            ->att('class','clickStop')
            ->add(Util\Tag::create('input'))
                    ->att('name', $field->getName().'[check][]')
                    ->att('type', 'checkbox')
                    ->att('value', $kval);
            if (!$disable) {
                if ($item['__check'] == $kval) {
                    $chk->prp('checked');
                }
            }
            // se il valore è presente tra quelli di default
            if (in_array($kval, $defList)) {
                $chk->prp('checked');
            }
    }
    }
    
    private function makeDataIterator($cols)
    {
        // configurazione dei campi su cui effettuare il listing
        $list = [];
        // chiave su cui effettuare il raggruppamento
        $pkey = [];
        foreach($cols as $col) {
            if (Util\arrayGet($col, 'pkey')) {
                $pkey[] = Util\arrayGet($col, 'field');
            }
            if (!Util\arrayGet($col, 'list')) {
                continue;
            }
            $list[Util\arrayGet($col, 'field')] = Util\asArray(Util\arrayGet($col, 'list'), ',');
        }
        if (!count($list)) {
            // se non sono stati trovati campi da raggruppare allora l'iteratore sarà quello standard
            return function ($data) {
                return $data;
            };
        }
        // altrimenti su ogni record con stessa chiave del precedente vengono raggruppati i campi indicati nel list
        return function($data) use ($list, $pkey) {
            $item = [];
            foreach($data as $rec) {
                if (Util\arraySubset($item, $pkey) != Util\arraySubset($rec, $pkey)) {
                    if (count($item)) {
                        yield $item;
                    }
                    $item = $rec;
                }
                foreach($list as $nme => $prp) {
                    $ii = [];
                    foreach($prp as $p) $ii[] = Util\arrayGet($rec, $p);
                    if (implode('', $ii)) {
                        $item[$nme][] = $ii;
                    }
                }
            }
            if (count($item)) {
                yield $item;
            }
        };
    }

}
