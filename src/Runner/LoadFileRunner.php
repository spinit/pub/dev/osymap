<?php

namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\AppRouter\ResponseException;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Dev\Opensymap\Runner;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;

class LoadFileRunner extends Runner {
    public function run(Request $request): Response
    {
        $path = $request->getPath();
        $file = basename($path);
        $data = $this->getInstance()->getDataSource()->query('
            select hex(id) as id, 
                cnt_typ, nme, bme
                from osy_fle 
            where id = {{@id}}', ['id'=>$file])->first();
        if (!$data) throw new MessageError('File non trovato');
        $data['fullPath'] = fspath($this->getInstance()->getHome(), $data["bme"]);
        $response = $this->getInstance()->getResponse();
        $response->setContent(function() use ($data, $request, $response) {
            
            header('Content-Description: File Transfer');
            header("Cache-Control: no-cache, must-revalidate");
            header("Expires: 0");
            header('Content-Length: ' . filesize($data['fullPath']));
            header('Content-Type: ' . $data['cnt_typ']);
            header('Pragma: public');
            if ($request->hasQuery('download')) {
                header('Content-Disposition: attachment; filename="'.$data['nme'].'"');
            } else {
                header('Content-Disposition: inline; filename="'.$data['nme'].'"');
            }
            readfile($data['fullPath']);
            throw new ResponseException($response);
        });
        return $response;

    }
}