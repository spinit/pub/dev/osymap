(function(desktop, $) {

    $("main#main").on("form-umount", function(event, option) {
        $(this).attr("class", "");
    })
    $("main#main").on("form-mount", function(event, option) {
        $(this).attr("class", option.class);
    })
    
    $('form').on('submit',  event => event.preventDefault());

    $(document).on('click', "[osy-trigger]", async function(event) {
        event.preventDefault();
        let message = this.getAttribute('osy-message');
        if (message && ! await desktop.confirm('Attenzione', message)) {
            return;
        }
        let el = $(this);
        let cmp = el;
        let obs = '';
        if (obs = el.attr('osy-observer')) {
            cmp = $('[id="'+obs+'"]');
        }
        let param = el.attr('osy-param');
        try {
            param = JSON.parse();
        } catch (e) {
            // null;
        }
        cmp.trigger(el.attr('osy-trigger'), param || el.attr('osy-param'));
    });
    $(document).on('click', ".clickStop", async function(event) {
        event.stopImmediatePropagation();
    });
    $(document).on('click', "[osy-event]", async function(event) {
        event.preventDefault();
        let target = this;
        let href = target.getAttribute('osy-href') || target.href || location.href;
        let message = target.getAttribute('osy-message');
        if (message && ! await desktop.confirm('Attenzione', message)) {
            return;
        }
        // viene richiesto ai diveri componenti di aggiungere le info necessarie;
        desktop.send([target, href], desktop.getFormData(target));
    });

    // se il click viene propagato fino a qui ... allora gli osy-box devono essere chiusi
    $(document).on('click', function(event) {
        if ($(event.target).closest('.osy-box').first()) {
            // se viene fatto dentro un box (ma non è un osy-event)
            if (!$(event.target).closest('[osy-event]').add(event.target).is('[osy-event]')) {
                return;
            }
        }
        $('.osy-box').trigger('close');
    });

    $(document).on("setTitle", function(event, title) {
        document.title = title;
        let wtitle = $("header#wrapper-title", this);
        if (!wtitle.first()) return;
        wtitle.html(title);
        let atitle = $("main#main>.content>.title", this);
        if (!atitle.first()) return;
        atitle.hide();
    });

    $(document).on('close', '.osy-box', function(event) {
        event.stopImmediatePropagation();
        $(this).remove();
    });

    desktop.confirm = async (title, message) => {
        let {isConfirmed: selected} = await  Swal.fire({
            icon:'warning',
            title : title, 
            html : message.replace('\n','<br/>'),
            showCancelButton : true
        });
        return selected;
    }
    desktop.error = async (title, message) =>  await Swal.fire({
        title, 
        html : '<pre>'+message+'</pre>', 
        width:'90%',
        icon : 'error'
    });
    
    desktop.alert = async (title, message, classer) => await Swal.fire(title, message, classer);
    
    //connessione a crossbar
    if (window['autobahn']) {
        const  makeWampTopic = window['makeWampTopic'] || ((item)=>item);

        let con = new autobahn.Connection({url: 'ws://'+location.hostname+':'+location.port+'/ws', realm: 'realm1'});
        let ses = null;
        let isBroked = false;
        con.onopen = function (session) {
            console.log('connection open', new Date())

            function write(message, type) {
                let d = document.createElement('div');
                let console = document.querySelector('.console');
                d.classList.add(type + '');
                d.innerText = message;
                console.appendChild(d);
                console.scrollTop = console.scrollHeight;
            }
            ses = session;
            session.subscribe(makeWampTopic('console.log'), function onevent(args) {
                write(JSON.stringify(args));
            });
            session.subscribe(makeWampTopic('console.error'), function onevent(args) {
                write(JSON.stringify(args), 'error');
            });
            session.subscribe(makeWampTopic('console.info'), function onevent(args) {
                if(args[0] && args[0].exec) {
                    desktop.exec(args[0].exec);
                } else {
                    console.log("Info : ", args);
                }
            });
            if (isBroked) {
                // occorre ripristinare tutte le sottoscrizione attive
                let oldList = subList;
                subList = [];
                oldList.forEach(item => desktop.subscribe(item.topic, item.controller));
                // e far verificare se qualche avviso è andato perso durante il down
                $(document).trigger('websocket-break');
            }
            isBroked = false;
        }
        con.onclose = function() {
            console.log('connection close', new Date());
            ses = null;
            isBroked = true;
        }
        
        con.open();

        let subList = [];

        desktop.subscribe = (topic, controller) => {
            let nameTopic = typeof(topic)== typeof('') ? makeWampTopic(topic) : topic[0];
            let listener = {
                topic : topic,
                controller : controller,
                handler : ses.subscribe(nameTopic, controller),
                unsubscribe : function () {
                }
            }
            listener.handler.then( s => listener.unsubscribe = (noFilter)=> {
                try {
                    ses.unsubscribe(s);
                } finally {
                    if (!noFilter) {
                        subList = subList.filter(item => item !== listener);
                    }
                }

            });
            subList.push(listener);
            return listener;
        }

        // ogni 10 secondi si verifica che la connessione sia attiva. Se non è attiva si manda un avvertimento per 
        // chi deve aggiornare i dati, altrimenti fa un saluto al server
        setInterval(
            () => ses? ses.publish('tic-tac', [$('#osy-session').val()]) : $(document).trigger('websocket-break'), 
            1000*50
        );
    }

}) (window.desktop, window.desktop.$)