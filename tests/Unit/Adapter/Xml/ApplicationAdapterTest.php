<?php
namespace Spinit\Test\Unit\Adapter\Xml;

use PHPUnit\Framework\TestCase;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;

use function Spinit\Dev\AppRouter\debug;

class ApplicationAdapterTest extends TestCase {
    /**
     * @var ApplicationAdapter
     */
    private $obj;
    protected function setUp() : void
    {
        parent::setUp();
        $this->obj = new TestApplicationAdapter();
    }
    
    public function testBuilder() {
        $this->assertEquals('Spinit:Test:Builder:TestBuilder', $this->obj->getBuilder(''));
    }

    public function testAttribute() {
        $this->assertEquals('', $this->obj->get('key'));
        $this->obj->set('key', 'test');
        $this->assertEquals('test', $this->obj->get('key'));
    }

    public function testModule() {
        $modules = [];
        foreach($this->obj->getModuleList() as $module) $modules[] = $module;
        $this->assertEquals(['Core'], (array) $modules);
        $module = $this->obj->getModule('Core');
        $this->assertEquals('Core', $module->getName());
    }
    public function testForm() {
        $form = $this->obj->getForm('Core:Test');
        $this->assertEquals('Core:Test', $form->getName());
    }

    public function testOverlay() {
        
        $list = $this->obj->getOverlayList();
        foreach($list as $overlayName) {
            $this->assertEquals($overlayName, $overlayName);
        }
    }
}