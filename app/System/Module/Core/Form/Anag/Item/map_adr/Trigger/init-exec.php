<?php
use Spinit\Dev\Opensymap\Core\Helper\MakerData;
$list = [];
$bound = false;
foreach((new MakerData())->build($this, $this->getValue('search')) as $rec) {
    $this->getForm()->getResponse()->set('map', ['center'=>[$rec['lng'], $rec['lat']]]);
    return;
}
