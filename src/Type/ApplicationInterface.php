<?php

namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;
use Spinit\Lib\Model\ModelInterface;

interface ApplicationInterface {
    public function getName();
    public function run(Request $request) : Response;
    public function getAdapter(): ApplicationAdapterInterface;
    public function getForm($name) : FormInterface;
    public function getModel($name) : ModelInterface;
    public function getInstance() : OpensymapInterface;
    public function getModuleList() : iterable;
    public function getModule($name) : ModuleInterface;
    public function getExtendList() : iterable;
    public function addExtend(ExtendInterface $extend);
    public function getBuilder($name) : string;
    public function getOverlay($name) : OverlayInterface;
    public function getOverlayList() : iterable;
    public function getResponse() : Response;
    public function getExternApplication() : ?ApplicationInterface;
}