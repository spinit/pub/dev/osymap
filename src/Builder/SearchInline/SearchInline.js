/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
    
    // cancellazione listeners già registrati
    $(document).off('.osy-search-inline');

    function moveOption(event, src)
    {
        switch(event.keyCode) {
            case 13: // INVIO
                console.log($('.box-content .current', src));
                $('.box-content .current', src).trigger('click');
                return true;
            case 37:
            case 39:
                return true;
            case 38:
                moveup($('.box-content', src));
                return true;
            case 40:
                movedown($('.box-content', src));
                return true;
        }
        return false;
    }

    function moveup(box)
    {
        var last = box.find('.current');
        if (!last.first()) {
            $(box.find('.osy-search-item').last()).addClass('current');
        } else {
            last.removeClass('current');
            last = last.prev();
            if (!last.is('.osy-search-item')) last = last.prev();
            if (!last.first()) {
                last = $(box.find('.osy-search-item').last());
            }
            last.addClass('current');
        }
    }

    function movedown(box)
    {
        var first = box.find('.current');
        if (!first.first()) {
            $(box.find('.osy-search-item').first()).addClass('current');
        } else {
            first.removeClass('current');
            first = first.next();
            if (!first.is('.osy-search-item')) first = first.next();
            if (!first.first()) {
                first = $(box.find('.osy-search-item').first());
            }
            first.addClass('current');
        }
    }

// se si digita nella casella di testo allora si caricano i dati
$(document).on('keyup.osy-search-inline', '.osy-search-inline .osy-search-name', function(event) {
    var el = this;
    if ($(el).is('.osy-readOnly')) {
        return;
    }
    var src = $(el).closest('.osy-search-inline');
    if (!moveOption(event, src)) {
        // se non è un carattere di movimento allora occorre aggiornare
        // la zona dati
        $('.osy-search-id', src).val('');
        src.addClass('modified');
        clearTimeout(el.ts);
        event.stopPropagation();
        el.ts = setTimeout(function() {
            src.trigger('load-data');
        }, 500);
    }
});

// se si clicca sul pulsante di ricerca si caricano i dati
$(document).on('click.osy-search-inline', '.osy-search-inline .osy-search-btn', function(event) {
    event.stopPropagation();
    event.preventDefault();
    var src = $(this).closest('.osy-search-inline');
    src.trigger('load-data', ['empty']);
    $('.osy-search-name', src).focus();
});

$(document).on('open.osy-search-inline', '.osy-search-inline', function(event, data) {
    if ($(this).is('.closing')) {
        $(this).removeClass('closing');
        return;
    }
    $(this).addClass('open');
    $(this).filter('.osy-search-place').css('minWidth', $(this).width()+'px').html(data);
});
$(document).on('close.osy-search-inline', '.osy-search-inline', function(event, data) {
    event.stopImmediatePropagation();
    $(this).removeClass('open');
});
// se si clicca sul pulsante di ricerca si caricano i dati
$(document).on('setValue.osy-search-inline', '.osy-search-inline', function(event, val) {
    val = val ? val : {'id':'', 'name':''};
    $('.osy-search-id', this).val(val['id']||'');
    $('.osy-search-name', this).val(val['name']||'');
    if (!val['id'] && val['name']) $(this).addClass('modified');
    else $(this).removeClass('modified');
    if (val['id']) {
        $(this).removeClass('no-key');
    } else {
        $(this).addClass('no-key');
    }
    event.preventDefault();
    event.stopImmediatePropagation();
});

// se si seleziona un elemento nella lista
$(document).on('click.osy-search-inline', '.osy-search-inline .osy-search-item', function(event) {
    event.stopPropagation();
    event.preventDefault();
    var itm = $(this);
    var src = itm.closest('.osy-search-inline');
    src.trigger('select-data', {'id':itm.attr('data-id'), 'name':itm.attr('data-name'), 'param':itm.attr('data-param')});
});

// se si seleziona un elemento di raggruppamento ... non succede niente
$(document).on('click.osy-search-inline', '.osy-search-inline .osy-search-group', function(event) {
    event.stopPropagation();
    event.preventDefault();
    return false;
});

// lettura dati
$(document).on('load-data.osy-search-inline', '.osy-search-inline', function(event, cnf) {
    var el = this;
    // se ci sono comandi in esecuzione
    el.xhr && el.xhr.abort();
    let opt = {'_[osy][event]':'load'};
    if (cnf != 'empty') {
        opt['_[osy][param]'] = $('.osy-search-name', el).val();
    }
    el.xhr = new AbortController();
    desktop.send(
        [el, location],
        desktop.getFormData(this, opt),
        {},
        {signal : el.xhr.signal}
    );
});

// seleziona dato
$(document).on('select-data.osy-search-inline', '.osy-search-inline', function(event, data) {
    event.stopPropagation();
    var el = $(this);
    //el.removeClass('modified');
    desktop.send(
        [this, location],
        desktop.getFormData(this, {'_[osy][event]':'select', '_[osy][param]':data})
    );
    $(document).trigger('click');
});

// L'evento viene lanciato sul componente riscritto.
// Può essere un contenitore o l'elemento search stesso.
// Quindi il target viene aggiunto alla ricerca dei search di chi è stato riscritto
$(document).on('initContent.osy-search-inline', function(event) {
    
    $('.osy-search-inline', event.target).add(event.target).each(function() {
        if (!$(this).is('.osy-search-inline')) {
            // se non è ciò che ci si aspetta (dato che è stato fatto un add sul target) allora non si fa nulla
            return;
        }
        var cnt = $('.box-content', this);
        if (cnt.first()) {
            $(this).addClass('open');
            $('.box-content', this).css('min-width', $(this).width());
        }
    });
});

// se si clicca da qualsiasi altra parte .. si chiude il visualizzatore dei dati
$(document).on('click.osy-search-inline', function(event) {
    $(event.target).closest('.osy-search-inline.open').addClass('closing');
    $('.osy-search-inline').trigger('close');
});

})(desktop.$);