<?php

namespace Spinit\Test\Unit\Entity\Item;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Data;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Entity\Item;
use Spinit\Dev\Opensymap\Type\Adapter\FormAdapterInterface;
use Spinit\Dev\Opensymap\Type\Adapter\ItemAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;

class ItemTest extends TestCase {
    /**
     * @var ItemInterface
     */
    private $item;
    private $form;
    private $adapter;
    protected function setUp() :void
    {
        parent::setUp();
        $this->form = new TestForm($this);
        $this->adapter = new TestItemAdapter($this);
        $this->item = new Item($this->form, $this->adapter);
    }

    public function testGetForm() {
        $this->assertInstanceOf(FormInterface::class, $this->item->getForm());
    }

    public function testGetBuild() {
        $this->adapter->set('type', TestBuilder1::class);
        $this->assertEquals('OK TEST 1', $this->item->build());
        $this->adapter->set('type', 'test');
        $this->assertEquals('OK TEST 2', $this->item->build());
    }
    public function testGetName() {
        $this->adapter->set('name', 'NAME OK');
        $this->assertEquals('NAME OK', $this->item->getName());
    }
    public function testGetItemList() {
        $child = new Item($this->form, $this->adapter);
        $this->item->addItem($child);
        $list = $this->item->getItemList();
        $this->assertEquals(1, count($list));
        $this->assertEquals($child, $list[0]);
    }
}

class TestForm implements FormInterface {
    function run(Request $request) : Response {
        return new Response();
    }
    function getApplication() : ApplicationInterface {
        throw new \Exception();
    }
    function getItemList() : iterable {
        return [];
    }
    function getItem($name) : ItemInterface {
        return new ItemDummy();
    }
    function getBuilder($name) : ?BuilderInterface {
        if ($name == 'test') return new TestBuilder2();
        return null;
    }
}

class TestITemAdapter implements ItemAdapterInterface, FormAdapterInterface {
    private $data;
    public function __construct()
    {
        $this->data = new Data();
    }
    function getForm() : FormAdapterInterface {
        return $this;
    }
    function getItemList() : iterable {
        return [];
    }
    function get($name, $default = '') {
        return $this->data->get($name, $default);
    }
    function set($name, $value) {
        $this->data->set($name, $value);
    }
    function getBuilder($name) : string {
        return '';
    }
}

class TestBuilder1 implements BuilderInterface {
    function build(ItemInterface $item)
    {
        return 'OK TEST 1';
    }
}

class TestBuilder2 implements BuilderInterface {
    function build(ItemInterface $item)
    {
        return 'OK TEST 2';
    }
}