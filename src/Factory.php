<?php
namespace Spinit\Dev\Opensymap;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Entity\Application;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\FactoryInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ViewInterface;
use Spinit\Util\Error\FoundException;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\arrayGetAssert;
use function Spinit\Util\getInstance;
use function Spinit\Util\getInstanceArray;

class Factory implements FactoryInterface {
    private $instance;
    
    private $cache = [];
    private $list = [];
    private $listUse = [];
    private $builders = [];

    public function __construct($instance) {
        $this->instance = $instance;
    }

    public function addApplication(string $name, $initer, $use = '', $conf = []) {
        if (array_key_exists($name, $this->list)) {
            throw new FoundException();
        }
        if (!is_callable($initer)) {
            if (is_string($initer)) $initer = [$initer];
            $call = function($use, $conf) use (&$initer) {
                $manager = array_shift($initer);
                return getInstance($manager, $initer, $use, $conf);
            };
        } else {
            $call = $initer;
        }
        $this->list[$name] = [$call, $use, $conf];
        $this->listUse[$use][]=$name;
    }
    
    public function getApplicationUseList($use = '') {
        return arrayGet($this->listUse, $use, []);
    }

    public function getApplication($name) : ApplicationInterface {
        if (!array_key_exists($name, $this->cache)) {
            if (!array_key_exists($name, $this->list)) {
                throw new NotFoundException();
            }
            $ctor = $this->list[$name];
            $adapter = call_user_func($ctor[0], $ctor[1], $ctor[2]);
            $this->cache[$name] = $app = new Application($this->instance, $adapter);
            $app->trigger('init');
        }
        return $this->cache[$name];
    }

    public function getMenu() :iterable {
        return [];
    }

    public function makeBuilder(string $name, ViewInterface $form) : BuilderInterface {
        try {
            $conf = arrayGetAssert($this->builders, $name);
        } catch (\Exception $e) {
            $conf = ['ctor' => $name];
        }
        if (!array_key_exists('obj', $conf)) {
            $conf['obj'] = getInstance($conf['ctor'], $form);
            $this->builders[$name] = $conf;
        }
        return $conf['obj'];
    }
    public function getBuilder(string $name) : string {
        return ''.arrayGet($this->builders, [$name, 'ctor']);
    }
    public function setBuilder($name, $ctor) {
        $this->builders[$name] = ["ctor" => $ctor];
    }

    public function makeRequest($method, $path, $data = null) : Request{
        return new Request($method, $path, $data);
    }
}