<?php

use function Spinit\Dev\AppRouter\debug;

$request = $this->getRequest();
$cur = $this->getApplication()->getInstance();

if ($ice = $request->getPost('hdn_id')) {
    $new = $cur->getMainInteractor()->getInstance($ice);
    $cur->setDataSource('current', $new->getDataSource());
} else {
    if($this->getPkey()) {
        $model = $this->getModel();
        $model->load($this->getPkey());
        $new = $cur->getMainInteractor()->getInstance($model->get('id'));
        $cur->setDataSource('current', $new->getDataSource());
    }
}
