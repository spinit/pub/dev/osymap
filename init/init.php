<?php

use Spinit\Dev\AppRouter\Config;
use Spinit\Dev\Opensymap\Adapter\Xml\ApplicationAdapter;
use Spinit\Dev\Opensymap\Opensymap;

use function Spinit\Dev\AppRouter\fspath;

Opensymap::addInitializer(function($instance) {
    $instance->getFactory()->addApplication('system@opensymap.org', [
        ApplicationAdapter::class, 
        fspath(dirname(__DIR__), 'app', 'System')
    ], 'require');
}); 

Opensymap::addInitializer(function($instance) {
    $instance->getFactory()->addApplication('monitor@opensymap.org', [
        ApplicationAdapter::class, 
        fspath(dirname(__DIR__), 'app', 'Monitor')
    ]);
}); 

Opensymap::addInitializer(function($instance) {
    $instance->getFactory()->addApplication('osy@opensymap.org', [
        ApplicationAdapter::class, 
        fspath(dirname(__DIR__), 'app', 'Osy')
    ]);
}); 

Config::addManager(Opensymap::class, 'Opensymap');
