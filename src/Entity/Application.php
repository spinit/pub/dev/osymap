<?php

namespace Spinit\Dev\Opensymap\Entity;

use PDOException;
use Spinit\Dev\AppRouter\Core\HasInstanceTrait;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Command\System\Install\Items;
use Spinit\Dev\Opensymap\Command\System\Install\Roles;
use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ApplicationAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\BuilderInterface;
use Spinit\Dev\Opensymap\Type\ExtendInterface;
use Spinit\Dev\Opensymap\Type\FactoryInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;
use Spinit\Dev\Opensymap\Type\OverlayInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\Error\MessageException;
use Spinit\Util\TriggerTrait;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;
use function Spinit\Util\biName;
use function Spinit\Util\getInstance;

class Application implements ApplicationInterface {

    use HasInstanceTrait {
        getInstance as private _getInstance;
    }
    use HasAdapterTrait {
        getAdapter as private _getAdapter;
    }

    use TriggerTrait;
    
    private $extends;
    private $extern;

    public function __construct(OpensymapInterface $instance, ApplicationAdapterInterface $adapter) {
        $this->setInstance($instance);
        $this->setAdapter($adapter);
        $this->extends = [];
        $this->extern = null;
        $this->bindExec('init', function() {
            foreach($this->getAdapter()->getBuilderList() as $name => $builder) {
                $this->getInstance()->getFactory()->setBuilder($name, $builder);
            }
        });
    }
    public function setExternApplication(ApplicationInterface $extern) {
        $this->extern = $extern;
        return $this;
    }
    public function getExternApplication() : ?ApplicationInterface {
        return $this->extern;
    }
    public function getName() {
        return $this->getAdapter()->get('name');
    }
    public function getAdapter() : ApplicationAdapterInterface {
        return $this->_getAdapter();
    }
    public function getInstance() : OpensymapInterface {
        return $this->_getInstance();
    }
    public function getModuleList() : iterable {
        return $this->getAdapter()->getModuleList();
    }
    public function getModule($name) : ModuleInterface {
        return new Module($this, $this->getAdapter()->getModule($name));
    }
    public function getExternModule($app, $module) {
        return new Module($this, $this->getAdapter()->getExternModule($app, $module));
    }
    public function getResponse() : Response {
        return $this->getInstance()->getResponse();
    }
    public function getForm($name) : FormInterface {
        $formName = biName($name);
        if ($this->getExternApplication()) {
            $module = $this->getExternModule($this->getExternApplication()->getName(), $formName[0]);
        } else {
            $module = $this->getModule($formName[0]);
        } 
        return $module->getForm($formName[1]);
    }
    public function getModel($name) : ModelInterface {
        $formName = biName($name);
        return $this->getModule($formName[0])->getModel($formName[1]);
    }
    public function run (Request $request) : Response {
        $formName = $request->getPath(0);
        if (strpos($formName,"@")>0) {
            $appRunner = $this->getInstance()->getFactory()->getApplication($formName);
            return $appRunner->setExternApplication($this)
                          ->run($request->shiftPath());
        }
        Assert::notEmpty($formName, 'Form Assente');
        $form = $this->getForm($formName);
        return $form->run($request->shiftPath());
    }
    public function getExtendList() : iterable
    {
        return $this->extends;
    }
    public function addExtend(ExtendInterface $extend) {
        $this->extends[] = $extend;
        return $this;
    }
    public function getBuilder($name) : string
    {
        if ($builder = $this->getAdapter()->getBuilder($name)) {
            return $builder;
        }
        return $this->getInstance()->getFactory()->getBuilder($name)?:$name;   
    }
    public function getOverlayList() : iterable {
        return $this->getAdapter()->getOverlayList();
    }
    public function getOverlay($name) : OverlayInterface {
        return new Overlay($this, $this->getAdapter()->getOverlay($name));
    }
    public function install() {
        foreach($this->getModuleList()as $moduleName) {
            $this->getModule($moduleName)->init();
        }
        (new Items($this))->exec($this->getAdapter()->getItemList());
        (new Roles($this))->exec($this->getAdapter()->getRoleList());
        $this->getInstance()->trigger('flush');
    }
}