<?php
namespace Spinit\Dev\Opensymap\Builder\Money;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

/**
 * Description of Money
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Money extends Builder
{
    public function registerEvent()
    {
        $this->bindExec('change', function($event, $target) {
            $value = trim($target->getValue('name'));
            $error = false;
            if ($value) {
                preg_match('/^([\+\-])?[0-9]+(\.[0-9]{0,2})?$/', $value, $matches);
                if (count($matches) == 0) {
                    // errore nel formato
                    $error = true;
                    $target->getForm()->getResponse()->addData('class', $target->getName(), 'error');
                } else {
                    $target->setValue(floatval($value));
                    $target->getForm()->getResponse()->addCommand("desktop.$('[id=\"'+args[0]+'\"] .osy-money-id').val(args[1])", $target->getName(), $value);
                }
            } else {
                $target->setValue('', 1);
            }
            if (!$error and $event->getParam(0) == 'blur') {
                $target->build($target->getForm()->getResponse());
            }
        });
    }
    public function isNull($field)
    {
        return trim((string) $field->getValue('id')) == '';
    }
    public function build(ItemInterface $field, $withTitle = true)
    {
        $this->addResource('Money.js');
        $this->addResource('Money.css');
        
        // contenitore principale
        $div = $this->makeComponent($field, 'osy-money', $withTitle);
        $value = $field->getValue();
        /*
        if ($value and !is_array($value)) {
            $value = ['id' => $value, 'name'=> number_format($value, 2)];
        }
        */
        $div->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[id]')
            ->att('value', Util\arrayGet($value, 'id'))
            ->att('type', 'hidden')
            ->att('class','osy-money-id');
        $div->add(Util\Tag::create('input'))
            ->att('name', $field->getName().'[name]')
            ->att('value', Util\arrayGet($value, 'name'))
            //->att('osy-id',$field->getName())
            ->att('class','osy-money-name  form-control')
            ->att('onblur', "desktop.$(this).trigger('check')");
        return $div;
    }
    
    public function checkValue($field, $value) {
        $precision = 2;
        if ($field->has('precision')) $precision = $field->get('precision');
        if (!is_array($value)) {
            $value = ['id'=>$value, 'name'=>strlen($value) ? number_format($value, intval($precision), '.', '') : ''];
        }
        return $value;
    }

    protected function storeInModelFieldValue($model, $field, $value, $item)
    {
        $model->set($field, arrayGet($value, 'name'));
    }
    
}
