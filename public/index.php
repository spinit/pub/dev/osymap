<?php
namespace Spinit\Dev\Opensymap;

use Spinit\Dev\AppRouter\Config;
use Spinit\Dev\AppRouter\Entity\MainInteractorDataSource\Interactor;
use Spinit\Dev\AppRouter\Entity\MainStandard;
use Spinit\Dev\Opensymap\Adapter\Xml\ApplicationAdapter;
use Spinit\Lib\DataSource\DataSource;

use Spinit\Dev\Opensymap\Opensymap;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\getenv;

require_once __DIR__. '/../autoload.php';

header('Content-Type: text/plain; charset=utf-8');

$interactor = new Interactor(new DataSource(getenv('CONNECTION_STRING')), fspath(dirname(__DIR__),"home"));

$app = new MainStandard($interactor);

Config::addInstance('main', [
    'manager' => Opensymap::class,
    'label'=>'Opensymap',
    'slug' => 'osy',
    'constr' => getEnv('CONNECTION_STRING'),
    'admin' => true,
    'init'=>['apps' => ['osy@opensymap.org']]
]);

Config::addInstance('monitor', [
    'manager' => Opensymap::class,
    'label'=>'Osy Monitor',
    'slug' => 'monitor',
    'constr' => getEnv('CONNECTION_STRING'),
    'admin' => true,
    'init' => ['apps'=>['monitor@opensymap.org']]
]);

echo $app->run();
