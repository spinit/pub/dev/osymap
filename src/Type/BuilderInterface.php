<?php

namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\InstanceInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util\TriggerInterface;

interface BuilderInterface extends TriggerInterface {
    function build(ItemInterface $item, $withLabel = true);
    function checkValue(ItemInterface $item, $value);
    function setForm(FormInterface $form);
    function getForm() : FormInterface;
    function getInstance() : InstanceInterface;
    function loadFromModel(ItemInterface $item, ModelInterface $model);
    function getItemList(ItemInterface $item);
}