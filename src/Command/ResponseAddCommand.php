<?php
namespace Spinit\Dev\Opensymap\Command;

use Spinit\Dev\AppRouter\Command\ResponseCommand;

class ResponseAddCommand extends ResponseCommand {
    public function exec()
    {
        $args = func_get_args();
        array_unshift($args, 'cmd');
        $this->getResponse()->add('data,exec', $args);
    }
}