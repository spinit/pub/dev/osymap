<?php

use Spinit\Util;
use Spinit\Dev\Opensymap\Helper\MergeMenuBar;
use Spinit\Dev\Opensymap\Core\ApplicationAdapterRegister;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

$instance = $this->getInstance();

$user = $instance->getApplication("system@opensymap.org")->getModel("Core:Anag")->load($instance->getUser("id"));
$role = $instance->getApplication("system@opensymap.org")->getModel("Core:OsyItem")->load($instance->getUser("aut_role"));
$mbars = $user->getPropertyValue("urn:opensymap.org:conf@menubar") |
         $role->getPropertyValue("urn:opensymap.org:conf@menubar") |
         "std";

$merger = new MergeMenuBar();
foreach($instance->getApplicationList() as $app) {
    foreach($app->getModuleList() as $modName) {
        $merger->merge($app->getModule($modName)->getMenuBars($mbars));
    }
}

$menuBars = $merger->getList();

$default = false;

// lista delle menuBar a disposizione ... se ne può vedere 1 per volta
if (count($menuBars) > 1) {
    $defBar = $this->getInstance()->getUser('mbar');
    if (array_key_exists($defBar, $menuBars)) {
        $default = $menuBars[$defBar];
    }
    /*
    $ul = $top->add(Tag::create('ul'));

    $defBar = $this->getInstance()->getUser('mbar');
    if (array_key_exists($defBar, $menuBars)) {
        $default = $menuBars[$defBar];
    }
    foreach($menuBars as $bar) {
        $default = $default ? : $bar;
        $li = $ul->add(Tag::create('li'));
        if ($defBar == $bar['name']) {
            $li->att('class', 'select');
        }
        $link = $li->add(Tag::create('a'));
        $link->att('style', 'width:initial; padding:2px 10px')
                ->att('href', $this->getInstance()->makeSectionPath('common', 'admin/'))
                ->att('osy-event', 'set-mbar')
                ->att('osy-param', $bar['name'])
                ->add($bar['label']);
    }
    */
}

if (!$default) {
    $default = current($menuBars);
}



function makeLabel($menu) : string {
    return (string) arrayGet($menu, 'label', '--no-label--');
}
function makeIcon($menu) : string {
    return (string) arrayGet($menu, 'icon');
}
function makeLink($menu, $self) {
    return (string) $self->getInstance()->makePath("app/".arrayGet($menu, 'app').'/'.Util\arrayGet($menu, 'form')."/");
}
function makeMenuLevel0($icon, $label, $link) {
    ?>
    <div class="nav-item">
        <a osy-event class="nav-link" href="<?php echo $link?>">
            <i class="fas fa-fw <?php echo $icon?>"></i>
            <span><?php echo $label?></span></a>
    </div>
    <?php    
}

function makeMenuLevel1($level, $icon, $label, $menu, $self) {
    ?>
<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    <?php echo $label;?>
</div>

<?php
    foreach($menu as $ki=>$mi) {
?>
        <div class="nav-item active">
<?php
        if (!arrayGet($mi, 'menu')) {
?>
            <a osy-event class="nav-link" href="<?php echo makeLink($mi, $self)?>">
                <i class="fas fa-fw <?php echo makeIcon($mi)?>"></i>
                <span><?php echo makeLabel($mi)?></span></a>
<?php
        } else {
?>
        <!-- Nav Item - Pages Collapse Menu -->
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapse-<?php echo "{$level}-{$ki}"?>" aria-expanded="true"
                aria-controls="collapse-<?php echo "{$level}-{$ki}"?>">
                <i class="fas fa-fw <?php echo makeIcon($mi)?>"></i>
                <span><?php echo makeLabel($mi)?></span>
            </a>
            <div id="collapse-<?php echo "{$level}-{$ki}"?>" class="collapse" aria-labelledby="headingTwo"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <!--h6 class="collapse-header">Custom Components:</h6 -->
<?php
            foreach(arrayGet($mi, 'menu') as $mm) {
                ?>
                    <a osy-event class="collapse-item" href="<?php echo makeLink($mm, $self)?>"><?php echo makeLabel($mm)?></a>
<?php
                
            }
?>
                </div>
            </div>
<?php
        }
?>
        </div>
<?php    
        
    }
}


?>

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="./">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3"><?echo $this->getInstance()->getInfo("name")?></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<?php

$last = [];
foreach($default['menu'] as $kmenu => $menu) {
    // i menu foglia di primo livello vengono mostrati in fondo
    if (!arrayGet($menu, 'menu')) {
        $last[] = $menu;
        continue;
    }
    makeMenuLevel1(
        $kmenu, 
        makeIcon($menu), 
        makeLabel($menu), 
        arrayGet($menu, 'menu'),
        $this
    );
}

if (count($last)) {

?>
<hr class="sidebar-divider">
<?php
    foreach($last as $menu) {
        makeMenuLevel0(
            makeIcon($menu), 
            makeLabel($menu), 
            makeLink($menu, $this)
        );
    }
?>
<?php
}
?>
