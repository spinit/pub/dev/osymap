<?php

use function Spinit\Dev\AppRouter\debug;

$request = $this->getRequest();
$cur = $this->getApplication()->getInstance();

if ($ice = $request->getPost('hdn_ice')) {
    $new = $cur->getMainInteractor()->getInstance($ice);
    $cur->setDataSource('', $new->getDataSource());
}
