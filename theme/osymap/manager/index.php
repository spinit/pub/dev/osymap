<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="%WEB-ROOT%">
    <title><?echo $this->getInstance()->getInfo("name")?></title>

    <!-- Custom fonts for this template-->
    <link href="common/vendors/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="common/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="common/css/main.css" rel="stylesheet" type="text/css">
    <link href="common/css/spinner.css" rel="stylesheet" type="text/css">
    <link href="common/css/app.css" rel="stylesheet" type="text/css">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <div id="menuSidebar">
            <?php include(__DIR__."/menu.php")?>
            </div>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </div>

        <div id="layoutSidenav_nav"></div>

<!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include(__DIR__."/topbar.php")?>
                <!-- End of Topbar -->
                <div id="console"></div>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <form>
                        <main id="main"></main>
                    </form>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="common/vendors/jquery/jquery.min.js"></script>
    <script src="common/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="common/vendors/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages ... -->
    <script src="common/js/sb-admin-2.min.js"></script>

    <script src="common/sweetalert2/sweetalert2.all.min.js"></script>
    <script>
        function makeWampTopic(item) { return "<?php echo $this->getInstance()->getInfo('id')?>:"+item;}
    </script>
    <script src="common/js/desktop.js"></script>
    <script src="common/js/desktop-modal.js"></script>
    <script src="common/js/desktop-box.js"></script>
    <script src="common/js/autobahn.min.js"></script>
    <script src="common/js/app.js"></script>
    <script>
        desktop.$(document).on('DOMContentLoaded', ()=> desktop.send(location.href, window.history?.state?.init));
        $(document).on("click", ".collapse-inner", function () {
            $(this.closest(".collapse.show")).collapse("hide");
        });    
    </script>
</body>
</html>