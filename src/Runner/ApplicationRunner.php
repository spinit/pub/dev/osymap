<?php
namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getenv;

class ApplicationRunner extends DesktopRunner {
    protected function start(Request $request) {
        $instance = $this->getInstance();
        if (!$request->getPath(0)) {
            $request->setPath($instance->getConfiguration('default:app:name'));
        }
        $appName = $request->getPath(0);
        if (getenv('PHPUNIT')) {
            $instance->getResponse()->set('test', $appName);
        }
        Assert::notEmpty($appName,'Application not found');
        $app = $instance->getApplication($appName);
        if (!$app) {
            throw new MessageError("Applicazione non trovata : <code>".$appName."</code>", "Errore di accesso");
        }
        return call_user_func([$app, 'run'], $request->shiftPath());
    }
}