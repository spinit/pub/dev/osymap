<?php

use Spinit\Dev\Opensymap\Command\ResponseSetContent;
use Spinit\Util;
use function Spinit\Dev\AppRouter\debug;

$this->getInstance()->trigger('makeMenuBar');
$this->getInstance()->getUser()->set('mbar', $request->getApp('osy,param'));
if (Util\getenv('PHPUNIT')) {
    $this->getResponse()->set('user', $user);
}
// riscrittura barra dei menu
include($this->getInstance()->getTheme()->getPath("manager/make-menubar.php"));
