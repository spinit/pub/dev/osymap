<?php

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Util;

/**
 * Description of Select
 *
 * @author ermanno
 */

class Select extends Util\Tag
{
    private $curValue;
    public function __construct($name, $value)
    {
        parent::__construct('select');
        $this->att('name', $name);
        $this->curValue = $value;
    }
    
    public function addOption($k)
    {
        $args = func_get_args();
        $v = $k;
        if (count($args) > 1) {
            $v = $args[1];
        }
        $opt = $this->add(Util\Tag::create('option'))->att('value', $k);
        $opt->add($v);
        if ((string)$k === (string)$this->curValue) {
            $opt->prp('selected');
        }
        return $opt;
    }
}
