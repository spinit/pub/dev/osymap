<?php
namespace Spinit\Test\Adapter;

use Spinit\Dev\AppRouter\Data;
use Spinit\Lib\DataSource\AdapterInterface;
use Spinit\Lib\DataSource\DataSetArray;
use Spinit\Util\ParamTrait;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\arrayGet;

class DataSourceAdapter implements AdapterInterface {
    use ParamTrait;
    
    public function __construct()
    {
        $this->data = new Data();
    }
    public function getType() {
        return 'test';
    }
    public function getTypeList() {
        return ['test'];
    }
    public function getPkeyDefault() {

    }
    function check($resourceName) {

    }
    function normalize() {

    }
    function getConnectionString() {
        
    }

    public function insert($table, $record) {
        $this->data[$table]['insert'] = $record;
        $this->data[$table]['data'][$record['f1']] = $record;
    }

    public function update($table, $record, $pkey) {
        $this->data[$table]['update'] = [$pkey, $record];
        $this->data[$table]['data'][$pkey['f1']] = $record;
    }

    public function delete($table,  $pkey) {
        $this->data[$table]['delete'] = $pkey;
        
        if (array_key_exists($pkey['f1'], $this->data[$table]['data'])) {
            unset($this->data[$table]['data'][$pkey['f1']]);
        }
    }

    public function getData($field = '') {
        if (!$field) return $this->data;
        return $this->data->get($field);
    }
    public function select($table, $pkey) {
        $record = arrayGet($this->data,[$table, 'data', $pkey['f1'].'']);
        if ($record) {
            return new DataSetArray([$record]);
        }
        return new DataSetArray([]);
    }

    public function setDataTable($table, $data) {
        $this->data[$table]['data'] = $data;
    }

    public function query($source) {
        return new DataSetArray(json_decode($source, 1));
    }

    public function getCommandLast() {
        
    }
}