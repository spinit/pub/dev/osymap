<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;
use Spinit\Util;

/**
 * Description of DisplayComact
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DisplayExtend extends Display
{
    public function make()
    {
        $field = $this->field;
        $this->builder->addResource('PropertyFilterExtend.js');
        $this->builder->addResource('PropertyFilterExtend.css');
        $this->root->att('class', 'extend', ' ');
        $data = new DataList($this->field);
        $dl = $this->makeContainer($this->field, $this->root);
        $fldID = '';
        $body = $dl;
        foreach($data->getFilter() as $item) {
            if ($fldID != $item['id']) {
                if ($fldID) {
                    $dl = $this->makeContainer($this->field, $this->root);
                }
                $fldID = $item['id'];
                $this->makeName($field, $dl, $item);
                $body = $dl->add(Util\Tag::create('div'))->att('class', 'body');
            }
            $this->makeItem($field, $body, $item);
        }
    }
    
    private function makeContainer($field, $cnt)
    {
        return $cnt->add(Util\Tag::create('div'))->att('class', 'filter');
    }
    
    private function makeName($field, $dl, $rec)
    {
        $dt = $dl->add(Util\Tag::create('div'))->att('class', 'title');
        $cnt = $dt->add(Util\Tag::create('div'));
        $cnt->att('pid', $rec['id'])
            ->att('class', 'name');
        if (in_array($rec['id'], (array) Util\arrayGet($field->getValue(), 'not', []))) {
            $dl->att('class', 'not', ' ');
        }
        $cnt->add(Util\Tag::create('span'))->add($rec['nme']);
    }
    private function makeItem($field, $dl, $rec)
    {
        if (count($rec['det']) != 0) {
            foreach($rec['det'] as $val) {
                $dd = $dl->add(Util\Tag::create('div'))->att('class', 'tag');
                $cnt = $dd->add(Util\Tag::create('div'));
                $cnt->att('did', $val[0])
                    ->att('class', 'item');
                if (Util\arrayGet($field->getValue(), ['item', $rec['id']]) == $val[0]) {
                    $cnt->att('class', 'selected', ' ');
                }
                $cnt->add("<span>".$val[1]."</span>");
            }
        } else {
            $dd = $dl->add(Util\Tag::create('dd'));
            $cnt = $dd->add(Util\Tag::create('div'));
            $cnt->add($rec['fnc']($rec, $field->getName().'[item]', $field->getValue('item')));
        }
    }

}
