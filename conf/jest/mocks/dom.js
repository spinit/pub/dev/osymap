const {JSDOM, Node} = require("jsdom");

const dom = new JSDOM()
global.document = dom.window.document
global.window = dom.window;
global.Node = window.Node;
global.HTMLElement = window.HTMLElement;
global.CustomEvent = window.CustomEvent;