<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;

/**
 * Description of DisplayComact
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Display
{
    protected $builder;
    protected $field;
    protected $root;
    protected $conf;
    
    public function __construct($builder, $field, $root)
    {
        $this->builder = $builder;
        $this->field = $field;
        $this->root = $root;
    }
    
    abstract public function make();
}
