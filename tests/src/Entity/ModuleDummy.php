<?php
namespace Spinit\Test\Entity;

use Spinit\Dev\Opensymap\Type\ExtendInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Lib\Model\Model;
use Spinit\Lib\Model\ModelInterface;

class ModuleDummy implements ModuleInterface {
    function getExtendList() : iterable {
        return [];
    }
    function getExtend($appName) : ExtendInterface {
        throw new \Exception();
    }
    function getFormList() : iterable {
        return [];
    }
    function getForm($name) : FormInterface {
        return new FormDummy();
    }
    function getModelLit() : iterable {
        return [];
    }
    function getModel($name) : ModelInterface {
        throw new \Exception();
    }
}