<?php
namespace Spinit\Dev\Opensymap\Command\System\Install;

use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Util;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of Items
 *
 * @author ermanno
 */

class Items {
    private $app;
    private $sys;
    
    public function __construct(ApplicationInterface $app) {
        $this->app = $app;
        $this->sys = $app->getInstance()->getApplication('system@opensymap.org');
    }
    public function exec($itemList = null) {
        
        $mType = $this->sys->getModel('OsyItem');
        $mRel = $this->sys->getModel('OsyItemRelation');
        $checkTypeList = function ($itemList, $parent) use ($mType, $mRel, &$checkTypeList) {
            foreach($itemList as $item) {
                $code = Util\arrayGet($item, 'code');
                if (!Util\arrayGet($item, 'urn') and $parent) {
                    Assert::notEmpty($code, 'Code not found for '.Util\arrayGet($item, 'name', Util\arrayGet($item, 'urn')));
                    // se il codice non inizia con il carattere @ (cioè è un nome effettivo e non una categoria)
                    // allora la urn viene generato preponendo #.
                    if ($code[0] != '@') {
                        $ncode = '#'.$code;
                    } else {
                        $ncode = $code;
                        $code = substr($code, 1);
                    }
                    $item['urn'] = $parent['urn']. $ncode;
                }
                $mType->clear();
                if (!array_key_exists('urn', $item)) debug($item);
                $mType->load(xid($item['urn']));
                if (!$mType->get('id', '')) {
                    $mType->set('id', xid($item['urn']))
                          ->set('id_app', xid($this->app->getName()))
                          ->set('urn', Util\arrayGet($item, 'urn'))
                          ->set('id_typ', xid(Util\arrayGet($item, 'type', 'urn:opensymap.org@entity')))
                          ->set('cod', $code)
                          ->set('ord', Util\arrayGet($item, 'ord'))
                          ->set('req', Util\arrayGet($item, 'req'))
                          ->set('nme', Util\arrayGet($item, 'name'))
                          ->set('nms', Util\arrayGet($item, 'names'))
                          ->set('val', Util\arrayGet($item, 'val'))
                          ->set('cnf', Util\arrayGet($item, 'conf'));
                    if ($parent) {
                        $mType->set('id_par', xid($parent['urn']));
                    }
                    foreach(Util\arrayGet($item, 'prop',[]) as $p) {
                        $ref = Util\arrayGet($p, "ref");
                        $mType->setPropertyValue($p["type"], $ref ? xid($ref) : $p["val"], "", $ref ? "uuid" : "");
                    }
                    $mType->save();
                    /*
                    foreach(Util\arrayGet($item, 'rels',[]) as $rel) {
                        $mRel->clear();
                        $mRel->set('id_itm', $mType->get('id'));
                        $mRel->set('nme_typ', substr($rel['type'], 0, 256));
                        $mRel->set('id_typ', xid($rel['type']));
                        if (Util\arrayGet($rel, 'ref')) {
                            $mRel->set('rel_id', xid($rel['ref']));
                        } else {
                            $mRel->set('rel_val', $rel['val']);
                        }
                        if (strlen($rel['ord'])) {
                            $mRel->set('ord', $rel['ord']);
                        }
                        $mRel->save();
                    }
                    */
                }
                $checkTypeList(Util\arrayGet($item, 'childs',[]), $item);
            }
        };
        // vengono caricati prima quelli di carattere generale
        $checkTypeList($this->app->getAdapter()->getItemList(), null);
        // e successivamente tutti quelli impostati su ogni modulo
        foreach($this->app->getAdapter()->getModuleList() as $moduleName) {
            $checkTypeList($this->app->getModule($moduleName)->getAdapter()->getItemList(), null);
        }
    }
}
   