<?php
namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Exception\UserNotLoggedExecption;
use Spinit\Dev\Opensymap\Runner;

use function Spinit\Dev\AppRouter\debug;

class CheckUserLoggedRunner extends Runner {
    
    private $runner;

    public function __construct(Runner $runner)
    {
        parent::__construct($runner->getInstance());
        $this->runner = $runner;
    }

    public function run(Request $request) : Response {
        if (!$this->getInstance()->hasUser()) {
            throw new UserNotLoggedExecption();
        }
        return $this->runner->run($request);
    }
}