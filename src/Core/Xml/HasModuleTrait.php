<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Spinit\Dev\Opensymap\Type\Adapter\ModuleAdapterInterface;

use function Spinit\Util\arrayGet;

trait HasModuleTrait {
    private $module;

    protected function setModule(ModuleAdapterInterface $module) {
        $this->module = $module;
        return $this;
    }
    public function getModule() : ModuleAdapterInterface {
        return $this->module;
    }
}