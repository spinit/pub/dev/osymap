/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {

$(document).on('serializeArray', function(event, form, post) {
    $('.osy-image').each(function() {
        var img = $('.osy-content', this);
        post.push({'name':this.id, 'value':img.attr('data-src')});
        });
});

$(document).on('setResource', '.osy-image', function(event, url) {
    Desktop.trigger(this, 'setSource', event.detail.data)
    event.detail.trigger('close');
    return;
    if (!$.isArray(url)) {
        url = [url];
    }
    $('.view', this).attr('src', url[0]).attr('data-list', JSON.stringify(url));
});

$(document).on('setSource', '.osy-image', function(event) {
    let data = event.detail.data;
    console.log(data);
    $('.osy-content', this)
        .attr('data-src', JSON.stringify({file:data.url, name:data.title}))
        .html('<iframe src="'+data.view+'/preview/"></iframe>');
    $('.label', this).html(data.title);

});

})(jQuery)
