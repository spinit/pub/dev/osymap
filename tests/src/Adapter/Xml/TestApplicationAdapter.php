<?php

namespace Spinit\Test\Adapter\Xml;

use Spinit\Dev\Opensymap\Adapter\Xml\ApplicationAdapter;

class TestApplicationAdapter extends ApplicationAdapter {
    public function __construct() {
        parent::__construct([__DIR__.'/app']);
    }
}