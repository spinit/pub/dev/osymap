(function(plugin, $) {

    let modal = $('<div/>');

    plugin.modal = ([option, initFunc], response) => {
        $('.modal-wrap').trigger("close-modal");
        modal = $('<div class="modal-wrap">\
            <div>\
            <input name="_[osy][modal]" class="modal-name" type="hidden"/>\
            <div class="modal-main">\
                <div class="modal-title">Title</div>\
                <div class="modal-body">Body</div>\
                <div class="modal-command">\
                    <button type="button" osy-trigger="close-modal" class="btn mclose">Chiudi</button>\
                    <button type="button" osy-event="back" class="btn mback btn-secondary">&laquo; Indietro</button>\
                    <span class="user-command"></span>\
                </div>\
            </div>\
            </div>\
        </div>').appendTo('main');
        modal.filter('.modal-name').val(option.name);
        modal.filter('.modal-title').html(option.title);
        modal.filter('.modal-body').html(option.body);
        modal.filter('.modal-command .user-command').html(option.command);
        
        if(option.noBtnClose) modal.addClass("no-close");
        if(option.yesBtnBack) modal.addClass("yes-back");

        modal.on('close-modal', function(event) {
            event.stopPropagation();
            modal.remove();
        });
    }

    $(document).on('mousedown', '.modal-wrap', function (event) {
        if (this === event.target) {
            event.stopPropagation();
            $(this).trigger('close-modal');
        }
    })
})(desktop.plugin, desktop.$)