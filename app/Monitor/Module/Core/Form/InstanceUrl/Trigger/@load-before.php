<?php

use Spinit\Lib\DataSource\DataSource;

use function Spinit\Dev\AppRouter\debug;

$request = $this->getRequest();
$cur = $this->getApplication()->getInstance();

if ($ice = $request->getPost('hdn_id')) {
    $new = $cur->getMainInteractor()->getInstance($ice);
    $cur->setDataSource('current', $new->getDataSource());
} else {
    if($this->getPkey()) {
        $model = $this->getModel();
        $model->load($this->getPkey());
        $new = $cur->getMainInteractor()->getInstance($model->get('id'));
        $cur->setDataSource('current', $new->getDataSource());
    } else {
        $DS = $this->getDataSource('main');
        $cnfDatasource = $DS->query('select conf from osx_ice where id = {{@id}}', $this->getItem('hdn_par')->getValue())->first('conf');
        $cur->setDataSource('current', new DataSource($cnfDatasource));
    }
}
