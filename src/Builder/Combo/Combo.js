(function($) {
// clear event namespace
$(document).off('.osy-combo');

$(document).on('change.osy-combo', '.osy-combo select', function(event) {
    event.stopPropagation();
    event.preventDefault();
    let cmp = this.closest('.osy-combo');
    desktop.send([cmp, location], desktop.getFormData(cmp, {
        '_[osy][param]': this.value, 
        '_[osy][event]': 'select'
    }));
});

$(document).on('setValue.osy-combo', '.osy-combo', function(event, data) {
    if (data==="[]") data = "";
    $('.osy-combo-id', this).val(data);
    $('.osy-combo-select', this).val(data);
});

})(desktop.$);
