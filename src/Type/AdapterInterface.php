<?php

namespace Spinit\Dev\Opensymap\Type;

interface AdapterInterface {
    public function get($name, $default = '');
    public function set($name, $value);
    public function has($name);
}