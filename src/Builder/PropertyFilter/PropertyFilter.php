<?php

namespace Spinit\Dev\Opensymap\Builder\PropertyFilter;

use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ItemInterface;
use Spinit\Lib\Model\ModelInterface;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of Custom
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PropertyFilter extends Builder
{
    protected function registerEvent() {
        // in fase di load del componente
        $this->bindExec('@load', function($event, $field) {
//            if ($field->get('type') == 'filter') return;
            if ($field->getForm()->getRequest()->getApp('osy,init')) return;
            $data = new DataList($field);
            $model = $field->getForm()->getModel();
            $values = [];
            if ($model) {
                foreach($data->getFilter() as $item) {
                    $values[$item['id']] = $model->getPropertyValue($item['id']);
                }
            }
            $field->setValue(['item'=>$values], 1);
        });
        
    }
    
    public function build(ItemInterface $field, $withTitle = true)
    {
        $master = $field->get('master');
        // il valore master deve essere impostato in fase di init della form.
        // esso è il valore base su cui verranno fatte tutte le query interne
        if (is_array($master)) {
            $master = xid(array_shift($master));
        }
        if (!$master) {
            return 'master not defined';
        }
        $field->set('master', $master);
        $cnt = $this->makeComponent($field, 'osy-property-filter', !!strlen($field->get('label')) and $withTitle);
        if ($field->get('display') == 'extend') {
            $display = new DisplayExtend($this, $field, $cnt);
        } else {
            $display = new DisplayCompact($this, $field, $cnt);
        }
        $display->make();
        return $cnt;
    }

    public function storeInModel(ItemInterface $field, ModelInterface $model)
    {
        //if ($field->get('type') == 'filter') return;
        // quando il model viene salvato ... allora vengono sincronizzate le proprietà
        $data = new DataList($field);
        if ($model) {
            foreach($data->getFilter() as $item) {
                $model->setPropertyValue($item['urn'], $field->getValue(['item', $item['id']]), '', Util\arrayGet($item, 'typ'));
            }
        }
    }
}
