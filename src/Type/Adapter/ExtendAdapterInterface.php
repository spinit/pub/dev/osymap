<?php

namespace Spinit\Dev\Opensymap\Type\Adapter;

use Spinit\Dev\Opensymap\Type\AdapterInterface;

interface ExtendAdapterInterface extends AdapterInterface {
    function getApplicationName(): string;
    function getApplication() : ApplicationAdapterInterface;
}