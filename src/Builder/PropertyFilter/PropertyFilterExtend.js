(function($) {
// clear event namespace
$(document).off('.osy-property-filter--extend');


$(document).on('click.osy-property-filter--extend', '.osy-property-filter.extend .filter .name', function (event) {
    $(this).closest('.filter').toggleClass('not');
    $(this).trigger('filter');
});
$(document).on('click.osy-property-filter--extend', '.osy-property-filter.extend .item', function (event) {
    var item  = $(this);
    var filter = item.closest('.filter');
    if (filter.is('.not')) {
        return;
    }
    if (!item.is('.selected')) {
        item.closest('.filter').find('.selected').removeClass('selected');
    }
    item.toggleClass('selected');
    item.trigger('filter');
});
$(document).on('keyup.osy-property-filter--extend', '.osy-property-filter.extend input', function (event) {
    $(this).trigger('filter');
});

$(document).on('filter.osy-property-filter--extend', '.osy-property-filter.extend', function (event) {
    event.stopPropagation();
    event.preventDefault();
    desktop.sendEvent(this, 'refresh' /*, {'_[osy][no-refresh][]': this.id}*/);
});

// memorizzazione dati per salvataggio
$(document).on('serializeArray.osy-property-filter--extend', function(event, [form, data]) {
    $('.osy-property-filter.extend').each(function(div) {
        $('.filter.not .name').each(function(itm) {
            data.append(div.id+'[not][]',$(itm).attr('pid'));
        });
        $('.filter:not(.not)').each(function(elm) {
            var itm = $('.item.selected', elm);
            if (itm.cc()) {
                data.append(div.id+'[item]['+ $('.name', elm).attr('pid')+']',itm.attr('did'));
            }
        });
    });
});

})(desktop.$);