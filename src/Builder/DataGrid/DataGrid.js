(function($) {
// clear event namespace
$(document).off('.osy-datagrid');

$(document).on('searchFor.osy-datagrid', function(event, search) {
    $('.osy-datagrid').trigger('loadData', [search.value]);
});

$(document).on('loadData.osy-datagrid', '.osy-datagrid', function(event, q) {
    desktop.send([target, href], desktop.getDataForm(this, {'_[osy][event]':'load', '_[osy][param]':q}));
});

$(document).on('init.osy-datagrid', function() {
    $('.osy-datagrid:not(.inited)').each(function(item) {
        initDataGrid.call(item, $(item));
    });
});

function initDataGrid(dg) {
    // bisogna visualizzare l'intestazione delle colonne nascoste
    let tt = dg.find('.osy-body-title');
    let trs = dg.find('.osy-row-head');
    let tr0 = trs.el(0).find('th');
    let tr1 = trs.el(1).find('th');
    dg.data('$initRows', () => tr1.each((item, i)=> tr0.el(i).width($(item).width()+'px')));
    dg.data('$initRows')();
    tt.removeClass('hide');
    dg.addClass('inited');
}
$(document).on('resized.osy-datagrid', function() {
    $('.osy-datagrid.inited').each(dg =>  $(dg).data('$initRows')());
});


var tsSrc = null;
$(document).on('keyup.osy-datagrid', '.osy-datagrid .search', (event)=>{
    clearTimeout(tsSrc);
    tsSrc = setTimeout(()=> {
        desktop.sendEvent(event.target, 'search', {'q':event.target.value});
        console.log(event.target.value);
    },300);
});

/**
 * deprecated?

//
desktop.on('serializeForm', function(event, form, data) {
    // memorizzazione stato datagrid
    form.querySelectorAll('.osy-datagrid').forEach(dgr => {
        var nme = dgr.getAttribute('id');
        $('tr.selected', this).each(function() {
            data.set( nme+'[selected][]',this.getAttribute('data-pkey'));
        });
        $('input[data-name]', this).each(function() {
            if ($(this).val()) {
                post.push({'name': nme+'[data]'+$(this).data('name'),'value':$(this).val()});
            }
        });
    });
});


desktop.on('scroll-body.osy-datagrid', '.osy-datagrid', (event, type)=>{
    if (type == 'reset') {
        event.target.osyInit = false;
    }
    var cnf = event.target.osyInit;
    if (!cnf) {
        var row  = $('.osy-row-head', event.target);
        cnf = {'fst' : row.first(), 'lst':row.last(), 'ttl' : $('.osy-body-title', event.target) , 'ini':0};
        event.target.osyInit = cnf; 
    }
    if (!cnf.ini) {
        cnf.ttl.removeClass('hide');
        cnf.ini = 1;
    }
    var ths = cnf.fst.find('th .label');
    cnf.lst.find('th .label').each((idx, th)=>{
        $(ths[idx]).width($(th).width());
    });
    cnf.ttl.css('width', event.target.clientWidth);
});

// il resize produce un riaggiornamento della colonna intestazione
desktop.on('resize.osy-datagrid', function() {
    $('.osy-datagrid .osy-body').trigger('resize-head');
});

desktop.on('resize-head', '.osy-datagrid .osy-body', (event)=>{
    var el = event.target;
    if(el.osyInit) {
        el.osyInit.tm = clearTimeout(el.osyInit.tm);
        setTimeout(function() {
            $(el).trigger('scroll-body');
        }, 500);
    }
});

dekstop.on('initContent.osy-datagrid', 'main#main', function(event) {
    var main = $(this);
    var form = main.closest('form');
    $('.osy-datagrid .maximize', main).each(function() {
        
        var os = $(this).offset();
        console.log();
        $(this).css('max-height', 'calc(100vh - '+(os.top + parseInt(form.css('marginBottom')) + 30)+'px)');
    });
});

desktop.on('initContent.osy-datagrid', '.osy-datagrid', function(event) {
    $('.osy-body', event.target).trigger('scroll-body', ['reset']);
});

desktop.on('click.osy-datagrid', '.osy-datagrid.viewCommand .osy-item-det .cmd', function(event) {
    event.stopPropagation();
    this.closest('tr').toggleClass('selected');
    var cmp = $(this).closest('.osy-datagrid');
    if ($('tr.selected', cmp).length > 0) {
        cmp.addClass('showCommand');
    } else {
        cmp.removeClass('showCommand');
    }
});
*/
})(desktop.$);