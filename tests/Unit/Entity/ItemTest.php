<?php
namespace Spinit\Test\Entity\ItemTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\Opensymap\Entity\Builder;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\FormInterface;
use Spinit\Dev\Opensymap\Type\ItemInterface;

use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class ItemTest extends TestCase {

    private $osy;
    /**
     * @var ApplicationInstance
     */
    private $app;

    /**
     * @var FormInterface
     */
    private $form;

    protected function setUp() : void
    {
        parent::setUp();
        $this->osy = new OpensymapDummy();
        $this->app = $this->osy->getFactory()->getApplication('app@test');
        $this->form = $this->app->getForm('Test');
    }
    
    public function testForm() {
        $it1 = $this->form->getItem('itm1');
        $this->assertEquals($this->form, $it1->getView());
    }
    public function testItem() {
        $it1 = $this->form->getItem('itm1');
        $this->assertEquals('itm1', $it1->getName());
        $itp = $it1->getParent();
        $this->assertNotNull($itp);
        foreach($itp->getItemList() as $ic) {
            $this->assertEquals($it1, $ic);
        }
    }

    public function testGetValue() {
        $it1 = $this->form->getItem('itm1');
        $it1->set('type', InnerBuilder::class);
        $it1->setValue(['uno'=>'due']);
        $this->assertEquals('tre', $it1->getValue('uno'));
        $it1->setValue('test');
        $this->assertEquals('test', $it1->getValue());
    }

}

class InnerBuilder extends Builder {
    public function build(ItemInterface $item, $withLabel = true) 
    {
        return 'ok';
    }

    public function checkValue(ItemInterface $item, $value) {
        if (!is_array($value)) {
            return parent::checkValue($item, $value);
        }
        $value['uno'] = 'tre';
        return $value;
    }

    public function setForm(FormInterface $form)
    {
        
    }
}