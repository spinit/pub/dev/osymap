<?php

namespace Spinit\Dev\Opensymap\Core\Xml;

use Spinit\Dev\Opensymap\Type\Adapter\ViewAdapterInterface;

trait HasViewTrait {
    private $view;

    protected function setView(ViewAdapterInterface $view) {
        $this->view = $view;
        return $this;
    }
    public function getView() : ViewAdapterInterface {
        return $this->view;
    }
}