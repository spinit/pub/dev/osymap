<?php
use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\asArray;
if(!$this->getPkey()) {
    $this->getItem('txt_pwd')->set('visible', '');
    $this->getItem('btn_pwd')->set('visible', 'no');
}

if ($this->getItem("cmb_fg")->getValue()) {
    $this->getItem('cmb_typ')->set('visible', '');
    $this->getItem('cmb_gnr')->set('visible', 'no');
    $this->getItem('pnl_brn')->set('visible', 'no');
}
foreach(asArray($this->getItem('hdn_hdn')->getValue(), ",") as $$hdn) {
    $this->getItem($hdn)->set('visible', 'no');    
}