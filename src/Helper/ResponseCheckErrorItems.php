<?php

namespace Spinit\Dev\Opensymap\Helper;

use Spinit\Dev\AppRouter\Core\HasResponseTrait;
use Spinit\Dev\AppRouter\Response;

class ResponseCheckErrorItems {
    use HasResponseTrait;
    public function __construct(Response $response)
    {
        $this->setResponse($response);
    }
    public function exec($errors) {
        if (count($errors)==0) return;
        $this
        ->getResponse()
        ->set(['status'=>'error', 'message'=>'Completare i dati mancanti'])
        ->addCommand('args[0].forEach(item => $(\'[id="\'+item+\'"]\').addClass("error"))', $errors)
        ->stop();
    }
}