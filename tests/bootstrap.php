<?php

use Spinit\Dev\Opensymap\Opensymap;
use Spinit\Lib\DataSource\AdapterManager;
use Spinit\Test\Adapter\Xml\TestApplicationAdapter;
use function Spinit\Util\getenv;

include __DIR__.'/../autoload.php';

Opensymap::addInitializer(function(Opensymap $instance) {
    $instance->getFactory()->addApplication('app@test', [
        TestApplicationAdapter::class
    ], 'require');
}); 

Opensymap::addInitializer(function(Opensymap $instance) {
    $instance->getFactory()->addApplication('test@opensymap.org', 'Spinit:Test:Adapter:Xml:TestApplicationAdapter', 'require');
}); 

AdapterManager::addAdapter('test', 'Spinit:Test:Adapter:DataSourceAdapter');

getenv("PHPUNIT", 1);