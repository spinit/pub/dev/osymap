<?php

namespace Spinit\Dev\Opensymap\Adapter\Xml;

use function Spinit\Dev\AppRouter\debug;

class UtilCheck {
    public static function getItemList ($typeList) {
        $items = [];
        foreach($typeList?:[] as $tt) {
            $item = [];
            foreach($tt->attributes() as $k => $v) {
                $item[(string)$k] = (string)$v;
            }
            $item['childs'] = self::getItemList($tt->item);
            $item['prop'] = [];
            $item['cdata'] = count($tt->children()) ? '' : (string) $tt;
            foreach($tt->prop as $rel) {
                $rec = ['type' => (string)$rel['type'], 'ord'=>(string)$rel['ord']];
                if ((string)$rel['ref']) {
                    $rec['ref'] = (string)$rel['ref'];
                } else {
                    $rec['val'] = (string)$rel;
                }
                $item['prop'][] = $rec;
            }
            $items[] = $item;
        }
        return $items;
    }
}