<?php
namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Runner;
use Spinit\Dev\AppRouter\Response;

use function Spinit\Dev\AppRouter\debug;

abstract class DesktopRunner extends Runner {
    public function run(Request $request) : Response {
        switch ($request->getMethod()) {
            case 'GET':
                $response = $this->getInstance()->getResponse();
                $response->setContent(function() use ($request) {
                    ob_start();
                    include $this->getInstance()->getTheme()->getPath('manager/index.php');
                    return str_replace('%WEB-ROOT%', $request->getScheme().':'.$this->getInstance()->makePath('/'), ob_get_clean());
                }, 'html');
                return $response;
            default:
                return $this->start($request);
        }
    }
    abstract protected function start(Request $request);
}