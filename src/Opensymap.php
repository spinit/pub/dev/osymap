<?php
namespace Spinit\Dev\Opensymap;

use Spinit\Dev\AppRouter\AppRouter;
use Spinit\Dev\Opensymap\Core\Asset;
use Spinit\Dev\AppRouter\InstanceManager;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Entity\Theme;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Dev\Opensymap\Exception\UserNotLoggedExecption;
use Spinit\Dev\Opensymap\Runner\AutenticationRunner;
use Spinit\Dev\Opensymap\Runner\ComponentRunner;
use Spinit\Dev\Opensymap\Runner\DefaultRunner;
use Spinit\Dev\Opensymap\Runner\ApplicationRunner;
use Spinit\Dev\Opensymap\Runner\CheckUserLoggedRunner;
use Spinit\Dev\Opensymap\Runner\ResourceRunner;
use Spinit\Dev\Opensymap\Type\FactoryInterface;
use Spinit\Dev\Opensymap\Type\OpensymapInterface;
use Spinit\Util\TriggerTrait;
use Thruway\ClientSession;
use Thruway\Connection;
use Spinit\Util;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;
use function Spinit\Util\arrayGet;
use function Spinit\Util\arrayGetAssert;
use function Spinit\Util\getenv;

class Opensymap extends InstanceManager implements OpensymapInterface {

    private static $initList;
    private $router;
    private $factory;
    private $apps = [];
    private $sections = [];
    use TriggerTrait;
    
    private $theme;

    public static function addInitializer(callable $init) {
        self::$initList[] = $init;
    }
    public static function rmInitializer(callable $init) {
        if (($index = array_search($init, self::$initList)) !== false) {
            unset(self::$initList[$index]);
        }
    }
    public function setFactory(FactoryInterface $factory) {
        $this->factory = $factory;
    }

    public function getFactory() : FactoryInterface {
        if(!$this->factory) $this->setFactory(new Factory($this));
        return $this->factory;
    }
    public function addRoute($prefix, $caller) {
        if (!is_array($prefix)) $prefix = [$prefix, $prefix];
        $name = array_shift($prefix);
        $prefix = array_shift($prefix);
        $this->sections[$name] = $prefix;
        $this->router = $this->router ?: new AppRouter();
        $this->router->addRoute($prefix, $caller);
    }
    public function makeSectionPath($name, $file) {
        return $this->makePath(fspath(arrayGetAssert($this->sections, $name), $file));
    }
    
    protected function init()
    {
        $init = is_array($this->getInfo("init"))?
            $this->getInfo("init"):
            json_decode($this->getInfo("init"), 1);
        $this->setTheme(new Theme($this->getHome(), $this->getInfo("theme")));

        $this->addRoute(['asset','assets'], function($request) {
            return (new Asset($this, $this->getTheme()->getPath('assets')))->run($request);
        });
        $this->addRoute(['common','common'], function($request) {
            return (new Asset($this, $this->getTheme()->getStdPath('common')))->run($request);
        });
        $this->addRoute(['builder', 'cmp'], function($request) {
            return (new CheckUserLoggedRunner(new ComponentRunner($this)))->run($request);
        });
        $this->addRoute('asset-form', function($request) {
            return (new CheckUserLoggedRunner(new ResourceRunner($this)))->run($request);
        });
        $this->addRoute(['app','app'], function($request) {
            return (new CheckUserLoggedRunner(new ApplicationRunner($this)))->run($request);
        });
        $this->addRoute(['main', ''], function($request) {
            return (new CheckUserLoggedRunner(new DefaultRunner($this)))->run($request);
        });

        foreach(self::$initList ?: [] as $init) call_user_func($init, $this);

        $this->loadApplication();
    }

    /**
     * Inizializzazione Applicazioni configurate 
     */
    private function loadApplication() {
        $apps = [];
        // vengono caricate le app di sistema
        $list = $this->getFactory()->getApplicationUseList('require');
        foreach($list as $app) {
            $apps[$app] = $this->getFactory()->getApplication($app);
        }
        // e successivamente tutte le altre abilitate
        $list = $this->getInit('apps') ?: [];
        if (!is_array($list)) $list = [$list];
        foreach($list as $app) {
            $apps[$app] = $this->getFactory()->getApplication($app);
        }
        // per ogni modulo delle app caricate vengono gestite le estensioni
        foreach($apps as $app) {
            foreach($app->getModuleList() as $moduleName) {
                $module = $app->getModule($moduleName);
                foreach($module->getExtendList() as $ext) {
                    $extApp = $ext->getApplicationName();
                    if (array_key_exists($extApp, $apps)) {
                        $apps[$extApp]->addExtend($ext);
                    }
                }
            }
        } 
        $this->apps = $apps;
    }

    public function getApplication($name) {
        if (array_key_exists($name, $this->apps)) {
            return $this->apps[$name];
        }
        return null;
    }

    public function setTheme(Theme $theme) {
        $this->theme = $theme;
    }
    public function getTheme() : Theme{
        return $this->theme;
    }
    public function getApplicationList() {
        return $this->apps;
    }
    
    public function run(Request $request)
    {
        $response = $this->getResponse();
        try {
            if($request->getPath() == '') {
                //$request->setPath('app');
            }
            $this->setRequest($request);
            $response = $this->router->run($request);
        } catch (MessageError $e) {
            $response = new Response();
            $response->set('status', 'error');
            $response->set('title', $e->getTitle("Errore imprevisto"));
            $response->set('message', $e->getMessage());
            $response->set('trace', explode("\n", $e->getTraceAsString()));
        } catch (UserNotLoggedExecption $e) {
            $response = (new AutenticationRunner($this))->run($request);
        }
        return $response;
    }

    public function install()
    {
        $args = func_get_args();
        if($user = array_shift($args)) {
            $user->set("aut_role",xid("urn:opensymap.org:conf@roles#admin"));
            $user->save(); 
        }
        
        foreach($this->apps as $app) {
            $app->install();
        }
    }
    public function publishTopic($topic, $param = []) {
    
        $par = [
            "realm"   => Util\nvl(Util\getenv('WAMP_REALM'), 'realm1'),
            "url"     => 'ws://'.Util\getenv('WAMP_SERVER'),
        ];
        if (!is_array($param)) {
            $param = [$param];
        }
        ob_start();
        $connection = new Connection($par);
        $connection->on(
            'open',
            function (ClientSession $session) use ($connection, $topic, $param) {
                $session->publish(
                    $this->getInfo('id').':'.$topic, 
                    [$param, [$this->getUser('id'), $this->getUser('id_ses'), $this->getRequest()->getApp('osy,ses')]] , 
                    [], 
                    ["acknowledge" => true])
                ->then(
                    function () use ($connection) {
                        $connection->close(); //You must close the connection or this will hang
                        echo "Publish Acknowledged!\n";
                    },
                    function ($error) {
                        // publish failed
                        echo "Publish Error {$error}\n";
                    }
                );
            }
        );

        $connection->open();
        ob_get_clean();
    }
}