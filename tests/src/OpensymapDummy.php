<?php
namespace Spinit\Test;

use Spinit\Dev\Opensymap\Opensymap;

class OpensymapDummy extends Opensymap {

    public function __construct($conf = [], $init = [])
    {
        parent::__construct(new InteractorDummy(), $conf, $init);
        $this->setBasePath('test');
    }
}