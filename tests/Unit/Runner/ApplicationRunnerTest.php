<?php

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\Opensymap\Runner\ApplicationRunner;
use Spinit\Test\Helper\TestChannel;
use Spinit\Test\OpensymapDummy;
use Spinit\Test\TestInstance;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Util\getInstance;


class ApplicationRunnerTest extends TestCase {

    private $channel;
    private $runner;

    protected function setUp() : void
    {
        parent::setUp();
        $instance = new OpensymapDummy([], []);
        $this->channel = $channel = new TestChannel();
        $this->runner = $runner = new ApplicationRunner($instance);
    }
    
    public function testFormApplicationNotFound() {
        $this->expectException(\Exception::class);
        $this->runner->run(new Request('POST', ['localhost/app','']));
    }

    public function testFormNotFound() {
        $this->expectException(\Exception::class);
        $this->runner->run(new Request('POST', ['localhost/app','test@opensymap.org/Core:FormNotFound']));
    }

    public function testApplicationSetInParameter() {
        $this->runner->getInstance()->getConfiguration()->set('default:app:name','test@opensymap.org/Core:Main');
        $response = $this->runner->run(new Request('POST', ['localhost/app','']));
        $this->assertStringContainsString('osy-input-text', $response->get('content'));
        $this->assertStringContainsString('Input di prova', $response->get('content'));
        $resList = $response->get('data,res');
        $this->assertArrayHasKey('css', $resList);
        $this->assertStringContainsString('InputText.css', $resList['css'][0]);
    }
}
