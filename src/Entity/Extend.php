<?php

namespace Spinit\Dev\Opensymap\Entity;

use Spinit\Dev\Opensymap\Core\HasAdapterTrait;
use Spinit\Dev\Opensymap\Type\Adapter\ExtendAdapterInterface;
use Spinit\Dev\Opensymap\Type\ApplicationInterface;
use Spinit\Dev\Opensymap\Type\ExtendInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;

class Extend implements ExtendInterface {
    use HasAdapterTrait {
        getAdapter as private _getAdapter;
    }
    /**
     * ModuleInterface
     */
    private $module;

    public function __construct(ModuleInterface $module, ExtendAdapterInterface $extend)
    {
        $this->module = $module;        
        $this->setAdapter($extend);
    }
    public function getModule() : ModuleInterface {
        return $this->module;
    }
    public function getAdapter() : ExtendAdapterInterface {
        return $this->_getAdapter();
    }
    public function getApplicationName() : string {
        return $this->getAdapter()->getApplicationName();
    }
    public function getApplication(): ApplicationInterface
    {
        return new Application(
            $this->getModule()->getApplication()->getInstance(), 
            $this->getAdapter()->getApplication()
        );
    }
}