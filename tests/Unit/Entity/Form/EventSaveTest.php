<?php
namespace Spinit\Test\Entity\Form\EventSaveTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class EventSaveTest extends TestCase {

    private $osy;
    private $request;
    private $DS;

    protected function setUp() : void
    {
        parent::setUp();
        // 
        $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $DS = new DataSource('test');
        $DS->getAdapter()->getData()->clear();
        $osy->setDataSource('', $DS);

        $request = $osy->getFactory()->makeRequest('POST', 'app/app@test/Load');
        $request->setApp('osy,event', 'save');

        $this->osy = $osy;
        $this->request = $request;
        $this->DS = $DS;
    }
    
    public function testSaveWhithoutDataInInit()  {

        $this->request->setApp('osy,init', 1);
        $response = $this->osy->run($this->request);
        //debug($this->DS->getAdapter()->getData());
        $this->assertEquals(['f1'=>'', 'f2'=>''], $this->DS->getAdapter()->getData('m1,insert'));
    }

    public function testSaveWhithoutDataNoInit()  {

        $response = $this->osy->run($this->request);
        $this->assertEquals(['f1'=>'10', 'f2'=>''], $this->DS->getAdapter()->getData('m1,insert'));
    }
    public function testSaveWhithDataNoInit() {
        $this->request->setPost('i2', "h3ll0");
        $response = $this->osy->run($this->request);
        $this->assertEquals(['f1'=>'10', 'f2'=>"h3ll0"], $this->DS->getAdapter()->getData('m1,insert'));
    }

    public function testSaveWhithPkey() {
        // il 10 non esiste
        $pkey = base64_encode(json_encode(['f1'=>10]));
        $request = $this->osy->getFactory()->makeRequest('POST', 'app/app@test/Load/'.$pkey);
        $request->setApp('osy,event', 'save');
        $request->setApp('osy,param', 'back');
        $request->setApp('osy,init', 1);
        $request->setPost('i1', '11');
        $request->setPost('i2', 'test');
        $response = $this->osy->run($request);
        $this->assertEquals(['f1'=>'11', 'f2'=>"test"], $this->DS->getAdapter()->getData('m1,data,11'));
        $this->assertStringContainsString('back', json_encode($response->get('data,exec')));
        $pkey = base64_encode(json_encode(['f1'=>11]));
        $request = $this->osy->getFactory()->makeRequest('POST', 'app/app@test/Load/'.$pkey);
        $request->setApp('osy,event', 'save');
        $request->setApp('osy,param', 'back');
        $request->setApp('osy,init', 1);
        $request->setPost('i1', '12');
        $request->setPost('i2', 'testo');
        $response = $this->osy->run($request);
        $this->assertEquals(['f1'=>'12', 'f2'=>"testo"], $this->DS->getAdapter()->getData('m1,data,11'));
    }

    public function testDelete() {
        $this->DS->getAdapter()->setDataTable('m1', [10=>['f1'=>'10','f2'=>'ciao']]);
        $pkey = base64_encode(json_encode(['f1'=>10]));
        $request = $this->osy->getFactory()->makeRequest('POST', 'app/app@test/Load/'.$pkey);
        $request->setApp('osy,event', 'delete');
        $request->setApp('osy,init', 1);
        $response = $this->osy->run($request);
        $this->assertEquals(['f1'=>'10'], $this->DS->getAdapter()->getData('m1,delete'));
        $this->assertStringContainsString('back', json_encode($response->get('data,exec')));
    }
}
