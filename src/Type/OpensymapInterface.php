<?php
namespace Spinit\Dev\Opensymap\Type;

use Spinit\Dev\AppRouter\InstanceInterface;

interface OpensymapInterface extends InstanceInterface {
    function getFactory() : FactoryInterface;
    function makeSectionPath($name, $resource);
    function getApplication($name);
}