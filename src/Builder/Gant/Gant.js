(function($) {

    const debug = console ? console.log : () => {};
    const error = console ? console.error : () => {};
    let isMovedGant = () => true;

    $(document).off('.osy-gant');

    import ('./GantLayout.js?r='+(Math.random()*1000000)).then(module => {
        isMovedGant = module.isMovedGant;
        $(document).on('init.osy-gant', function() {
            $('.osy-gant:not(.inited)').each(function(item) {
                module.init.call(item, $(item), desktop);
            });
        });
    });
    
        
    $(document).on('gant-scroll.osy-gant', '.osy-gant', function(event, ...args)
    {
        dataLoad($(this));
        event.stopImmediatePropagation();
    });
    
    $(document).on('data-move-load.osy-gant', '.osy-gant', function(event, startX, startY) {
        var gant = $(this);
        if (gant.data('startD') != gant.data('@datInput').val()) {
            dataLoad(gant);
            isMovedGant(true);
        }
    });

    $(document).on('data-reload.osy-gant', '.osy-gant', function(event) {
        dataLoad($(this));
    });
    
    $(document).on('click.osy-gant', '.osy-gant .boxLine .box', function(event)
    {
        if (isMovedGant()) return;
        if ($(event.target).is('.box') || $(event.target).closest('.box .content').first()) {
            var box = $(this);
            // viene memorizzata la posizione del click così da poter essere usato dalla gestione dell'evento
            desktop.setPositionClick(event);
            desktop.sendEvent(box, ['box-click', box.data('@box')['id']]);
            $('.osy-box').trigger('close');
        }
    });
    
    // lista risorse visibili
    // su queste vengono richiesti i dati
    let ttLoad = 0;
    function dataLoad(gant, onlyClass) {
        clearTimeout(ttLoad);
        function dataLoadExec() {
            let resView = {};
            let resList = gant.find('.resName .resLine');
            let topGant = gant.offset().top + gant.data('@datList').height()-10;
            let heightGant = gant.height();
            let gap = 0;
            let numRes = 3;
            try {
                let firstRes = null;
                resList.each(function(res) {
                    let rectRes = res.getBoundingClientRect();
                    if (!gap) {
                        // quanti elementi verranno presi sopra e sotto il gant?
                        gap = numRes * rectRes.height;
                    }
                    // rientra nel range delle risorse "visibili"?
                    if (rectRes.top  + gap < topGant) return;
                    if (rectRes.top  - gap > heightGant + topGant) return;
                    // è la prima risorsa visualizzata?
                    if (rectRes.top >= topGant && !firstRes) firstRes = res.getAttribute('rid');
                    // è stato richiesto di aggiornare solo una particolare classe di risorse ... ed essa gli appartiene?
                    if (onlyClass && !res.classList.contains(onlyClass)) return;
                    resView[res.getAttribute('rid')] = res;
                });
                if (firstRes) gant.data('@firstRes').val(firstRes);
                gant.data('$resView', resView);
                gant.trigger('data-load', [resView, onlyClass]);
            } catch (e) {
                error(e);
            }
        }
        ttLoad = setTimeout(dataLoadExec, 500);
    }
    
    /**
     * 
     * @param {type} gant
     * @param {type} rlist : lista box aggiornati
     * @param {type} onlyClass : se not null allora le risorse non presenti NON DEVONO ESSERE CANCELLATE
     * @returns {undefined}
     */
    function dataUpdate(gant, rlist, onlyClass)
    {
        var resList = gant.data('$resList');
        var stat = {'ins':0, 'del':0, 'pass':0};
        for(let rid in rlist) {
            var rdet = rlist[rid];
            // la riga non è nel gant
            if (!resList[rid]) {
                error('risorsa non trovata', rid);
                continue;
            }
            let boxList = {};
            $('.box', resList[rid]).each(item => boxList[$(item).data('@box')['id']] = $(item));
            //debug(rdet);
            for(let bid in rdet.box) {
                let rbox = rdet.box[bid];
                // se il box non ha variazioni .. si passa altrove
                if (boxList[bid]) {
                    if (boxList[bid].data('@box')['sha'] == rbox.sha) {
                        stat.pass += 1;
                        // elemento esaminato ... quindi da NON cancellare
                        delete boxList[bid];
                        continue;
                    } else {
                        boxList[bid].remove();
                    }
                }
                makeBox(gant, rbox, resList[rid]);
                if (boxList[bid]) {
                    stat.upd += 1;
                } else {
                    stat.ins += 1;
                }
            }
            for (let bid in boxList) boxList[bid].remove();

            let pointList = $(resList[rid]).data('$pointRes') || {};
            for(let idx in pointList) pointList[idx].remove();
            pointList = {};
            for(let dat in rdet.date || {}) {
                let dax = rdet.date[dat];
                pointList[dat] = makePoint(gant, dat, dax, resList[rid]);
            }
            $(resList[rid]).data('$pointRes', pointList);
        }
        // da questo momento può iniziare a ricevere le notifiche dal server
        // sulle strutture che hanno risorse modificate
        gant.addClass('data-loaded');
        gant.trigger('data-loaded.osy-gant', [stat]);
    }
    
    function makeBox(gant, box, res)
    {
        let el = $('<div class="box"><div class="content"></div></div>').appendTo(res);
        if (box.has_ovb != '0') {
            el.addClass('overbook');
        }
        //var left = gant.data('@dayWidth').val() * box.step;
        el.data('@box', box).attr('id','box-'+box.id);
        let wd = gant.data('@dayWidth').val();
        el.css('left', Math.round(wd * parseFloat(box.start)) +  'px')
          .css('width', Math.round(wd * parseFloat(box.width)) + 'px');
        let cnt = $('.content', el);
        el.data('@cnt', cnt);
        cnt.attr('style', box.style)
           .html(box.html);
        box.class && cnt.addClass(box.class);
        box.title && el.attr('title', box.title);
        box.before && el.prepend(box.before);
        box.after  && el.append(box.after);
        gant.trigger('make-box.gant', [el, box, res]);
        return el;
    }
    
    function makePoint(gant, dat, point, res)
    {
        let el = $('<div class="point"></div>').appendTo(res);
        el.data('point', point);
        let wd = gant.data('@dayWidth').val();
        el.css('left', Math.round(wd * parseFloat(point.start)) +  'px')
          .css('width', Math.round(wd * 1) + 'px');
        el.html(point.html);
        return el;
    }

    $(document).on('data-set.osy-gant', '.osy-gant', function(event, [data, onlyClass]) {
        dataUpdate($(this), data, onlyClass);
    });
    
})(desktop.$);
