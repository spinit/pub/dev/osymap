<?php

namespace Spinit\Dev\Opensymap\App\Monitor\Module\Core\Source;

use Spinit\Dev\AppRouter\Core\HasInstanceTrait;
use Spinit\Dev\Opensymap\Type\CommandInterface;
use Spinit\Dev\Opensymap\Type\ModuleInterface;
use Spinit\Util\Tag;

use function Spinit\Dev\AppRouter\debug;

class AfterInitModuleCommand implements CommandInterface {
    private $module;
    use HasInstanceTrait;

    public function __construct(ModuleInterface $module)
    {
        $this->module = $module;
        $this->setInstance($this->module->getApplication()->getInstance());
    }
    public function exec($param = null)
    {
        $DS = $this->getDataSource('main');
        $cc = $DS->query('select count(*) as cc from osx_istat_geo_cod')->first('cc');
        if (!$cc) {
            // inizializzazione tabella istat
            $DS->query("
                insert into osx_istat_geo_cod(cod, nme, dsc, dsc_en, typ, ref_tbl, ref_id) 
                select concat('100', lpad(cod_istat, 6, '0')) as cod, nme, dsc, dsc_en, 'nat' as typ, 'osx_istat_stato' as ref_tbl, id as ref_id
                from osx_istat_stato");
        }
    }
}