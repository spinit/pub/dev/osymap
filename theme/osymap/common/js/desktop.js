(function() {

    // gestione visualizzazione spinner
    let spinner = new (function() {
        let timer_in = 0;
        let timer_out = 0;
        let body = document.querySelector('body');
        this.start = ()=> {
            clearTimeout(timer_out);
            if (timer_in) return;
            timer_in = setTimeout(()=>body.classList.add('view-spinner'), 500);
        }
        this.end = ()=> {
            clearTimeout(timer_in);
            clearTimeout(timer_out);
            timer_in = 0;
            timer_out = setTimeout(()=>body.classList.remove('view-spinner'), 100);
        }
    })();

    function split(type) {
        let part = type.split('.');
        return [part.shift(), part.shift()];
    }

    // registro degli eventi
    // è usa responsabi
    function EventRegister() {
        let register = [];
        function check(evName, ns) {
            // workaround per poter gestire eventi che abbiano lo stesso nome dei metodi della classe []
            try {
                if (!register[evName]) register[evName] = {'cb' : [], 'ns':{}}
                if (ns && !register[evName]['ns'][ns]) register[evName]['ns'][ns] = [];
            } catch (e) {
                throw e;
            }
        }
        const removeListener = (element, evNs) => (ev) => {
            if (!ev) return;
            let evName = 'ev-'+ev;
            if (evNs) {
                if(register[evName]) {
                    register[evName]['ns'][evNs] && 
                    register[evName]['ns'][evNs].forEach(cbAr => {
                        element.removeEventListener(ev+'.'+evNs, cbAr[0], cbAr[1]);
                        element.removeEventListener(ev, cbAr[0], cbAr[1]);
                    });
                    delete register[evName]['ns'][evNs];
                    register[evName]['ns'][evNs]=[];
                }
            } else {
                if(register[evName]) {
                    register[evName]['cb'].forEach(cbAr => {
                        element.removeEventListener(ev, cbAr[0], cbAr[1]);
                    });
                    delete register[evName]['cb'];
                    register[evName]['cb']=[];
                }
            }
        }
        this.on = function (element, type, cb, opt) {
            if (!(element instanceof Node)) return;
            let [ev, evNs] = split(type);
            let evName = 'ev-'+ev;
            opt = opt || false;
            check(evName, evNs);
            if (evNs) {
                register[evName]['ns'][evNs].push([cb, opt]);
                element.addEventListener(type, cb, opt);
                element.addEventListener(ev, cb, opt);
            } else {
                register[evName]['cb'].push([cb, opt]);
                element.addEventListener(ev, cb, opt);
            }
        }
        this.off = function (element, type, cb, opt) {
            let [ev, evNs] = split(type);
            if (cb) {
                // cancellazione diretta callback
                element.removeEventListener(ev, cb, opt || false);
                if (evNs) {
                    element.removeEventListener(type, cb, opt || false);
                }
                return;
            }
            // se l'evento non è indicato allora vanno cancellati tutti i listener del ns indicato
            if (!ev) {
                register.forEach(removeListener(element, evNs));
            } else {
                removeListener(element, evNs)(ev);
            }
        }
    }
    let eventRegister = new EventRegister();
    let backPkey = null;
    // classe desktop
    let desktop = {
        alert : async function(title, message, classer) {
            desktop.log(arguments);
            alert(message);
            return desktop;
        },
        confirm : async (title, message) => {
            return confirm(title + ' :\n' + message);
        },
        abort : function() {
            abortConroller.abort();
        },
        error : async function () {
            console && console.error && console.error.apply(console, [].slice.call(arguments));
            return desktop;
        },
        log :  function() {
            console && console.log && console.log.apply(console, [].slice.call(arguments));
            return desktop;
        },
        eq : function (urlA, urlB) {
            while (urlA && urlA.substr(-1) == '/') urlA = urlA.substr(0, urlA.length-1);
            while (urlB && urlB.substr(-1) == '/') urlB = urlB.substr(0, urlB.length-1);
            return urlA == urlB;
        },
        scrollTo : function(element) {
            let el = null;
            if (typeof(element) == typeof('')) {
                element = {'id':element};
            }
            let qry = [];
            for(let n in element) qry.push('['+n+'="'+element[n]+'"]');
            el = document.querySelector(qry.join());
            if (!el) return;
            
            let ref = el.getAttribute('data-scrollTo');
            while (ref) {
                let pel = null
                switch(ref) {
                    case 'parent':
                        pel = el.parentNode;
                        break;
                    default :
                        pel = document.getElementById(ref);
                        break;
                }
                ref = '';
                if (pel) {
                    el = pel;
                    ref = el.getAttribute('data-scrollTo');
                }
            }          
            el.scrollIntoView({
                behavior: 'smooth'
            });

            // evidenziazione dell'elemento su cui si è diretti
            $(el).addClass('scrolledTo');
            setTimeout(()=>$(el).removeClass('scrolledTo'), 3000);
        },
        plugin : {
            cmd : function (item, response) {
                let code = item.shift();
                switch(code) {
                    case 'close':
                        code = '$(this).trigger("close")';
                        break;
                }
                let cmd = new Function('args, response, $', code);
                cmd.apply(this, [item, response, $]);
            },
            back : function(item, response) {
                backPkey = item.shift();
                console.log('back', backPkey);
                window.history.back();
            },
            refresh : function(item, response) {
                // la richiesta di aggiornamento viene fatta partire dopo che sono stati effettuati tutti i comandi
                setTimeout(()=>desktop.send([item, location], {
                    ...response.init, 
                    '_[osy][init]':'1', 
                    '_[osy][ses]': $('#osy-session').val()
                }), 100);
            }
        },
        exec : function(list, target, response) {
            Array.from(list).forEach((item)=>{
                if (!item) return;
                if (typeof(item) == typeof('')) {
                    item = [item];
                }
                var type = item.shift();
                let fnc = this.plugin[type]
                if ( fnc && typeof(fnc) == typeof(()=>{})) {
                    fnc.apply(target ?? document, [item, response]);
                }
            });
        },
        setContent : function (selector, content, outer) {
            let item = document.querySelector(selector);
            if(item) {
                if (typeof(content) == typeof('')) {
                    $(item).html(content, outer);
                } else {
                    for(let e in content) $(e,item).html(content[e]);
                }
                $(item).trigger('init');
            }
        },
        sendEvent : async function (target, event, data, addHeaders, addOptions) {
            if (typeof(target) != typeof([])) target = [target, location.href];
            if (typeof(event) == typeof('')) event = [event];
            if (!data) data = {};
            let args = {};
            // ciò che viene impostato sul parametro evento ha precedenza sui data
            if (event.length) args['_[osy][event]'] = event.shift();
            if (event.length) args['_[osy][param]'] = event.shift();
            if (event.length) args['_[osy][item]'] = event.shift();

            return this.send(
                target, 
                this.getFormData(target, {...data, ...args}),
                addHeaders,
                addOptions
            );
        },/*
        sendEventParam : function (target, event, param) {
            return this.sendEvent(target, event, {'_[osy][param]':param});
        },*/
        setPositionClick : function(event) {

        },
        process : processData,
        
        send : async function(who, data, addHeaders, addOptions) {
            spinner.start();
            addHeaders = addHeaders??{}
            addOptions = addOptions??{}
            let osyReloaded = sessionStorage.getItem('osy-reloaded');
            sessionStorage.setItem('osy-reloaded', '');
            let target = document;
            let href = who;
            // i dati vengono inviati in formato url-encode e vengono accettati in formato json
            let headers =  {
                'Accept': 'application/json', 
                // se la chiamata è frutto di un "reload" forzato della pagina ... occorre comunicarlo
                'X-Opensymap-Call': (osyReloaded == '1' ? 'reload' : ''),
                ... addHeaders};
            
            let body = data;
            if (!(data instanceof FormData)) {
                body = new FormData();
                function storeInList(name, value) {
                    if (typeof(value)==typeof({})) {
                        for(let e in value) storeInList(name?name+'['+e+']':e, value[e]);
                    } else {
                        name && body.append(name, value);
                    }
                }
                storeInList('', data);
            }
            let fetcher = (href, options) => fetch(href, options);
            
            if (addOptions['fetcher']) {
                fetcher = addOptions['fetcher'];
                delete addOptions['fetcher'];
            }
            let options = {method: 'POST', body, headers, ...addOptions};

            if (who instanceof Node | who instanceof myQuery) {
                target = who;
                href = location.href;
            }else if (who instanceof Array) {
                target = who.shift();
                href = who.shift();
            }

            let dbg = [];
            if (options?.body?.entries) {
                for(var itm of options.body.entries()) dbg.push(itm);
            }
            let json = {};
            try {
                $('.error').removeClass('error');
                let response = await fetcher(href || location.href, options);
                if (response) {
                    if (response.ok) {
                        let dummy = response.clone();
                        try {
                            json = await response.json();
                            let {message, title, status, icon} = json;
                            if (!title) title = (status == 'error' ? 'Errore' : 'Attenzione');
                            if(message) {
                                if (! (message instanceof Array)) {
                                    message = [title, message, status == 'error' ? 'error' : icon];
                                }
                                desktop.alert.apply(desktop, message);
                            } 
            
                            processData(json.data, target, json);
                        } catch (e) {
                            console.log('exception', e);
                            desktop.error('Errore', await dummy.text());
                        }
                    } else {
                        console.error(response);
                    }
                }
            } finally {
                spinner.end();
            }
            return new Promise((resolve)=>resolve(json));
        },
        getFormData : function(element, opt) {
            $(document).trigger('beforeSerializeArray');
            let target = $(element);

            let itemName = target.attr('osy-observer');
            if (!itemName) itemName = target.attr('osy-item');
            if (!itemName) itemName = target.closest('[osy-item]:not([osy-item=""])').attr('osy-item');
            let itarget = $(itemName ? '[id="'+itemName+'"]'  : element);
            let form = itarget.closest('form').first() || target.closest("form").first();
            let data = form ? new FormData(form) : new FormData();
            let stdData = {
                '_[osy][event]' : target.attr('osy-event'),
                '_[osy][param]' : target.attr('osy-param')
            }
            
            // trova il primo componente con nome non nullo ... o chi per esso (l'observer)
            if (itemName && !target.attr('osy-event-form')) {
                stdData['_[osy][item]'] = itemName;
            }
            for (let [field, value] of Object.entries({...stdData, ...opt})) {
                if (typeof(value) == typeof({})) value = JSON.stringify(value);
                data.append(field, value);
            }
            $(document).trigger('serializeArray', [form, data]);
            return data;
        },
        download : function(el, data, target) {
            let ifrTag = '';
            let href = "";
            if (el instanceof Array) [el, href] = el; 
            
            if (!(el instanceof myQuery)) el = $(el);
            if (!data) data = [];

            if (!href) href = el.attr('osy-href') || el.attr('href') || location.href
            if (!target) {
                target = 'IFR-'+Math.ceil((1+Math.random())*1000000);
                ifrTag = '<iframe name="'+target+'" id="'+target+'"></iframe>';
            }
            let cmp = $('<div style="display:none"><form method="post" action="'+href+'" target="'+target+'"></form>'+ifrTag+'</div>').appendTo('body');
            let frm = $('form', cmp);
            for([field, value] of desktop.getFormData(el, data)) {
                frm.append($('<textarea></textarea>').attr('name', field).val(value));
            }
            frm.submit();
            setTimeout(()=>cmp.remove(), 5000);
        }
    };

    const myQuery = function(list) {

        let els = [];
        if(list) for(item of list) item instanceof Node && els.push(item);
        this.els = els;
        this.val = (...args) => {
            if (args.length == 0) {
                return this.first() ? els[0].value : '';
            }
            this.first() && (els[0].value = args[0]);
            return this;
        }

        this.addClass = (val) => {
            els.forEach(item=>item.classList.add(val));
            return this;
        }

        this.removeClass = (val) => {
            els.forEach(item=>item.classList.remove(val));
            return this;
        }

        this.toggleClass = (val) => {
            this.is('.'+val) ? 
                this.removeClass(val):
                this.addClass(val);
            return this;
        }
        
        this.add = (el) => {
            $(el).each(item=> els.push(item));
            return this;
        }
        this.is = (selector) => {
            return this.first() ? this.first().matches(selector) : false;
        }
        this.closest = (qry) => {
            return $(this.first() && this.first().closest && this.first().closest(qry));
        }

        this.on = (event, selector, callback, option) => {
            if (typeof(selector) == typeof(()=>{}) ) {
                option = callback;
                callback = selector;
                selector = '';
            }
            event.trim().split(' ').forEach(ev => {
                let processor = null;
                if (selector == '') {
                    processor = (event) => {
                        callback.apply(event.target, [event, event.detail])
                    }
                } else {
                    processor = (event) => {
                        let target = event.target;
                        if (target.matches) {
                            let element = target.matches(selector) ? target : target.closest(selector);
                            if (element) {
                                callback.apply(element, [event, event.detail]);
                                //console.log(selector, event.type, [target, element]);
                            }
                        }
                    };
                }
                els.forEach(item => eventRegister.on(item, ev, processor, option || false));
            });
        }

        this.off = (type) => {
            this.each( item => eventRegister.off(item, type));
            return this;
        }

        this.trigger = (event, data) => {
            if (typeof(event) == typeof('')) {
                event = new CustomEvent(event, {detail : data, bubbles : true});
            }
            
            els.forEach(item => {
                item.dispatchEvent && 
                item.dispatchEvent(event)
            });
        }
        
        this.html = (...args) => {
            switch(args.length) {
                case 0:
                    return this.first() ? els[0].innerHTML : '';
                default:
                    els.forEach( item => args.length>1 && args[1] ? item.outerHTML = args[0]: item.innerHTML = args[0]);
            }
            return this;
        }
        this.next = () => {
            return $(els[0]&&els[0].nextElementSibling);
        }

        this.prev = () => {
            return $(els[0]&&els[0].previousElementSibling);
        }

        this.attr = (...args) => {
            if (args.length<2) {
                if (!this.first()) return '';
                return (els[0]?.getAttribute(args[0]))??'';
            }
            if (typeof(args[1])==typeof({}) || typeof(args[1])==typeof([])) args[1] = JSON.stringify(args[1]);
            if (this.first()) els[0]?.setAttribute(args[0], args[1]);
            return this;
        }

        this.show = () => {
            this.css('display', 'initial');
        }
        this.hide = () => {
            this.css('display', 'none');
        }
        this.css = (...args) => {
            switch (args.length) {
            case  0 : return this;
            case  1 : 
                if (typeof(args[0]) == typeof('')) return els[0]?.style[args[0]];
                for(let e in args[0]) this.css(e, args[0][e]);
                return this;
            default : els.forEach(item=>item.style[args[0]] = args[1]); return this;
            }
        }
        this.styles = () => {
            if (!els[0]) return {};
            let style = els[0].currentStyle || (window.getComputedStyle && window.getComputedStyle(els[0]));
            return style || {};
        } 
        
        this.each = (f) => {
            els.forEach(f);
            return this;
        }

        this.width = (...args) => {
            if (args.length == 0) {
                return this.first() ? els[0].clientWidth : 0;
            }
            return $(els).css('width', args[0]);
        }

        this.height = (...args)  => {
            if (args.length == 0) {
                return this.first() ? els[0].clientHeight : 0;
            }
            return $(els).css('height', args[0]);
        }

        this.filter = (query) => {
            return $(this.first() && els[0].querySelectorAll(query));
        }
        
        this.find = (query) => {
            return $(this.first() && els[0].querySelectorAll(query));
        }
        
        this.focus = () =>{
            this.first() && els[0].focus();
            return this;
        }

        this.appendTo = (element) => {
            $(element).append(this);
            return this;
        }

        this.append = (elements) => {
            $(elements).each(item => els.forEach(base =>base.append(item)));
            return this;
        }
        this.prepend = (elements) => {
            $(elements).each(item => els.forEach(base =>base.prepend(item)));
        }
        this.after = (element) => {
            let p = $(element).parent();
            this.each(item => 
                p.first().insertBefore(item, $(element).first().nextSibling)
            );
        }
        this.before = (element) => {
            let p = $(element).parent();
            this.each(item => 
                p.first().insertBefore(item, $(element).first())
            );
        }
        this.first = () => {
            return els[0];
        }
        this.last = () => {
            if (!els.length) return null;
            return els[els.length - 1];
        }
        this.parent = () => {
            return $(this.first() && this.first().parentNode);
        }

        this.cc = (...attrs) => {
            if (attrs.length == 0) return els.length;
            return els[attrs[0]];
        }
        this.el = (num) => {
            return $(els[num]);
        }
        this.remove = () =>{
            this.each(item => {
                item.remove()
            });
        }
        this.clone = (...args) => {
            let el = this.first();
            if (args.length == 0) return $(el.cloneNode());
            return $(el.cloneNode(args[0]));
        }
        this.data = (...args) => {
            if (!this.first()) {
                if (args.length == 0) return {};
                if (args.length < 2) return null;
                return this;
            }
            if (!els[0]._data) {
                els[0]._data = {};
            }
            if (args.length == 0) return els[0]._data;
            // se la proprietá é privata .. allora non viene esposta nel dataset
            if (["$","@"].includes(args[0][0])) {
                if (args.length<2)  return els[0]._data[args[0]];
                els[0]._data[args[0]] = args[1];
            } else {
                if (args.length<2)  {
                    let val = els[0].dataset[args[0]];
                    if (typeof (val) == typeof("")) {
                        try {
                            val = JSON.parse(val);
                            if (!val) val = els[0].dataset[args[0]];
                        } catch (e) {
                            //  se cé una eccezione allora si prende il valore di val originale
                        }
                    }
                    return val;
                }
                let val = args[1];
                if (typeof (val) == typeof({})) {
                    val = JSON.stringify(val);
                }
                els[0].dataset[args[0]] = val;
            }
        
            return this;
        }
        this.offset = () => {
            if (!this.first()) {
                return {top : 0, left: 0};
            }
            var rect = els[0].getBoundingClientRect();

            return { 
                top: rect.top + window.scrollY, 
                left: rect.left + window.scrollX, 
            };            
        }
        this.scrollTop = () => {
            if (!this.first()) {
                return 0;
            }
            return els[0].scrollTop;
        }
        this.submit = () => {
            this.each(f => f.submit());
        }
    }

    const $ = function () {
        if (arguments.length > 1) {
            return $(arguments[1]).filter(arguments[0]);
        }
        let [query] = arguments;
        if (typeof(query)==typeof('')) {
            // è una stringa html?
            if (query.trim().substr(0,1) == '<') {
                let t = document.createElement('div');
                t.innerHTML = query;
                return $(t.children);
            }
            return $(document).filter(query);
        }
        // è un wrapper?
        if (query instanceof myQuery) return query;
        // è un nodo singolo della pagina?
        if (query instanceof Node) query = [query];

        return new myQuery(query);
    }
    // workaround
    $.each = (list, f) => {
        list.forEach((el, key)=>f(key, el));
    }

    desktop.$$ = desktop.$ = $;
    
    // Passando una lista di script vengono caricati tutti insieme.
    // Quando sono stati caricati tutti, viene eseguito "success"
    let jsAdded = {};
    let jsDep = 0;      // quanti script vengono sono in fase di caricamento
    let jsCount;    // quanti script sono stati caricati
    function addJs(jsList, success) {
        jsList = jsList ? jsList : [];
        let load = () => {
            if (jsList.length) {
                url = jsList.shift();
                if (url && !(url in jsAdded)) {
                    jsAdded[url] = 1;
                    let src = document.createElement('script');
                    src.setAttribute('src', url);
                    src.onload = load;
                    $('body').append(src);
                } else {
                    load();
                }
                // si aspetta a far partire il success
                return;
            }
            success();
        }
        load();
    }
    
    var cssAdded = {};
    function addCss(cssList) {
        cssList = cssList ? cssList : [];
        cssList.forEach(function(value, key) {
            if (value in cssAdded) {
                return;
            }
            cssAdded[value] = 1;
            let src = document.createElement('link');
            src.setAttribute('href', value);
            src.setAttribute('type', 'text/css');
            src.setAttribute('rel', 'stylesheet');
            document.querySelector('head').append(src);
      });
    }

    function processData(data, target, response) {
        if (!data) return;
        addCss(data.res && data.res.css);
        addJs(data.res && data.res.js, function() {
            // impostazione delle classi dei componenti
            data.class && data.class.forEach((itm)=>$('[id="'+itm.shift()+'"]').addClass(itm.shift()));
            // impostazione dei valori iniziali
            for(k in response.init||{}) $('[id="'+k+'"]').trigger('setValue', response.init[k]);
            // esecuzione degli script inviati
            data.exec && desktop.exec(data.exec, target, response);
            // eventuali aggiustamenti
            for(k in response.values||{}) {
                let vv = response.values[k];
                $('[id="'+k+'"]').trigger('setValue', vv);
            }
        });
    }

    window.desktop = desktop;

    window.addEventListener('popstate', function() {
        let data = window.history?.state?.init || {};
        if (data['_'] && data['_']['osy'] && data['_']['osy']['back-name']) {
            data['_']['osy']['back-pkey'] = backPkey;
        }
        backPkey = null;
        $('form').addClass('osy-sending');
        desktop.send(location.href, data, {'X-Opensymap-Call': 'popstate'})
            .then(()=>$('form').removeClass('osy-sending'));        
    });
    let resizeTimer = 0;
    window.onresize = () => {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(()=>$(document).trigger('resized'), 500);
    }
    window.onbeforeunload = () => {
        // la pagina viene forzatamente riletta
        sessionStorage.setItem('osy-reloaded', '1');
    }

})();
