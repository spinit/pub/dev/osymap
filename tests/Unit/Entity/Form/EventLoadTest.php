<?php
namespace Spinit\Test\Entity\Form\EventLoadTest;

use PHPUnit\Framework\TestCase;
use Spinit\Dev\AppRouter\Request;
use Spinit\Lib\DataSource\DataSource;
use Spinit\Test\OpensymapDummy;

use function Spinit\Dev\AppRouter\debug;

class EventLoadTest extends TestCase {

    public function testLoadFormWhithoutPkey()  {
        $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $osy->setDataSource('', new DataSource('test'));

        $request = $osy->getFactory()->makeRequest('POST', 'app/app@test/Load/');
        $request->setPost('i2', "h3ll0");

        $response = $osy->run($request);
        $this->assertTrue($osy->hasUser());
        $this->assertStringContainsString('value="10"', $response->get('content'));
        $this->assertStringContainsString('value="h3ll0"', $response->get('content'));
        $this->assertStringContainsString('TEST TITLE OK', $response->get('content'));
    }

    public function testLoadFormWhithPkey()  {
        $osy = new OpensymapDummy();
        $osy->setUser(['name'=>'test']);
        $DS = new DataSource('test');

        $osy->setDataSource('', $DS);
        $pkey = base64_encode(\json_encode(['f1'=>100]));
        $DS->getAdapter()->setDataTable('m1', [100=>['f1'=>'100','f2'=>'ciao']]);
        $request = $osy->getFactory()->makeRequest('POST', 'app/app@test/Load/'.$pkey);
        $response = $osy->run($request);
        $this->assertStringContainsString('value="100"', $response->get('content'));
        $this->assertStringContainsString('value="ciao"', $response->get('content'));
    }
}