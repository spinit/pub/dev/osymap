<?php
namespace Spinit\Dev\Opensymap\ViewEvent;

use Spinit\Dev\Opensymap\Command\ResponseAddCommand;
use Spinit\Dev\Opensymap\Helper\ResponseCheckErrorItems;

use function Spinit\Dev\AppRouter\debug;

class FormSave extends EventHandlerForm {

    public function doSave($event) {
        $form  = $this->getForm();
        // se è impostato un model
        if ($form->get('model')) {
            $model = $form->getModel();
            try {
                $model->save();
                //debug($model->getPkey());
                $form->setPkey($model->getPkey());
                // viene impostato preventivamente la chiave come valore di ritorno
                $form->setParam("@backValue", $form->getPkey());
                $form->getResponse()->addCommand("window.history.replaceState(history.state, '', args[0])", $form->getUrl());
            } catch (\Exception $e) {
                debug(
                    $e->getMessage(),
                    $model->getDataSource()->getCommandLast(),
                    $model->getResource(),
                    $model->asArray(),
                    $e->getTraceAsString()
                );
            }
        }
    }

    public function doStore($event)
    {
        $form  = $this->getForm();
        if (!$form->get('model')) return;
        $model = $form->getModel();
        $model->clear();

        if ($pkey = $this->getForm()->getPkey()) {
            $model->load($pkey);
        }
        // i campi chiave sono preimpostati
        $model AND $pkey AND $model->set($pkey);

        $errors = $form->checkItemsValue(function($item) use ($model) {
            $item->getBuilder()->storeInModel($item, $model);
        });

        (new ResponseCheckErrorItems($this->getForm()->getResponse()))->exec($errors);
    }

    public function doEnd($event) {
        $form = $this->getForm();
        switch($event->getParam(0)) {
            case 'back':
                $form->getResponse()->addExec('back', $form->getParam("@backValue"));
                break;
            case 'noBuild':
                // viene richiesta la rilettura forzata del model
                $form->getRequest()->setApp('osy,load-model', '1');
                $form->trigger('@load', [$form->getPkey()]);
                break;
            default :
                // viene richiesta la rilettura forzata del model
                $form->getRequest()->setApp('osy,load-model', '1');
                $form->trigger('@load', [$form->getPkey()]);
                $form->trigger('@build');
                break;
        }
    }
}