<?php
namespace Spinit\Dev\Opensymap\Runner;

use Spinit\Dev\AppRouter\Core\Asset;
use Spinit\Dev\AppRouter\Request;
use Spinit\Dev\AppRouter\Response;
use Spinit\Dev\Opensymap\Exception\MessageError;
use Spinit\Dev\Opensymap\Runner;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Dev\AppRouter\debug;
use function Spinit\Dev\AppRouter\fspath;

class AutenticationRunner extends Runner {
    private $auth;
    protected function init() {
        $this->auth = new Asset($this->getInstance(), $this->getInstance()->getTheme()->getPath('auth'));
    }
    public function run(Request $request) : Response {
        try {
            try {
                return $this->auth->run($request->getPath(), $request->getApp('osy,event'), $request);
            } catch (NotFoundException $e) {
                return $this->auth->run('login', $request->getApp('osy,event'), $request);
            }    
        } catch (MessageError $e) {
            $response = $this->getInstance()->getResponse();
            //$response = new Response();
            $response->set('status', 'error');
            $response->set('title', $e->getTitle("Errore imprevisto"));
            $response->set('message', $e->getMessage());
            return $response;
        } 
    }
}